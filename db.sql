-- Adminer 4.8.1 MySQL 5.7.37-0ubuntu0.18.04.1 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `addresses`;
CREATE TABLE `addresses` (
  `number` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postal_code` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addressesID` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`addressesID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `addresses` (`number`, `postal_code`, `street`, `addressesID`, `city`, `state`) VALUES
('90',	'16558',	'Výprachtice',	1,	'',	''),
('90',	'16548',	'Praha',	13,	'',	''),
('90',	'16548',	'Praha',	14,	'',	''),
('90',	'16548',	'Ostrava',	15,	'',	''),
('90',	'16548',	'Praha',	16,	'',	''),
('90',	'16548',	'Praha',	17,	'',	''),
('90',	'16548',	'Praha',	18,	'',	''),
('90',	'16548',	'Praha',	19,	'',	''),
('90',	'16548',	'Praha',	20,	'',	''),
('90',	'16548',	'Praha',	21,	'',	''),
('90',	'16548',	'Praha',	22,	'',	''),
('90',	'16548',	'Praha',	23,	'',	''),
('90',	'16548',	'Praha',	24,	'',	''),
('90',	'16548',	'Praha',	25,	'',	''),
('90',	'16548',	'Praha',	26,	'',	''),
('10',	'12345',	'Lallaa',	27,	'',	''),
('95',	'56141',	'Retuvka',	28,	'Retova',	'Czechia'),
('24',	'57434',	'Habibova',	29,	'Praha',	'Česká republika'),
('224',	'57334',	'Nevimkohova',	30,	'Praha',	'Česká republika'),
('2244',	'52334',	'Nevimkohova',	31,	'Brno',	'Česká republika'),
('223',	'52335',	'Nevihova',	32,	'Brno',	'Česká republika'),
('223',	'52335',	'Nevihova',	33,	'Brno',	'Česká republika'),
('223',	'52335',	'Nevihova',	34,	'Brno',	'Česká republika'),
('359',	'58635',	'Nahodnova',	35,	'Karlovy Vary',	'Česká republika'),
('654',	'85242',	'Hospital street',	36,	'Krimson City',	'United States of America'),
('',	'56141',	'Řetůvka 95',	37,	'Řetová',	'Česko'),
('95',	'56141',	'Řetůvka 95',	38,	'Řetová',	'Česko'),
('654',	'85242',	'Hospital street',	39,	'Krimson City',	'United States of America');

DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applicationsID` int(11) NOT NULL AUTO_INCREMENT,
  `windowsID` int(11) NOT NULL,
  PRIMARY KEY (`applicationsID`),
  UNIQUE KEY `url_windowsID` (`url`,`windowsID`),
  KEY `windowsID` (`windowsID`),
  CONSTRAINT `applications_ibfk_1` FOREIGN KEY (`windowsID`) REFERENCES `windows` (`windowsID`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `applications` (`url`, `applicationsID`, `windowsID`) VALUES
('https://loutiot.cz',	2,	1),
('https://lukaskovar.com',	6,	1),
('https://o7fger.csb.app',	7,	1);

DROP TABLE IF EXISTS `calendars`;
CREATE TABLE `calendars` (
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calendarsID` int(11) NOT NULL AUTO_INCREMENT,
  `portalsID` int(11) DEFAULT NULL,
  PRIMARY KEY (`calendarsID`),
  KEY `portalsID` (`portalsID`),
  CONSTRAINT `calendars_ibfk_1` FOREIGN KEY (`portalsID`) REFERENCES `portals` (`portalsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `calendars` (`name`, `config`, `description`, `type`, `calendarsID`, `portalsID`) VALUES
('Dny jedna rezervace',	'{\"days\":false,\"half\":false,\"rezerveMax\":\"5\",\"style\":{\"day\":{\"disabled\":{\"backgroundColor\":\"#bdbdbd\",\"color\":\"#ffffff\"},\"reserve\":{\"full\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#ff0000\"},\"part\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#ff9742\"}},\"select\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#4d9afe\"}}}}',	'Vyber dny, více rezervací',	'day',	1,	1),
('Dny více rezervací',	'{\"days\":true,\"half\":false,\"rezerveMax\":4,\"style\":{\"day\":{\"disabled\":{\"backgroundColor\":\"rgb(220, 220, 220)\",\"color\":\"white\"},\"reserve\":{\"full\":{\"color\":\"white\",\"backgroundColor\":\"red\"},\"part\":{\"color\":\"white\",\"backgroundColor\":\"orange\"}},\"select\":{\"color\":\"white\",\"backgroundColor\":\"green\"}}}}',	'Vyber dny, více rezervací',	'day',	2,	1),
('dny_a_pul',	'{\r\n\"days\":true,\r\n\"half\":true,\r\n\"rezerveMax\":1,\r\n\"style\":{\r\n	\"day\":{\r\n		\"select\":{\r\n			\"color\": \"white\",\r\n			\"backgroundColor\": \"green\"\r\n		},\r\n		\"reserve\":{\r\n			\"part\":{\r\n				\"color\": \"white\",\r\n				\"backgroundColor\": \"orange\"\r\n			},\r\n			\"full\":{\r\n				\"color\": \"white\",\r\n				\"backgroundColor\": \"red\"\r\n			}\r\n		},\r\n		\"disabled\":{\r\n			\"color\": \"white\",\r\n			\"backgroundColor\": \"rgb(220, 220, 220)\"\r\n		}\r\n	}\r\n}\r\n}',	'Vyber dny a půl',	'day',	3,	1),
('den_vice_rezervaci',	'{\r\n\"days\":false,\r\n\"half\":false,\r\n\"rezerveMax\":3,\r\n\"style\":{\r\n	\"day\":{\r\n		\"select\":{\r\n			\"color\": \"white\",\r\n			\"backgroundColor\": \"green\"\r\n		},\r\n		\"reserve\":{\r\n			\"part\":{\r\n				\"color\": \"white\",\r\n				\"backgroundColor\": \"orange\"\r\n			},\r\n			\"full\":{\r\n				\"color\": \"white\",\r\n				\"backgroundColor\": \"red\"\r\n			}\r\n		},\r\n		\"disabled\":{\r\n			\"color\": \"white\",\r\n			\"backgroundColor\": \"rgb(220, 220, 220)\"\r\n		}\r\n	}\r\n}\r\n}',	'Vyber den, více rezervací',	'day',	4,	1),
('Dny více rezervací dd',	'{\"days\":true,\"half\":false,\"rezerveMax\":4,\"style\":{\"day\":{\"disabled\":{\"backgroundColor\":\"#d4d4d4\",\"color\":\"#000000\"},\"reserve\":{\"full\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#ff0000\"},\"part\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#dfb172\"}},\"select\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#fe7171\"}}}}',	'Vyber dny, více rezervací',	'day',	5,	1),
('fjskdlafjasd',	'{\"days\":false,\"half\":false,\"rezerveMax\":\"2\",\"style\":{\"day\":{\"disabled\":{\"backgroundColor\":\"#ff0000\",\"color\":\"#f5f5f5\"},\"reserve\":{\"full\":{\"color\":\"white\",\"backgroundColor\":\"#f8f7f7\"},\"part\":{\"color\":\"white\",\"backgroundColor\":\"#b31414\"}},\"select\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#36e52a\"}}}}',	'heej',	'day',	16,	1),
('kous',	'{\"days\":false,\"half\":false,\"rezerveMax\":1,\"style\":{\"day\":{\"disabled\":{\"backgroundColor\":\"#bdbdbd\",\"color\":\"#ffffff\"},\"reserve\":{\"full\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#ff0000\"},\"part\":{\"color\":\"white\",\"backgroundColor\":\"#ff9742\"}},\"select\":{\"color\":\"#ffffff\",\"backgroundColor\":\"#4d9afe\"}}}}',	'ss',	'day',	17,	2),
('Hostel UHK, pokoj 1',	'{\"days\":true,\"half\":false,\"rezerveMax\":\"2\",\"style\":{\"day\":{\"disabled\":{\"backgroundColor\":\"#bdbdbd\",\"color\":\"white\"},\"reserve\":{\"full\":{\"color\":\"white\",\"backgroundColor\":\"#ff0000\"},\"part\":{\"color\":\"white\",\"backgroundColor\":\"#d8ff14\"}},\"select\":{\"color\":\"#0040ff\",\"backgroundColor\":\"#f6fa00\"}}}}',	'Tohle je hostel',	'day',	18,	24),
('My awesome calendar',	'{\"days\":false,\"half\":false,\"rezerveMax\":\"5\",\"style\":{\"day\":{\"disabled\":{\"backgroundColor\":\"#bdbdbd\",\"color\":\"white\"},\"reserve\":{\"full\":{\"color\":\"white\",\"backgroundColor\":\"#ff0000\"},\"part\":{\"color\":\"white\",\"backgroundColor\":\"#ff9742\"}},\"select\":{\"color\":\"white\",\"backgroundColor\":\"#4d9afe\"}}}}',	'Be happy, smile',	'day',	19,	25),
('DEMO-calendar',	'{\"days\":true,\"half\":false,\"rezerveMax\":\"5\",\"style\":{\"day\":{\"disabled\":{\"backgroundColor\":\"#bdbdbd\",\"color\":\"white\"},\"reserve\":{\"full\":{\"color\":\"white\",\"backgroundColor\":\"#ff0000\"},\"part\":{\"color\":\"white\",\"backgroundColor\":\"#ff9742\"}},\"select\":{\"color\":\"white\",\"backgroundColor\":\"#4d9afe\"}}}}',	'Demo kalendář k prezentaci funkcí prvku kalendář',	'day',	20,	27);

DROP TABLE IF EXISTS `calendars_exceptions`;
CREATE TABLE `calendars_exceptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `windowsID` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `description` varchar(8000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` json NOT NULL,
  PRIMARY KEY (`id`),
  KEY `windowsID` (`windowsID`),
  CONSTRAINT `calendars_exceptions_ibfk_1` FOREIGN KEY (`windowsID`) REFERENCES `windows` (`windowsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `calendars_exceptions` (`id`, `windowsID`, `date_start`, `date_end`, `time_start`, `time_end`, `description`, `config`) VALUES
(1,	1,	'2022-05-07',	'2022-04-06',	NULL,	NULL,	NULL,	'{\"reason\": \"WTF\", \"allowed\": false}');

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customersID` int(11) NOT NULL AUTO_INCREMENT,
  `addressesID` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`customersID`),
  UNIQUE KEY `email` (`email`),
  KEY `addressesID` (`addressesID`),
  CONSTRAINT `customers_ibfk_3` FOREIGN KEY (`addressesID`) REFERENCES `addresses` (`addressesID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `customers` (`email`, `first_name`, `last_name`, `phone`, `customersID`, `addressesID`, `created_at`, `updated_at`) VALUES
('janickovapata@test.cz',	'Paťa',	'Janíčková',	'730559547',	1,	1,	'2022-03-23 08:51:41',	'2022-03-23 10:49:02'),
('sov@post.cz',	'Viťák',	'Beraník',	'123456789',	5,	NULL,	'2022-03-23 10:07:06',	'2022-03-23 10:07:06'),
('lkov@post.cz',	'Lukáš',	'Kovář',	'730659547',	7,	28,	NULL,	NULL),
('zdenda@post.cz',	'Zdeněk',	'Archleb',	'123456777',	8,	28,	'2022-04-03 12:35:57',	'2022-04-03 12:35:57'),
('zdendak@post.cz',	'Zdeněk',	'Archleb',	'123456777',	10,	28,	'2022-04-03 12:41:51',	'2022-04-03 12:41:51'),
('zdendakuk@post.cz',	'Zdeněk',	'Archleb',	'123456777',	11,	28,	'2022-04-05 16:57:55',	'2022-04-05 16:57:55'),
('zdendaksuk@post.cz',	'Zdeněk',	'Archleb',	'123456777',	12,	28,	'2022-04-05 16:59:25',	'2022-04-05 16:59:25'),
('vitek@post.cz',	'Zdeněk',	'Archleb',	'123456777',	13,	NULL,	'2022-04-05 17:06:49',	'2022-04-05 17:06:49'),
('vitek.beran@seznam.cz',	'Vítezslav',	'Beran',	'731510126',	14,	NULL,	'2022-04-05 17:12:31',	'2022-04-05 17:12:31'),
('ahoj@cau.cz',	'Tom',	'Tomovic',	'734234232',	15,	30,	NULL,	NULL),
('ahoj@caau.cz',	'Tom',	'Tomovic',	'734234232',	17,	34,	NULL,	NULL),
('coolmail@owndomain.com',	'Karel',	'Ztohoven',	'853426852',	18,	35,	NULL,	NULL),
('seb@mobius.com',	'Sebastian',	'Castellanos',	'852962573',	19,	36,	NULL,	NULL),
('lukas@post.cz',	'Lukas',	'Kovvvaaar',	'730659547',	21,	38,	NULL,	NULL),
('lkov@test.cz',	'Lukáš',	'Kovář',	'123456789',	22,	NULL,	'2022-04-19 15:37:43',	'2022-04-19 15:37:43'),
('seba@mobius.com',	'Sebastian',	'Castellanos',	'852962573',	23,	39,	NULL,	NULL),
('neco@neco.cz',	'Petr',	'Neco',	'123456487',	24,	NULL,	'2022-04-19 15:59:03',	'2022-04-19 15:59:03'),
('info@naralamp.cz',	'Lukas',	'Kovar',	'12345679',	25,	NULL,	'2022-05-10 07:05:10',	'2022-05-10 07:05:10'),
('hotline@telnes.cz',	'Kareel',	'Novák',	'12346578',	26,	NULL,	'2022-05-10 07:08:19',	'2022-05-10 07:08:19'),
('test@test.cz',	'Kareel',	'test',	'test',	27,	NULL,	'2022-05-10 07:11:05',	'2022-05-10 07:11:05'),
('janickovapssata@test.cz',	'Paťa',	'Janíčková',	'730559547',	28,	1,	'2022-05-10 08:26:27',	'2022-05-10 08:26:27'),
('janickovapssfjdslkata@test.cz',	'Paťa',	'Janíčková',	'730559547',	29,	1,	'2022-05-10 08:27:51',	'2022-05-10 08:27:51'),
('pata@janicka.cz',	'Lukas',	'Kovar',	'123456',	30,	1,	'2022-05-10 08:30:30',	'2022-05-10 08:30:30'),
('apssfjdslkata@test.cz',	'Paťa',	'Janíčková',	'730559547',	31,	1,	'2022-05-10 08:32:44',	'2022-05-10 08:32:44'),
('nevim@gmail.com',	'Ar',	'Zd',	'852645987',	32,	1,	'2022-05-10 10:19:01',	'2022-05-10 10:19:01'),
('ty@ty.cz',	'qw',	'er',	'465156514',	33,	1,	'2022-05-10 10:45:13',	'2022-05-10 10:45:13'),
('demouser@ourdemo.cz',	'Demo',	'Demovicz',	'753951852',	34,	1,	'2022-05-10 11:00:30',	'2022-05-10 11:00:30');

DROP VIEW IF EXISTS `CustWithAddr`;
CREATE TABLE `CustWithAddr` (`customersID` int(11), `email` varchar(50), `first_name` varchar(50), `last_name` varchar(50), `phone` varchar(13), `street` varchar(50), `number` varchar(10), `city` varchar(100), `postal_code` varchar(30), `state` varchar(100));


DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `capacity` int(11) NOT NULL,
  `date_end` date NOT NULL,
  `date_start` date NOT NULL,
  `description` varchar(999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(10,2) DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `eventsID` int(11) NOT NULL AUTO_INCREMENT,
  `addressesID` int(11) DEFAULT NULL,
  `windowsID` int(11) DEFAULT NULL,
  `portalsID` int(11) NOT NULL,
  PRIMARY KEY (`eventsID`),
  KEY `FK_events_windows` (`windowsID`),
  KEY `addressesID` (`addressesID`),
  KEY `portalsID` (`portalsID`),
  CONSTRAINT `FK_events_windows` FOREIGN KEY (`windowsID`) REFERENCES `windows` (`windowsID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `events_ibfk_1` FOREIGN KEY (`addressesID`) REFERENCES `addresses` (`addressesID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `events_ibfk_2` FOREIGN KEY (`portalsID`) REFERENCES `portals` (`portalsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `events` (`capacity`, `date_end`, `date_start`, `description`, `name`, `price`, `time_end`, `time_start`, `eventsID`, `addressesID`, `windowsID`, `portalsID`) VALUES
(50,	'2022-05-09',	'2022-05-08',	'adasda',	'Pokus',	200.00,	'18:57:00',	'17:57:00',	4,	NULL,	1,	1),
(10,	'2022-05-10',	'2022-05-09',	'vítkova událost',	'událost 1',	200.00,	'22:19:00',	'21:19:00',	5,	NULL,	1,	2),
(68,	'2022-05-17',	'2022-05-10',	'Smile and be happy ^^',	'Happy day',	200.00,	'12:21:00',	'12:21:00',	6,	NULL,	1,	25);

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `forms`;
CREATE TABLE `forms` (
  `config` json DEFAULT NULL,
  `description` varchar(999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formsID` int(11) NOT NULL AUTO_INCREMENT,
  `portalsID` int(11) NOT NULL,
  PRIMARY KEY (`formsID`),
  UNIQUE KEY `name` (`name`),
  KEY `portalsID` (`portalsID`),
  CONSTRAINT `forms_ibfk_1` FOREIGN KEY (`portalsID`) REFERENCES `portals` (`portalsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `forms` (`config`, `description`, `name`, `formsID`, `portalsID`) VALUES
('[]',	'dsfsdfsfdssdfs f',	'Ubytkkoh',	1,	1),
('[{\"key\": \"email\", \"text\": {\"name\": \"email\", \"readable\": true, \"helperText\": \"vložte email\"}, \"type\": \"text\", \"style\": {\"width\": 50}}, {\"key\": \"dospeli\", \"type\": \"number\", \"style\": {\"width\": 50}, \"number\": {\"max\": 3, \"min\": 0, \"name\": \"Dospělí\", \"helperText\": \"Zadejte počet dospělých\", \"defaultValue\": 2}}, {\"key\": \"deti\", \"type\": \"number\", \"style\": {\"width\": 50}, \"number\": {\"max\": 5, \"min\": 0, \"name\": \"Děti\", \"helperText\": \"Zadejte počet dětí\", \"defaultValue\": 0}}, {\"key\": \"cislo\", \"date\": {\"date\": \"2021-10-21\", \"name\": \"Číslo\"}, \"type\": \"date\", \"style\": {\"width\": 50}}]',	'nějaký popis ale nic mě nenapadá',	'Kancelář',	2,	1),
('[]',	'vítek form',	'form1',	15,	2),
('[{\"key\": \"email\", \"text\": {\"name\": \"email\", \"readable\": true, \"helperText\": \"vložte email\"}, \"type\": \"text\", \"style\": {\"width\": 50}}]',	'formik',	'UHK form',	16,	24),
('[{\"key\": \"nalada\", \"text\": {\"name\": \"Jak se máš? :)\", \"readable\": true, \"helperText\": \"Napište vaši náladu\"}, \"type\": \"text\", \"style\": {\"width\": 50}}]',	'Very nice form',	'NewAwesomeForm',	17,	25),
('[{\"key\": \"email\", \"text\": {\"name\": \"email\", \"readable\": true, \"helperText\": \"vložte email\"}, \"type\": \"text\", \"style\": {\"width\": 50}}, {\"key\": \"email2\", \"text\": {\"name\": \"email\", \"readable\": true, \"helperText\": \"vložte email\"}, \"type\": \"text\", \"style\": {\"width\": 50}}]',	'testiiiik',	'testik',	18,	25),
('[{\"key\": \"email\", \"text\": {\"name\": \"email\", \"readable\": true, \"helperText\": \"vložte email\"}, \"type\": \"text\", \"style\": {\"width\": 50}}, {\"key\": \"mobil\", \"text\": {\"name\": \"mobil\", \"readable\": true, \"helperText\": \"vložte mobil\"}, \"type\": \"text\", \"style\": {\"width\": 50}}]',	'Demo formulář k prezentaci prvku formulář',	'DEMO-form',	19,	27);

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1,	'App\\Models\\User',	3,	'test',	'968ab70cb77f57f0d637fbdb25fdaa9d10fbd72d769175439e20912d9935b367',	'[\"*\"]',	NULL,	'2022-04-06 09:06:54',	'2022-04-06 09:06:54'),
(2,	'App\\Models\\User',	3,	'test',	'71c43e57f6cf17771e4ac65e2243326e5e0923953110438deae4480f770e80be',	'[\"*\"]',	NULL,	'2022-04-06 09:06:57',	'2022-04-06 09:06:57'),
(3,	'App\\Models\\User',	3,	'test',	'b763ce46d7413202bbcb22f28f943a0e6c821ab5171a7aa3fd4ebf7560efa3aa',	'[\"*\"]',	NULL,	'2022-04-06 09:06:59',	'2022-04-06 09:06:59'),
(4,	'App\\Models\\User',	3,	'test',	'47532fc86e65a49173ed51837b401955dbd8bc6ff44b48ea4c8ed753cb5b8fa7',	'[\"*\"]',	NULL,	'2022-04-06 09:07:00',	'2022-04-06 09:07:00'),
(5,	'App\\Models\\User',	3,	'test',	'0699264414f7518320ed22b69811f64b9329899893d18d28525bc98e20f4849d',	'[\"*\"]',	NULL,	'2022-04-06 09:07:02',	'2022-04-06 09:07:02'),
(6,	'App\\Models\\User',	3,	'test',	'0c037b403a2a7c2af31e608e16ca299d4e93e715d94067e80c82c0174b38aada',	'[\"*\"]',	'2022-05-10 11:32:44',	'2022-04-06 09:08:28',	'2022-05-10 11:32:44'),
(7,	'App\\Models\\User',	3,	'test',	'f7af36f779bf50ef15b107065039bb26532cca8e9a5b041fcb450f49a4195abd',	'[\"*\"]',	NULL,	'2022-04-06 09:09:41',	'2022-04-06 09:09:41'),
(8,	'App\\Models\\User',	3,	'test',	'901abcba9c4b12be303e0f3a5a05bab199c45f4014ad6ef4d97caca879f0d66d',	'[\"*\"]',	NULL,	'2022-04-06 09:11:29',	'2022-04-06 09:11:29'),
(9,	'App\\Models\\User',	3,	'test',	'7cb146123ff5c35c1e0d79d84b6a36a385249a9cbff1cda3d2d149410e33e58a',	'[\"*\"]',	NULL,	'2022-04-06 09:15:11',	'2022-04-06 09:15:11'),
(10,	'App\\Models\\User',	3,	'test',	'ef0830ef6624febb2243b2e64de61aedbac5f0a79c74a10da5ccde570259fb84',	'[\"*\"]',	NULL,	'2022-04-06 09:15:24',	'2022-04-06 09:15:24'),
(11,	'App\\Models\\User',	3,	'test',	'9b901cf7a6f07d0ae75e2345a3639f83eafa7bc2963d95dcac5bf3b21be54b9f',	'[\"*\"]',	NULL,	'2022-04-25 16:57:16',	'2022-04-25 16:57:16'),
(12,	'App\\Models\\User',	3,	'test',	'4e61fdf1f6ffaf7d14990e0586ef3c6d65d56c4c2c51898e2b124802dfabc40d',	'[\"*\"]',	NULL,	'2022-04-25 16:59:27',	'2022-04-25 16:59:27'),
(13,	'App\\Models\\User',	3,	'test',	'f0adbb2aa320c9a2782563a9427e24102e26cabb4067eb23dd5c7cd9ba44fafc',	'[\"*\"]',	NULL,	'2022-04-25 16:59:59',	'2022-04-25 16:59:59'),
(14,	'App\\Models\\User',	3,	'test',	'5b709e70c9dd073623caf1ca5aebc505f135033e09c532f134fbae807fb8f0a5',	'[\"*\"]',	NULL,	'2022-04-25 17:00:34',	'2022-04-25 17:00:34'),
(15,	'App\\Models\\User',	3,	'test',	'3a2821eab8a7d413de464e0f37b657922a11f28b10f16445d16eb822704f2f9c',	'[\"*\"]',	NULL,	'2022-04-25 17:04:28',	'2022-04-25 17:04:28'),
(16,	'App\\Models\\User',	3,	'test',	'8ba7c3542d231e77e3f38df52b3d848f43a54ffb1e518a4cd747a313d52b3733',	'[\"*\"]',	NULL,	'2022-04-25 17:13:38',	'2022-04-25 17:13:38'),
(17,	'App\\Models\\User',	3,	'test',	'b6e8f58c2ecb01684d7458164483c3d96da6f34dd33370dff992804fad21e099',	'[\"*\"]',	NULL,	'2022-04-25 17:16:20',	'2022-04-25 17:16:20'),
(18,	'App\\Models\\User',	3,	'test',	'0abd0ec8a9b3486080f1f3db03616f609f2f9eb76c483caa8ae7bd29cc402c47',	'[\"*\"]',	NULL,	'2022-04-25 17:16:34',	'2022-04-25 17:16:34'),
(19,	'App\\Models\\User',	3,	'test',	'6c6de751ae423c447149091d5f67968bc8751e5ceec0d4bacafe88540ea1b4b5',	'[\"*\"]',	NULL,	'2022-04-25 17:19:09',	'2022-04-25 17:19:09'),
(20,	'App\\Models\\User',	3,	'test',	'e2e522791b30a1acb2d3a5df17254a46710be4ec730f330ebbb3a0b7b44ff056',	'[\"*\"]',	NULL,	'2022-04-25 17:19:56',	'2022-04-25 17:19:56'),
(21,	'App\\Models\\User',	3,	't2est',	'33d9e990c8a8991d217dcacb919f66e7d286118b1768b033b3a6ca4d9e64c7d9',	'[\"*\"]',	NULL,	'2022-04-25 17:20:16',	'2022-04-25 17:20:16'),
(22,	'App\\Models\\User',	3,	't2est',	'2f4657f7a2f4ae54293fd5bf4c2eaa65bd950884d94d300eaf1683b738979fdb',	'[\"*\"]',	NULL,	'2022-04-27 08:26:47',	'2022-04-27 08:26:47'),
(23,	'App\\Models\\User',	3,	'web',	'0c425cbefe109635a1009484918f8e446dc3d574d7e5652e7e42e275baa56064',	'[\"*\"]',	NULL,	'2022-04-27 08:27:57',	'2022-04-27 08:27:57'),
(24,	'App\\Models\\User',	3,	'web',	'4942abc8d2da5ddbf5cba268ac87f0a8b3b03fa27e1b09016a10a94929624a59',	'[\"*\"]',	NULL,	'2022-04-27 08:28:14',	'2022-04-27 08:28:14'),
(25,	'App\\Models\\User',	3,	'test',	'b5d9867c4bd44815c97027b806c6678f2b3d7cc838c1e2d8270603b197db69ba',	'[\"*\"]',	NULL,	'2022-04-27 08:31:59',	'2022-04-27 08:31:59'),
(26,	'App\\Models\\User',	2,	'web',	'9df7243e6a3d957a167e0324fe68873a03115ea5f30e691b5e86fbd459fcb4b3',	'[\"*\"]',	'2022-05-08 10:24:24',	'2022-04-30 11:24:12',	'2022-05-08 10:24:24'),
(27,	'App\\Models\\User',	2,	'web',	'3a21714538d70581ea09f0117d5ee18984aea22bb7758b9a5752bf338da256ff',	'[\"*\"]',	'2022-05-08 18:02:51',	'2022-05-02 15:54:42',	'2022-05-08 18:02:51'),
(28,	'App\\Models\\User',	2,	'web',	'7d41f02a2a835093a9ea570058dc4e57375a3294be8f4fa0dc98a41de12a243c',	'[\"*\"]',	'2022-05-08 16:11:38',	'2022-05-08 10:24:44',	'2022-05-08 16:11:38'),
(29,	'App\\Models\\User',	2,	'web',	'3b3a4df9e484220149818a7cc840847e0606a00e0621c34a406a86de149dd36e',	'[\"*\"]',	'2022-05-09 06:22:58',	'2022-05-08 14:44:26',	'2022-05-09 06:22:58'),
(30,	'App\\Models\\User',	1,	'web',	'57c1549e98f160ddd781aea80f3952a51a40ba5995c9a48e1f007edfeadf8a03',	'[\"*\"]',	NULL,	'2022-05-08 16:32:52',	'2022-05-08 16:32:52'),
(31,	'App\\Models\\User',	1,	'web',	'69ce74347f31dd0976f38e9b2a431131e957aba58cd13e7428238f2c92464816',	'[\"*\"]',	'2022-05-08 17:20:07',	'2022-05-08 16:35:05',	'2022-05-08 17:20:07'),
(32,	'App\\Models\\User',	1,	'web',	'8e4aa020c54dedf320bcfa1445b7112eb885541de1c0c7706fd5a73ab0fd524f',	'[\"*\"]',	'2022-05-08 17:16:26',	'2022-05-08 17:06:38',	'2022-05-08 17:16:26'),
(33,	'App\\Models\\User',	1,	'web',	'4df27a18cc0008c732dd02663f8abff565e1547e8d369b8dfd1fbb78418d67ff',	'[\"*\"]',	'2022-05-09 16:59:17',	'2022-05-08 17:16:54',	'2022-05-09 16:59:17'),
(34,	'App\\Models\\User',	2,	'web',	'bfe6037b27fbcf9b090a39fa65caf3f3839cdc8e253914d0d53347fcf28fb199',	'[\"*\"]',	'2022-05-08 18:15:46',	'2022-05-08 18:15:39',	'2022-05-08 18:15:46'),
(35,	'App\\Models\\User',	2,	'web',	'c4f14e2e1cd6a600e952663308557226a1139c05dc08ec67b0e2181459d8b34e',	'[\"*\"]',	'2022-05-09 13:42:10',	'2022-05-08 18:15:42',	'2022-05-09 13:42:10'),
(36,	'App\\Models\\User',	1,	'web',	'0a4888897c3d2749e677cf809fc0055c1352f4ca7f7d475f8a49440e183461bd',	'[\"*\"]',	'2022-05-10 11:34:29',	'2022-05-08 19:06:40',	'2022-05-10 11:34:29'),
(37,	'App\\Models\\User',	2,	'web',	'31924eb4bfa6ec3330e73959f4b53e37f55c3d715f447fee2dc180924cf9c6aa',	'[\"*\"]',	NULL,	'2022-05-09 06:23:43',	'2022-05-09 06:23:43'),
(38,	'App\\Models\\User',	2,	'web',	'd42600f66c57c46a104298f95015ae700594ee991c226728244866eb0f39dbe4',	'[\"*\"]',	NULL,	'2022-05-09 10:13:04',	'2022-05-09 10:13:04'),
(39,	'App\\Models\\User',	2,	'web',	'a2381aabcbcc03ebdfe83ebaa2df826de8334b76a66383e6d39b653263251189',	'[\"*\"]',	'2022-05-10 06:00:46',	'2022-05-09 10:16:19',	'2022-05-10 06:00:46'),
(40,	'App\\Models\\User',	2,	'web',	'0aaa2579400e626e387a680731f181627de1efe56d785e25784d9ec96216a2f2',	'[\"*\"]',	'2022-05-09 14:07:43',	'2022-05-09 13:50:44',	'2022-05-09 14:07:43'),
(41,	'App\\Models\\User',	2,	'web',	'96899d5993c3f13349300b963dd6562027d8dffed8849f2370ed001ab32a851c',	'[\"*\"]',	'2022-05-09 14:35:15',	'2022-05-09 14:12:16',	'2022-05-09 14:35:15'),
(42,	'App\\Models\\User',	2,	'web',	'9fe630e80e37022ee8bc0a74e96d50cc367383ead7a767e187ed4163b4d02625',	'[\"*\"]',	'2022-05-09 14:43:00',	'2022-05-09 14:35:27',	'2022-05-09 14:43:00'),
(43,	'App\\Models\\User',	2,	'web',	'16c15eca3187aa23bc49656443cc8a73217f2a4fc5d59c15ef2509aa1459ff13',	'[\"*\"]',	'2022-05-10 10:02:54',	'2022-05-09 14:44:29',	'2022-05-10 10:02:54'),
(44,	'App\\Models\\User',	2,	'web',	'63fdd822a5390b7682b5f07151979c7fd79d31078897e0563fe37d538518541e',	'[\"*\"]',	'2022-05-09 14:47:59',	'2022-05-09 14:47:18',	'2022-05-09 14:47:59'),
(45,	'App\\Models\\User',	2,	'web',	'd6672d92b5d98ca9ea85c4c9f335c5ff8f7478bb77b24d9a085ab8c251dffd66',	'[\"*\"]',	'2022-05-09 14:53:00',	'2022-05-09 14:48:09',	'2022-05-09 14:53:00'),
(46,	'App\\Models\\User',	2,	'web',	'824f7f0368f252f11ff5a7efe41f7eb0e7cdb4679eb18824276fae496ad1d440',	'[\"*\"]',	'2022-05-09 15:28:07',	'2022-05-09 14:53:14',	'2022-05-09 15:28:07'),
(47,	'App\\Models\\User',	1,	'web',	'3520fd5c84c70dc3f8a27a0e1836d91aae5b626d9a2a50ae237e1528537cffc3',	'[\"*\"]',	'2022-05-09 19:46:27',	'2022-05-09 15:43:47',	'2022-05-09 19:46:27'),
(48,	'App\\Models\\User',	1,	'web',	'a7e9db8dc164b04f36b4d434b19fe22b65598bd3e469ce40bc87284dfcd605eb',	'[\"*\"]',	'2022-05-09 17:16:03',	'2022-05-09 17:10:49',	'2022-05-09 17:16:03'),
(49,	'App\\Models\\User',	2,	'web',	'92e4a3362287b6edbd7eb41438a513daa43785c614a4d2fb548e2ac6527ebff4',	'[\"*\"]',	'2022-05-09 19:19:43',	'2022-05-09 19:16:55',	'2022-05-09 19:19:43'),
(50,	'App\\Models\\User',	2,	'web',	'281e44b8f406805342368305a04cc73929564da94357fdcb8dc0f8d7a172504c',	'[\"*\"]',	'2022-05-10 05:08:23',	'2022-05-09 19:36:53',	'2022-05-10 05:08:23'),
(51,	'App\\Models\\User',	1,	'web',	'5fe14de97321576d4b1541969640cddbdda63df1eadbf21a1bf67aa62984e3d7',	'[\"*\"]',	'2022-05-10 05:24:41',	'2022-05-10 05:01:46',	'2022-05-10 05:24:41'),
(52,	'App\\Models\\User',	2,	'web',	'fbdda8d1cdb4a45968f612b678f6686290edf8dcceba1343262105dcc2fa7ea8',	'[\"*\"]',	'2022-05-10 05:30:43',	'2022-05-10 05:23:12',	'2022-05-10 05:30:43'),
(53,	'App\\Models\\User',	2,	'web',	'3b122e9b29f16cb790b69e9c11a554d272d1c5e04555448a8a70dfa45ab1b998',	'[\"*\"]',	'2022-05-10 05:47:20',	'2022-05-10 05:41:53',	'2022-05-10 05:47:20'),
(54,	'App\\Models\\User',	2,	'web',	'5c9cdcafcff6f195987d965893c6e7d78ae5a97f49d27fde1b2df560dadc7989',	'[\"*\"]',	'2022-05-10 06:01:26',	'2022-05-10 06:01:23',	'2022-05-10 06:01:26'),
(55,	'App\\Models\\User',	2,	'web',	'c7b76402443fdc17b31c07c07a5ddc22e6ba5e137eca80c4fc7f7edf2d15e6d3',	'[\"*\"]',	'2022-05-10 06:02:02',	'2022-05-10 06:01:55',	'2022-05-10 06:02:02'),
(56,	'App\\Models\\User',	2,	'web',	'87f4ffc6b196c6cb560a88b4952679959ff3e0bcbd8d16140e581f4c09434b51',	'[\"*\"]',	'2022-05-10 06:10:06',	'2022-05-10 06:10:01',	'2022-05-10 06:10:06'),
(57,	'App\\Models\\User',	1,	'web',	'bffc35cbb841e1c7e6d620be2dc43bc7893392db44a8cbccc9773f1b0e2983b9',	'[\"*\"]',	'2022-05-10 06:52:13',	'2022-05-10 06:14:42',	'2022-05-10 06:52:13'),
(58,	'App\\Models\\User',	20,	'web',	'8ccc0db9d980af83a0b834b9fee62efb7e6bfda54dc828cf3731b7e29342d826',	'[\"*\"]',	'2022-05-10 09:44:20',	'2022-05-10 06:55:40',	'2022-05-10 09:44:20'),
(59,	'App\\Models\\User',	1,	'web',	'bb5b100cd210fdcd8436f45bf7086044ed141aa390b3b23a9293b14338aad872',	'[\"*\"]',	'2022-05-10 10:24:01',	'2022-05-10 09:44:45',	'2022-05-10 10:24:01'),
(60,	'App\\Models\\User',	2,	'web',	'09bfde7c50d4a654ca5c8f311ffc26c13d4012a4645773e690d69d9ec5c9a9b8',	'[\"*\"]',	'2022-05-10 10:18:02',	'2022-05-10 10:03:52',	'2022-05-10 10:18:02'),
(61,	'App\\Models\\User',	6,	'web',	'16b596f16bd21d05a72aca1475cb31ee2f16a0abfb9eb078a554176dfbad9e97',	'[\"*\"]',	'2022-05-10 10:04:28',	'2022-05-10 10:04:26',	'2022-05-10 10:04:28'),
(62,	'App\\Models\\User',	6,	'web',	'1d810159e196551e60685c81338a83e414c5e17ad7edb41e7c2805937fa7f930',	'[\"*\"]',	'2022-05-10 10:51:54',	'2022-05-10 10:04:28',	'2022-05-10 10:51:54'),
(63,	'App\\Models\\User',	21,	'web',	'a1141f8e5323a0e115e5c39be746f93ed8c964e79af5db7590ed33f83aae9ef7',	'[\"*\"]',	'2022-05-10 11:12:38',	'2022-05-10 10:37:06',	'2022-05-10 11:12:38'),
(64,	'App\\Models\\User',	21,	'web',	'181a8d8c64eb170f478111a1fd041bfef28273d34d79b157c5fd945524ae2ebc',	'[\"*\"]',	'2022-05-10 11:02:47',	'2022-05-10 10:52:30',	'2022-05-10 11:02:47'),
(65,	'App\\Models\\User',	21,	'web',	'bdf3f8c1ce11aac7ed8626177fb659b36184becbccb5b263dad1c30b7ee57d97',	'[\"*\"]',	'2022-05-10 11:09:51',	'2022-05-10 11:03:23',	'2022-05-10 11:09:51'),
(66,	'App\\Models\\User',	21,	'web',	'e2bcf5a459d6e2f64112e292bdc6c5fd11d801ebcdbc43d133a600a63dfb2fbe',	'[\"*\"]',	'2022-05-10 11:12:39',	'2022-05-10 11:05:50',	'2022-05-10 11:12:39'),
(67,	'App\\Models\\User',	21,	'web',	'fc519209383e1fd0d04427517f3643db79bd764748cff96ccbe5242bb257f0a9',	'[\"*\"]',	'2022-05-10 11:11:11',	'2022-05-10 11:11:08',	'2022-05-10 11:11:11'),
(68,	'App\\Models\\User',	22,	'web',	'7506ec5ee51a577421b302fa41a6c5df1048feb4d09893b24db9c91734239fa3',	'[\"*\"]',	'2022-05-10 11:13:38',	'2022-05-10 11:13:36',	'2022-05-10 11:13:38'),
(69,	'App\\Models\\User',	22,	'web',	'728b99e2990325e8fce7e3c829d659d25451be01f0a3211db911785d7a71618e',	'[\"*\"]',	'2022-05-10 11:13:54',	'2022-05-10 11:13:45',	'2022-05-10 11:13:54'),
(70,	'App\\Models\\User',	22,	'web',	'd00bdc0ab5e85027e02c627fdbb4c2b2d997472e7d97c22300e5800246c508bb',	'[\"*\"]',	'2022-05-10 11:14:05',	'2022-05-10 11:14:03',	'2022-05-10 11:14:05'),
(71,	'App\\Models\\User',	22,	'web',	'0f992f23609ed0a6bbbd8b3feccf8a1987f897399f5ee391d673615c543d32a7',	'[\"*\"]',	'2022-05-10 11:18:05',	'2022-05-10 11:18:03',	'2022-05-10 11:18:05'),
(72,	'App\\Models\\User',	22,	'web',	'26aa8a06a1cec9e0bac787f1f3d94dd586f5f4376e773fd236437f2913dd652c',	'[\"*\"]',	'2022-05-10 11:28:18',	'2022-05-10 11:22:24',	'2022-05-10 11:28:18'),
(73,	'App\\Models\\User',	22,	'web',	'f800a45fe4b2b6b0957c7820a750d2daecda1d2c858fb60f48f13ded1ef70879',	'[\"*\"]',	'2022-05-10 11:29:18',	'2022-05-10 11:28:42',	'2022-05-10 11:29:18'),
(74,	'App\\Models\\User',	21,	'web',	'75ccc28861606a0c0323538f0f87c52de8d1f13e123647d19e969e4f816c6ee2',	'[\"*\"]',	'2022-05-10 11:29:53',	'2022-05-10 11:29:36',	'2022-05-10 11:29:53'),
(75,	'App\\Models\\User',	21,	'web',	'f963e39f6b32ab210e7a15ea79650fa70762c1daaa2bfbed3358a9ed1aafc26f',	'[\"*\"]',	'2022-05-10 11:31:18',	'2022-05-10 11:30:58',	'2022-05-10 11:31:18'),
(76,	'App\\Models\\User',	22,	'web',	'55f520ed9200c9b8b2edf322a6b987af9ae12f61fcff604d68071552ca57b77d',	'[\"*\"]',	'2022-05-10 11:32:15',	'2022-05-10 11:32:04',	'2022-05-10 11:32:15'),
(77,	'App\\Models\\User',	1,	'web',	'6fb0f5a7314225954433ff21020dba0580e36770e3a0459c55a4005a894e7a78',	'[\"*\"]',	'2022-05-10 11:38:10',	'2022-05-10 11:32:21',	'2022-05-10 11:38:10'),
(78,	'App\\Models\\User',	21,	'web',	'6682d5b6dec76e9bb0be521e53c501221d2616797423530230ea05aff71e0cf1',	'[\"*\"]',	'2022-05-10 11:38:33',	'2022-05-10 11:35:13',	'2022-05-10 11:38:33'),
(79,	'App\\Models\\User',	21,	'web',	'6827163fadd2f6094df6feb0484733277c70a9e5498994051b02784a8f3feeed',	'[\"*\"]',	'2022-05-10 11:38:23',	'2022-05-10 11:38:21',	'2022-05-10 11:38:23'),
(80,	'App\\Models\\User',	21,	'web',	'589d8faaf618a91c3fff85d016b99a93a0bbf0bd36d3e38757ae7f9b0d357c14',	'[\"*\"]',	'2022-05-10 11:38:34',	'2022-05-10 11:38:32',	'2022-05-10 11:38:34'),
(81,	'App\\Models\\User',	21,	'web',	'd44b300d019832acddbde52a34e3222128b6ffb9bfe6cd62072c639f6741fbb0',	'[\"*\"]',	'2022-05-10 11:42:26',	'2022-05-10 11:38:46',	'2022-05-10 11:42:26'),
(82,	'App\\Models\\User',	21,	'web',	'08e5b1502c6f6515f2bdb3c111260720a134ca45ebc65c366a36c37cadfa6bf1',	'[\"*\"]',	NULL,	'2022-05-10 11:42:32',	'2022-05-10 11:42:32');

DROP TABLE IF EXISTS `portals`;
CREATE TABLE `portals` (
  `portalsID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`portalsID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `portals` (`portalsID`, `name`) VALUES
(27,	'DEMO-Portal'),
(28,	'Hotel UHK'),
(25,	'MyAwesomePortal'),
(16,	'nasrat'),
(15,	'pokus'),
(2,	'Test'),
(1,	'Úbytko Vítek'),
(24,	'UHK');

DROP TABLE IF EXISTS `portalusers`;
CREATE TABLE `portalusers` (
  `portalusersID` int(11) NOT NULL AUTO_INCREMENT,
  `portalsID` int(11) NOT NULL,
  `usersID` int(11) NOT NULL,
  `roleID` tinyint(4) NOT NULL,
  PRIMARY KEY (`portalusersID`),
  UNIQUE KEY `usersID_portalsID` (`usersID`,`portalsID`),
  KEY `FK_portalusers_portals` (`portalsID`),
  KEY `FK_portalusers_users` (`usersID`),
  KEY `roleID` (`roleID`),
  CONSTRAINT `portalusers_ibfk_1` FOREIGN KEY (`roleID`) REFERENCES `roles` (`roleID`),
  CONSTRAINT `portalusers_ibfk_3` FOREIGN KEY (`portalsID`) REFERENCES `portals` (`portalsID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `portalusers_ibfk_4` FOREIGN KEY (`usersID`) REFERENCES `users` (`usersID`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `portalusers` (`portalusersID`, `portalsID`, `usersID`, `roleID`) VALUES
(1,	1,	1,	1),
(2,	1,	3,	1),
(3,	2,	3,	1),
(6,	2,	2,	1),
(7,	15,	2,	1),
(8,	16,	2,	1),
(17,	24,	20,	1),
(18,	25,	6,	1),
(20,	27,	21,	1),
(21,	27,	22,	2);

DROP TABLE IF EXISTS `reservations`;
CREATE TABLE `reservations` (
  `data` json DEFAULT NULL,
  `date_end` date NOT NULL,
  `date_start` date NOT NULL,
  `time_end` time DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `reservationsID` int(11) NOT NULL AUTO_INCREMENT,
  `customersID` int(11) DEFAULT NULL,
  `windowsID` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `update_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`reservationsID`),
  KEY `FK_reservations_customers` (`customersID`),
  KEY `FK_reservations_windows` (`windowsID`),
  CONSTRAINT `FK_reservations_customers` FOREIGN KEY (`customersID`) REFERENCES `customers` (`customersID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_reservations_windows` FOREIGN KEY (`windowsID`) REFERENCES `windows` (`windowsID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `reservations` (`data`, `date_end`, `date_start`, `time_end`, `time_start`, `reservationsID`, `customersID`, `windowsID`, `created_at`, `updated_at`, `update_token`) VALUES
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	50,	NULL,	1,	'2022-04-17 15:07:06',	'2022-04-17 15:07:06',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-21',	'2022-04-21',	'00:00:00',	'00:00:00',	51,	NULL,	1,	'2022-04-17 15:10:14',	'2022-04-17 15:10:14',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	52,	NULL,	4,	'2022-04-17 15:10:58',	'2022-04-17 15:10:58',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	53,	NULL,	4,	'2022-04-17 15:14:35',	'2022-04-17 15:14:35',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-03-06',	'2022-03-06',	'00:00:00',	'00:00:00',	54,	NULL,	4,	'2022-04-17 15:15:02',	'2022-04-17 15:15:02',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-08',	'2022-04-08',	'00:00:00',	'00:00:00',	55,	NULL,	4,	'2022-04-17 15:18:43',	'2022-04-17 15:18:43',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-14',	'2022-03-30',	'00:00:00',	'00:00:00',	56,	NULL,	2,	'2022-04-17 15:19:20',	'2022-04-17 15:19:20',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-08',	'2022-04-06',	'00:00:00',	'00:00:00',	57,	NULL,	5,	'2022-04-17 15:19:50',	'2022-04-17 15:19:50',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	59,	NULL,	4,	'2022-04-17 17:13:54',	'2022-04-17 17:13:54',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"neco@neco.cz\\\"}\"',	'2022-04-08',	'2022-04-08',	'00:00:00',	'00:00:00',	61,	NULL,	4,	'2022-04-19 15:59:03',	'2022-04-19 15:59:03',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"loutaci@loutaci.cz\\\"}\"',	'2022-04-14',	'2022-04-14',	'00:00:00',	'00:00:00',	62,	NULL,	4,	'2022-04-25 11:04:17',	'2022-04-25 11:04:17',	NULL),
('\"{}\"',	'2022-05-05',	'2022-05-05',	'00:00:00',	'00:00:00',	67,	NULL,	1,	'2022-05-10 07:55:40',	'2022-05-10 07:55:40',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	70,	NULL,	1,	'2022-05-10 08:00:07',	'2022-05-10 08:00:07',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	71,	NULL,	1,	'2022-05-10 08:00:37',	'2022-05-10 08:00:37',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	72,	NULL,	1,	'2022-05-10 08:00:49',	'2022-05-10 08:00:49',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	73,	1,	1,	'2022-05-10 08:01:04',	'2022-05-10 08:01:04',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	74,	NULL,	1,	'2022-05-10 08:01:32',	'2022-05-10 08:01:32',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	75,	NULL,	1,	'2022-05-10 08:01:47',	'2022-05-10 08:01:47',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	76,	NULL,	1,	'2022-05-10 08:02:06',	'2022-05-10 08:02:06',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	77,	NULL,	1,	'2022-05-10 08:02:13',	'2022-05-10 08:02:13',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	78,	NULL,	1,	'2022-05-10 08:02:29',	'2022-05-10 08:02:29',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	79,	NULL,	1,	'2022-05-10 08:02:36',	'2022-05-10 08:02:36',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	80,	NULL,	1,	'2022-05-10 08:03:02',	'2022-05-10 08:03:02',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	81,	NULL,	1,	'2022-05-10 08:03:22',	'2022-05-10 08:03:22',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	82,	NULL,	1,	'2022-05-10 08:03:32',	'2022-05-10 08:03:32',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	83,	NULL,	1,	'2022-05-10 08:03:38',	'2022-05-10 08:03:38',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	84,	NULL,	1,	'2022-05-10 08:04:07',	'2022-05-10 08:04:07',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	85,	NULL,	1,	'2022-05-10 08:04:25',	'2022-05-10 08:04:25',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	86,	NULL,	1,	'2022-05-10 08:04:33',	'2022-05-10 08:04:33',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	87,	NULL,	1,	'2022-05-10 08:04:42',	'2022-05-10 08:04:42',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	88,	NULL,	1,	'2022-05-10 08:13:41',	'2022-05-10 08:13:41',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	89,	NULL,	1,	'2022-05-10 08:14:10',	'2022-05-10 08:14:10',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	90,	NULL,	1,	'2022-05-10 08:26:27',	'2022-05-10 08:26:27',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	91,	28,	1,	'2022-05-10 08:27:28',	'2022-05-10 08:27:28',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	92,	29,	1,	'2022-05-10 08:27:51',	'2022-05-10 08:27:51',	NULL),
('\"{}\"',	'2022-05-18',	'2022-05-18',	'00:00:00',	'00:00:00',	93,	NULL,	1,	'2022-05-10 08:28:23',	'2022-05-10 08:28:23',	NULL),
('\"{}\"',	'2022-05-18',	'2022-05-18',	'00:00:00',	'00:00:00',	94,	NULL,	1,	'2022-05-10 08:30:30',	'2022-05-10 08:30:30',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	95,	29,	1,	'2022-05-10 08:31:48',	'2022-05-10 08:31:48',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	96,	29,	1,	'2022-05-10 08:32:03',	'2022-05-10 08:32:03',	NULL),
('\"{\\\"dospeli\\\":\\\"3\\\",\\\"deti\\\":\\\"1\\\",\\\"cislo\\\":null,\\\"email\\\":\\\"vitek.beran@seznam.cz\\\"}\"',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	97,	31,	1,	'2022-05-10 08:32:44',	'2022-05-10 08:32:44',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	103,	31,	1,	'2022-05-10 09:16:55',	'2022-05-10 09:16:55',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	104,	31,	1,	'2022-05-10 09:16:57',	'2022-05-10 09:16:57',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	105,	31,	1,	'2022-05-10 09:16:59',	'2022-05-10 09:16:59',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	106,	31,	1,	'2022-05-10 09:17:01',	'2022-05-10 09:17:01',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	107,	31,	1,	'2022-05-10 09:17:04',	'2022-05-10 09:17:04',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	108,	31,	1,	'2022-05-10 09:17:06',	'2022-05-10 09:17:06',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	109,	31,	1,	'2022-05-10 09:17:09',	'2022-05-10 09:17:09',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	110,	31,	1,	'2022-05-10 09:17:11',	'2022-05-10 09:17:11',	NULL),
('{\"deti\": \"1\", \"cislo\": null, \"email\": \"vitek.beran@seznam.cz\", \"dospeli\": \"3\"}',	'2022-04-06',	'2022-04-06',	'00:00:00',	'00:00:00',	117,	31,	11,	'2022-05-10 09:20:43',	'2022-05-10 09:20:43',	NULL),
('{\"nalada\": \"jde to\"}',	'2022-05-12',	'2022-05-12',	'00:00:00',	'00:00:00',	118,	32,	12,	'2022-05-10 10:19:01',	'2022-05-10 10:19:01',	NULL),
('{\"nalada\": \"jde to\"}',	'2022-05-19',	'2022-05-19',	'00:00:00',	'00:00:00',	119,	33,	12,	'2022-05-10 10:45:13',	'2022-05-10 10:45:13',	NULL),
('{\"email\": \"Demo column 1\", \"mobil\": \"Demo column 2\"}',	'2022-05-10',	'2022-05-10',	'00:00:00',	'00:00:00',	120,	34,	13,	'2022-05-10 11:00:30',	'2022-05-10 11:00:30',	NULL);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `roleID` tinyint(4) NOT NULL AUTO_INCREMENT,
  `role` char(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `roles` (`roleID`, `role`) VALUES
(1,	'admin'),
(2,	'editor'),
(3,	'viewer');

DROP TABLE IF EXISTS `telescope_entries`;
CREATE TABLE `telescope_entries` (
  `sequence` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sequence`),
  UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`),
  KEY `telescope_entries_batch_id_index` (`batch_id`),
  KEY `telescope_entries_family_hash_index` (`family_hash`),
  KEY `telescope_entries_created_at_index` (`created_at`),
  KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `telescope_entries_tags`;
CREATE TABLE `telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  KEY `telescope_entries_tags_tag_index` (`tag`),
  CONSTRAINT `telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `telescope_entries` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `telescope_monitoring`;
CREATE TABLE `telescope_monitoring` (
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usersID` int(11) NOT NULL AUTO_INCREMENT,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`usersID`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`email`, `first_name`, `last_name`, `password`, `phone`, `usersID`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
('lkov@post.cz',	'Lukáš',	'Kovář',	'$2y$10$vgVawsVY/HVnTPQ41UMVbuxY4PX6tltPsADEUGaN4JPhPCnjSqZHe',	NULL,	1,	NULL,	'1OiltnH2Elqo0AMqJXZea2qbGCFgsFQn7Q6ekfTZJVA08CRyFGJjZ5Cf3ik3',	'2022-03-23 08:28:21',	'2022-05-08 16:31:42'),
('vitek.beran@loutaci.cz',	'Vítězslav',	'Beran',	'$2y$10$wzCNnt3Gw7I48jxTQYb84.TxkoPiMM5N5FKxDjS/qJ7RgyBZWq/Ue',	NULL,	2,	NULL,	NULL,	'2022-03-24 12:15:51',	'2022-03-24 12:15:51'),
('test@test.cz',	'Test',	'Tester',	'$2y$10$0qc9iVBTVBN2AzowB4RJNeXiQPHQ.ffNL7x5A7437qmyVIosMJaoe',	NULL,	3,	NULL,	NULL,	'2022-04-06 08:33:02',	'2022-04-06 08:33:02'),
('uhk@uhk.cz',	'Karel',	'WTF',	'$2y$10$SxEZaFd9mP9AOcVwl6N40.RBg/wMUUu8e0rNCV/P.DZulkzVLhZTW',	NULL,	4,	NULL,	NULL,	'2022-04-19 15:53:55',	'2022-04-19 15:53:55'),
('info@naralamp.cz',	'Lukáš',	'Kovář',	'$2y$10$wJ8nEchlL7Z8HaUP/mU1wOWk3lMPqNlmPuIlOmd3rqYn3oamkB.di',	NULL,	5,	NULL,	NULL,	'2022-04-25 16:53:42',	'2022-04-25 16:53:42'),
('archlzd1@uhk.cz',	'Zdenek',	'Archleb',	'$2a$12$VloEmNG2IH5HGyPp6e.PruLDT8Uvsj/Ds1.DWNBVB6E2ftRZMVziW',	NULL,	6,	NULL,	NULL,	NULL,	NULL),
('lukas.kovar@loutaci.cz',	'Lukáš',	'Kovář',	'$2y$10$6TJEX6wXx.I/8jBdcyFNBuGekRnmij0EiqZTzunIhVVdFfXeT/IX.',	NULL,	7,	NULL,	NULL,	'2022-05-09 17:18:24',	'2022-05-09 17:18:24'),
('testeer@test.cz',	'Test',	'Tester',	'$2y$10$jiFcl05miQBM4EWKPdPvS.FLq2F7I/YnNO6sdwvogd4yCz4yvqcny',	NULL,	8,	NULL,	NULL,	'2022-05-09 17:46:35',	'2022-05-09 17:46:35'),
('piip@test.cz',	'Test',	'Tester',	'$2y$10$ShAmgRGhULdJDGs1l.5h.ehcqFbePvC.J.VN5fPWEcWHWiwXWhyZ6',	NULL,	9,	NULL,	NULL,	'2022-05-09 17:54:50',	'2022-05-09 17:54:50'),
('piipl@test.cz',	'Test',	'Tester',	'$2y$10$NJBkVLJv06zWHX0c8llP/un36CasxvPezHCehOHWQ85vOf4P26v0y',	NULL,	10,	NULL,	NULL,	'2022-05-09 17:56:46',	'2022-05-09 17:56:46'),
('vitek.beran@seznam.cz',	'Vítek',	'Beran',	'$2y$10$fcATacHRlIIhVo17X1PwlOirocvv8fucS06JTjkQ79A5Kcvt99cX2',	NULL,	11,	NULL,	NULL,	'2022-05-09 19:28:13',	'2022-05-09 19:28:13'),
('kkokyk@test.cz',	'lukas',	'kovar',	'$2y$10$51J/8rEnnOBEbFWfNJzKke.4Eu0oklqXCX1hPLIONGkcesv9uQRLG',	NULL,	12,	NULL,	NULL,	'2022-05-10 04:41:15',	'2022-05-10 04:41:15'),
('kskokyk@test.cz',	'lukas',	'kovar',	'$2y$10$Y9oI337dK3Ocv7V9G.qb8eokMgupFsoqnNUAbkxzrpfH7SjHVvqKy',	NULL,	13,	NULL,	NULL,	'2022-05-10 04:45:32',	'2022-05-10 04:45:32'),
('lkovik@post.cz',	'jmeno',	'prijmeni',	'$2y$10$0iFPX3nhFuf9Jjg4nuMyYebGvMLMvn/fD2Ea2eySvEFvP8dieJDti',	NULL,	14,	NULL,	NULL,	'2022-05-10 04:51:06',	'2022-05-10 04:51:06'),
('ttt@ttt.com',	'jajks',	'juks',	'$2y$10$xs4v/cY6Wcy3V7ntD3d53OycW0XSGb9EN5GM8Hx67euJc.rZkqS3y',	NULL,	15,	NULL,	NULL,	'2022-05-10 04:52:53',	'2022-05-10 04:52:53'),
('lkovss@post.cz',	'testik',	'test',	'$2y$10$ayBT0GPPwanUinlodOUGn.f.nDCKZjSA3IwEk6UYAwjXmUzYMtKru',	NULL,	16,	NULL,	NULL,	'2022-05-10 05:25:00',	'2022-05-10 05:25:00'),
('loutaci@loutaci.cz',	'vitekkkk',	'beran',	'$2y$10$v54x5EYbpVJDng.EuvdzR.JnVdxhDSF2iRiUhqxDOk6nJzMz.i24.',	NULL,	17,	NULL,	NULL,	'2022-05-10 05:48:17',	'2022-05-10 05:48:17'),
('testter@test.com',	'testik',	'test',	'$2y$10$Cq6SLD0.DPr9j6xOS4jzCOeMlrOL6amO1uTUSpeypkT5zTHAwg4aS',	NULL,	18,	NULL,	NULL,	'2022-05-10 06:54:20',	'2022-05-10 06:54:20'),
('uhk@uhkk.cz',	'test',	'testt',	'$2y$10$8XQJBBgHLEqNnmNIc8Mei.DGUy03eImewlJByVUJuzNzHXDAYabe.',	NULL,	19,	NULL,	NULL,	'2022-05-10 06:55:06',	'2022-05-10 06:55:06'),
('lkovar2@gmail.com',	'lukas',	'kovar',	'$2y$10$o8mJ9n0v6z09erR5WbForu97jiHJkVVfxbPMwmv4ZgU1u.AhUmLA.',	NULL,	20,	NULL,	NULL,	'2022-05-10 06:55:32',	'2022-05-10 06:55:32'),
('admin@admin.test',	'Admin',	'Administrátorskej',	'$2y$10$DEiNgTuy0E3kGFGLwXgm5Oov1raWiXES9e.nxo.Vzeoqo3aDArV/i',	NULL,	21,	NULL,	NULL,	'2022-05-10 10:36:59',	'2022-05-10 10:36:59'),
('editor@editor.test',	'Editor',	'Editorský',	'$2y$10$LK92g4yNjre6U92sLOjt8ugqltUIKBi8IHeAn9eGOpulR/BEgFa2e',	NULL,	22,	NULL,	NULL,	'2022-05-10 11:13:28',	'2022-05-10 11:13:28');

DROP TABLE IF EXISTS `windows`;
CREATE TABLE `windows` (
  `calendarsID` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `windowsID` int(11) NOT NULL AUTO_INCREMENT,
  `formsID` int(11) DEFAULT NULL,
  `portalsID` int(11) DEFAULT NULL,
  PRIMARY KEY (`windowsID`),
  KEY `FK_windows_calendars` (`calendarsID`),
  KEY `FK_windows_forms` (`formsID`),
  KEY `portalsID` (`portalsID`),
  CONSTRAINT `FK_windows_calendars` FOREIGN KEY (`calendarsID`) REFERENCES `calendars` (`calendarsID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_windows_forms` FOREIGN KEY (`formsID`) REFERENCES `forms` (`formsID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `windows_ibfk_1` FOREIGN KEY (`portalsID`) REFERENCES `portals` (`portalsID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `windows` (`calendarsID`, `name`, `description`, `windowsID`, `formsID`, `portalsID`) VALUES
(1,	'vokno',	NULL,	1,	1,	1),
(2,	'okno',	NULL,	2,	1,	1),
(3,	'okynko',	NULL,	3,	1,	1),
(4,	'kokynko',	NULL,	4,	1,	1),
(5,	'hovinko',	NULL,	5,	1,	1),
(17,	'test window',	'vítek test window',	9,	15,	2),
(17,	'kk',	'hojjjk',	10,	15,	2),
(18,	'Hotel UHK',	'Pronajmnete si nas pokoj',	11,	16,	24),
(19,	'Já udělal kuk und viděl das zlaté okno',	NULL,	12,	17,	25),
(20,	'DEMO-CalendarWindow',	'Demo okno kalendáře sloužící pro prezentaci rezervačního prostoru Calendar',	13,	19,	27);

DROP TABLE IF EXISTS `CustWithAddr`;
CREATE ALGORITHM=UNDEFINED DEFINER=`uhk`@`%.%.%.%` SQL SECURITY DEFINER VIEW `CustWithAddr` AS select `c`.`customersID` AS `customersID`,`c`.`email` AS `email`,`c`.`first_name` AS `first_name`,`c`.`last_name` AS `last_name`,`c`.`phone` AS `phone`,`a`.`street` AS `street`,`a`.`number` AS `number`,`a`.`city` AS `city`,`a`.`postal_code` AS `postal_code`,`a`.`state` AS `state` from (`customers` `c` join `addresses` `a` on((`c`.`addressesID` = `a`.`addressesID`)));

-- 2022-05-10 12:16:17
