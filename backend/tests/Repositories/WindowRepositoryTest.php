<?php namespace Tests\Repositories;

use App\Models\Window;
use App\Repositories\WindowRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class WindowRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var WindowRepository
     */
    protected $windowRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->windowRepo = \App::make(WindowRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_window()
    {
        $window = Window::factory()->make()->toArray();

        $createdWindow = $this->windowRepo->create($window);

        $createdWindow = $createdWindow->toArray();
        $this->assertArrayHasKey('id', $createdWindow);
        $this->assertNotNull($createdWindow['id'], 'Created Window must have id specified');
        $this->assertNotNull(Window::find($createdWindow['id']), 'Window with given id must be in DB');
        $this->assertModelData($window, $createdWindow);
    }

    /**
     * @test read
     */
    public function test_read_window()
    {
        $window = Window::factory()->create();

        $dbWindow = $this->windowRepo->find($window->id);

        $dbWindow = $dbWindow->toArray();
        $this->assertModelData($window->toArray(), $dbWindow);
    }

    /**
     * @test update
     */
    public function test_update_window()
    {
        $window = Window::factory()->create();
        $fakeWindow = Window::factory()->make()->toArray();

        $updatedWindow = $this->windowRepo->update($fakeWindow, $window->id);

        $this->assertModelData($fakeWindow, $updatedWindow->toArray());
        $dbWindow = $this->windowRepo->find($window->id);
        $this->assertModelData($fakeWindow, $dbWindow->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_window()
    {
        $window = Window::factory()->create();

        $resp = $this->windowRepo->delete($window->id);

        $this->assertTrue($resp);
        $this->assertNull(Window::find($window->id), 'Window should not exist in DB');
    }
}
