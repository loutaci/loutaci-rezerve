<?php namespace Tests\Repositories;

use App\Models\Portal;
use App\Repositories\PortalRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PortalRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PortalRepository
     */
    protected $portalRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->portalRepo = \App::make(PortalRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_portal()
    {
        $portal = Portal::factory()->make()->toArray();

        $createdPortal = $this->portalRepo->create($portal);

        $createdPortal = $createdPortal->toArray();
        $this->assertArrayHasKey('id', $createdPortal);
        $this->assertNotNull($createdPortal['id'], 'Created Portal must have id specified');
        $this->assertNotNull(Portal::find($createdPortal['id']), 'Portal with given id must be in DB');
        $this->assertModelData($portal, $createdPortal);
    }

    /**
     * @test read
     */
    public function test_read_portal()
    {
        $portal = Portal::factory()->create();

        $dbPortal = $this->portalRepo->find($portal->id);

        $dbPortal = $dbPortal->toArray();
        $this->assertModelData($portal->toArray(), $dbPortal);
    }

    /**
     * @test update
     */
    public function test_update_portal()
    {
        $portal = Portal::factory()->create();
        $fakePortal = Portal::factory()->make()->toArray();

        $updatedPortal = $this->portalRepo->update($fakePortal, $portal->id);

        $this->assertModelData($fakePortal, $updatedPortal->toArray());
        $dbPortal = $this->portalRepo->find($portal->id);
        $this->assertModelData($fakePortal, $dbPortal->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_portal()
    {
        $portal = Portal::factory()->create();

        $resp = $this->portalRepo->delete($portal->id);

        $this->assertTrue($resp);
        $this->assertNull(Portal::find($portal->id), 'Portal should not exist in DB');
    }
}
