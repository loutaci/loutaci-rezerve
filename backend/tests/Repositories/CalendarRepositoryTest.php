<?php namespace Tests\Repositories;

use App\Models\Calendar;
use App\Repositories\CalendarRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CalendarRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CalendarRepository
     */
    protected $calendarRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->calendarRepo = \App::make(CalendarRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_calendar()
    {
        $calendar = Calendar::factory()->make()->toArray();

        $createdCalendar = $this->calendarRepo->create($calendar);

        $createdCalendar = $createdCalendar->toArray();
        $this->assertArrayHasKey('id', $createdCalendar);
        $this->assertNotNull($createdCalendar['id'], 'Created Calendar must have id specified');
        $this->assertNotNull(Calendar::find($createdCalendar['id']), 'Calendar with given id must be in DB');
        $this->assertModelData($calendar, $createdCalendar);
    }

    /**
     * @test read
     */
    public function test_read_calendar()
    {
        $calendar = Calendar::factory()->create();

        $dbCalendar = $this->calendarRepo->find($calendar->id);

        $dbCalendar = $dbCalendar->toArray();
        $this->assertModelData($calendar->toArray(), $dbCalendar);
    }

    /**
     * @test update
     */
    public function test_update_calendar()
    {
        $calendar = Calendar::factory()->create();
        $fakeCalendar = Calendar::factory()->make()->toArray();

        $updatedCalendar = $this->calendarRepo->update($fakeCalendar, $calendar->id);

        $this->assertModelData($fakeCalendar, $updatedCalendar->toArray());
        $dbCalendar = $this->calendarRepo->find($calendar->id);
        $this->assertModelData($fakeCalendar, $dbCalendar->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_calendar()
    {
        $calendar = Calendar::factory()->create();

        $resp = $this->calendarRepo->delete($calendar->id);

        $this->assertTrue($resp);
        $this->assertNull(Calendar::find($calendar->id), 'Calendar should not exist in DB');
    }
}
