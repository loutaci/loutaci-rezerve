<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Window;

class WindowApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_window()
    {
        $window = Window::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/windows', $window
        );

        $this->assertApiResponse($window);
    }

    /**
     * @test
     */
    public function test_read_window()
    {
        $window = Window::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/windows/'.$window->id
        );

        $this->assertApiResponse($window->toArray());
    }

    /**
     * @test
     */
    public function test_update_window()
    {
        $window = Window::factory()->create();
        $editedWindow = Window::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/windows/'.$window->id,
            $editedWindow
        );

        $this->assertApiResponse($editedWindow);
    }

    /**
     * @test
     */
    public function test_delete_window()
    {
        $window = Window::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/windows/'.$window->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/windows/'.$window->id
        );

        $this->response->assertStatus(404);
    }
}
