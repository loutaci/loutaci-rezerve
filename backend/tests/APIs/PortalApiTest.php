<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Portal;

class PortalApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_portal()
    {
        $portal = Portal::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/portals', $portal
        );

        $this->assertApiResponse($portal);
    }

    /**
     * @test
     */
    public function test_read_portal()
    {
        $portal = Portal::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/portals/'.$portal->id
        );

        $this->assertApiResponse($portal->toArray());
    }

    /**
     * @test
     */
    public function test_update_portal()
    {
        $portal = Portal::factory()->create();
        $editedPortal = Portal::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/portals/'.$portal->id,
            $editedPortal
        );

        $this->assertApiResponse($editedPortal);
    }

    /**
     * @test
     */
    public function test_delete_portal()
    {
        $portal = Portal::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/portals/'.$portal->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/portals/'.$portal->id
        );

        $this->response->assertStatus(404);
    }
}
