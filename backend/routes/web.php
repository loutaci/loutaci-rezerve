<?php

use App\Models\Application;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
    // return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::resource('users', App\Http\Controllers\UserController::class);


// Route::resource('reservations', App\Http\Controllers\ReservationController::class);


Route::resource('customers', App\Http\Controllers\CustomerController::class);
// Route::resource('api/customers', App\Http\Controllers\ApiCustomerController::class);


Route::resource('addresses', App\Http\Controllers\AddressController::class);
Route::resource('reservations', App\Http\Controllers\ReservationController::class);

Route::any('/sendmail', function () {
    mail('lkov@post.cz', 'Test', "Hello");
});


Route::resource('windows', App\Http\Controllers\WindowController::class);


Route::resource('applications', App\Http\Controllers\ApplicationController::class);

Route::get("iframe-test/{id}", function ($id) {
    $apps = Application::where('windowsID', $id)->get();
    // $apps = Application::where('windowsID','=',$id)->get();
    // dd($id);
    header("Content-Security-Policy: frame-ancestors 'self' " . join(', ', $apps->pluck('url')->toArray()));
    return view('iframe-test')->with('apps', $apps);
});


Route::resource('calendarExceptions', App\Http\Controllers\CalendarExceptionsController::class);

// Route::any('', function() {
//     return '';
// });


Route::resource('portalUsers', App\Http\Controllers\PortalUsersController::class);


Route::resource('roles', App\Http\Controllers\RoleController::class);

Route::get("calendar/test/{id}", function ($id) {
    return view('calendar-test')->with('id', $id);
});
