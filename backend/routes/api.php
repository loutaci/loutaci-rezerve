<?php

use App\Http\Controllers\API\WindowAPIController;
use App\Models\Portal;
use App\Models\PortalUsers;
use App\Models\Reservation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Broadcast::routes(['middleware' => ['auth:sanctum']]);



Route::post('/sanctum/token', function (Request $request) {
    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required',
    ]);

    $user = User::where('email', $request->email)->first();

    if (!$user || !Hash::check($request->password, $user->password)) {
        // throw ValidationException::withMessages([
        //     'email' => ['The provided credentials are incorrect.'],
        // ]);
        return response()->json([
            'message' => 'Neplatné přihlašovací údaje, zkuste to znovu.'
        ], 401);
    }

    return ['message' => 'Přihlášení proběhlo úspěšně', 'token' => $user->createToken($request->device_name)->plainTextToken];
});


Route::middleware('web')->post('login', function (Request $request) {
    $credentials = $request->validate([
        'email' => ['required', 'email'],
        'password' => ['required'],
    ]);

    if (Auth::attempt($credentials)) {
        $request->session()->regenerate();

        return response()->json(['success' => true, 'user' => Auth::user()]);
    }

    return response()->json(['error' => 'invalid_credentials'], 401);
    // return $controller->login($request);
});

// Route::middleware('auth:sanctum')->get('user', function (Request $request) {
//     $user = $request->user()->toArray();
//     // return $user;
// });

// Route::get("windows/{windowId}/", [WindowAPIController::class,"index"]);
// Route::get("windows/", [WindowAPIController::class,"index"]);
Route::get('windows/{windowId}/', [WindowAPIController::class, 'show']);
Route::apiResource('windows', WindowAPIController::class)->middleware('auth:sanctum', ['include' => ['index', 'store', 'update', 'destroy'], 'exclude' => ['getForm', 'getCalendar', 'getPortal', 'getReservation', 'getEvents', 'show']]);
// Route::get('windows/{windowId}/', [WindowAPIController::class,"index"])->middleware('auth:sanctum');
Route::get("windows/{windowId}/form", [WindowAPIController::class, "getForm"]);
Route::get("windows/{windowId}/calendar", [WindowAPIController::class, "getCalendar"]);
Route::get("windows/{windowId}/reservations", [WindowAPIController::class, "getReservations"]);
Route::get("windows/{windowId}/events", [WindowAPIController::class, "getEvents"]);


// Route::resource('users', App\Http\Controllers\API\UserController::class)->middleware('auth:sanctum');

Route::resource('calendars', App\Http\Controllers\API\CalendarAPIController::class)->middleware('auth:sanctum');


Route::resource('forms', App\Http\Controllers\API\FormAPIController::class)->middleware('auth:sanctum');


Route::resource('portals', App\Http\Controllers\API\PortalAPIController::class)->middleware('auth:sanctum');


Route::resource('events', App\Http\Controllers\API\EventAPIController::class)->middleware('auth:sanctum');


Route::resource('reservations', App\Http\Controllers\API\ReservationAPIController::class);


Route::resource('customers', App\Http\Controllers\API\CustomerAPIController::class)->middleware('auth:sanctum');


Route::resource('applications', App\Http\Controllers\API\ApplicationAPIController::class)->middleware('auth:sanctum');

Route::get('hello', function (Request $request) {

    $request->user()->portals;
    $data = $request->user()->portals->filter(function ($value, $key) {
        return $value->portalsID == 8;
    });
    return empty($data) ? "hey" : "nope";
    //->get('portalsID', 9)
    // return 'Hello World';
    // dd(request()->user());
    // return 'Hello World';
})->middleware('auth:sanctum');


Route::get('user', function (Request $request) {
    $user = $request->user()->toArray();

    $portalusers = PortalUsers::where('usersID', '=', $user['usersID'])->where('portalsID', '=', user_portal(true)->portalsID)->first();
    if (!empty($portalusers)) {
        $user['roleID'] = $portalusers->roleID;
    }
    $user['portals'] = $request->user()->portals;
    return ['success' => true, 'data' => $user, 'message' => 'User data retrieved successfully.'];
})->middleware('auth:sanctum');

Route::patch('users/{userId}', function (Request $request, $userId) {
    $user = User::find($userId);
    $user->update($request->all());
    return ['success' => true, 'data' => $user, 'message' => 'User data updated successfully.'];
})->middleware('auth:sanctum');

Route::post('logout', function (Request $request) {
    $request->user()->tokens->each(function ($token) {
        $token->revoke();
    });
})->middleware('auth:sanctum');

Route::post('register', function (Request $request) {
    $data = $request->validate([
        'first_name' => ['required', 'string', 'max:255'],
        'last_name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:6']
    ]);

    $user = User::create([
        'first_name' => $data['first_name'],
        'last_name' => $data['last_name'],
        'email' => $data['email'],
        'password' => Hash::make($data['password']),
    ]);

    return response()->json(['success' => true, 'data' => $user, 'message' => 'Uživatel úspěšně vytvořen.']);

    // $user->sendEmailVerificationNotification();

    // return $user;
});


Route::patch('reservations-paid/{id}', function ($id) {
    $reservation = Reservation::find($id);
    $reservation->paid = request()->get('markPaid');
    $reservation->save();
    return ['success' => true, 'data' => $reservation, 'message' => 'Reservation paid successfully.'];
    // $reservation = Reservation::find($id);
});

Route::get('portal-users', function () {
    // $portal = user_portal();
    // $portal = Portal::find(user_portal()->portalsID);
    // $portal->users;
    $users = user_portal()->portalusers->map(function ($portaluser) {
        $user = $portaluser->users->toArray();
        $user['portalusersID'] = $portaluser->portalusersID;
        $user['roleID'] = $portaluser->roleID;
        // $user->role = $portaluser->role;
        return $user;
    });
    return ['success' => true, 'data' => $users, 'message' => 'Portal users retrieved successfully.'];
})->middleware('auth:sanctum');

Route::post('portal-users', function () {
    // $data = request()->validate([
    //     'usersID' => ['required', 'integer'],
    //     'portalsID' => ['required', 'integer'],
    //     'roleID' => ['required', 'integer'],
    // ]);
    $data = request()->all();
    $data['usersID'] = User::where('email', '=', $data['email'])->first()->usersID;
    $portaluser = PortalUsers::create($data);
    return ['success' => true, 'data' => $portaluser, 'message' => 'PortalUser created successfully.'];
})->middleware('auth:sanctum');

Route::delete('portal-users/{id}', function ($id) {
    $portaluser = PortalUsers::find($id);
    $portaluser->delete();
    return ['success' => true, 'data' => $portaluser, 'message' => 'PortalUser deleted successfully.'];
})->middleware('auth:sanctum');
