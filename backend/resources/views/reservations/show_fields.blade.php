<!-- Data Field -->
<div class="col-sm-12">
    {!! Form::label('data', 'Data:') !!}
    <p>{{ $reservation->data }}</p>
</div>

<!-- Date End Field -->
<div class="col-sm-12">
    {!! Form::label('date_end', 'Date End:') !!}
    <p>{{ $reservation->date_end }}</p>
</div>

<!-- Date Start Field -->
<div class="col-sm-12">
    {!! Form::label('date_start', 'Date Start:') !!}
    <p>{{ $reservation->date_start }}</p>
</div>

<!-- Time End Field -->
<div class="col-sm-12">
    {!! Form::label('time_end', 'Time End:') !!}
    <p>{{ $reservation->time_end }}</p>
</div>

<!-- Time Start Field -->
<div class="col-sm-12">
    {!! Form::label('time_start', 'Time Start:') !!}
    <p>{{ $reservation->time_start }}</p>
</div>

<!-- Customersid Field -->
<div class="col-sm-12">
    {!! Form::label('customersID', 'Customersid:') !!}
    <p>{{ $reservation->customersID }}</p>
</div>

<!-- Eventsid Field -->
<div class="col-sm-12">
    {!! Form::label('eventsID', 'Eventsid:') !!}
    <p>{{ $reservation->eventsID }}</p>
</div>

<!-- Windowsid Field -->
<div class="col-sm-12">
    {!! Form::label('windowsID', 'Windowsid:') !!}
    <p>{{ $reservation->windowsID }}</p>
</div>

