<!-- Data Field -->
<div class="form-group col-sm-6">
    {!! Form::label('data', 'Data:') !!}
    {!! Form::text('data', null, ['class' => 'form-control']) !!}
</div>

<!-- Date End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_end', 'Date End:') !!}
    {!! Form::text('date_end', null, ['class' => 'form-control','id'=>'date_end']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#date_end').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Date Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date_start', 'Date Start:') !!}
    {!! Form::text('date_start', null, ['class' => 'form-control','id'=>'date_start']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#date_start').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Time End Field -->
<div class="form-group col-sm-6">
    {!! Form::label('time_end', 'Time End:') !!}
    {!! Form::text('time_end', null, ['class' => 'form-control']) !!}
</div>

<!-- Time Start Field -->
<div class="form-group col-sm-6">
    {!! Form::label('time_start', 'Time Start:') !!}
    {!! Form::text('time_start', null, ['class' => 'form-control']) !!}
</div>

<!-- Customersid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('customersID', 'Customersid:') !!}
    {!! Form::number('customersID', null, ['class' => 'form-control']) !!}
</div>

<!-- Eventsid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('eventsID', 'Eventsid:') !!}
    {!! Form::number('eventsID', null, ['class' => 'form-control']) !!}
</div>

<!-- Windowsid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('windowsID', 'Windowsid:') !!}
    {!! Form::number('windowsID', null, ['class' => 'form-control']) !!}
</div>