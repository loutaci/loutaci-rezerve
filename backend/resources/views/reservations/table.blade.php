<div class="table-responsive">
    <table class="table" id="reservations-table">
        <thead>
        <tr>
            <th>Data</th>
        <th>Date End</th>
        <th>Date Start</th>
        <th>Time End</th>
        <th>Time Start</th>
        <th>Customersid</th>
        <th>Eventsid</th>
        <th>Windowsid</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($reservations as $reservation)
            <tr>
                <td>{{ $reservation->data }}</td>
            <td>{{ $reservation->date_end }}</td>
            <td>{{ $reservation->date_start }}</td>
            <td>{{ $reservation->time_end }}</td>
            <td>{{ $reservation->time_start }}</td>
            <td>{{ $reservation->customersID }}</td>
            <td>{{ $reservation->eventsID }}</td>
            <td>{{ $reservation->windowsID }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['reservations.destroy', $reservation->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('reservations.show', [$reservation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('reservations.edit', [$reservation->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
