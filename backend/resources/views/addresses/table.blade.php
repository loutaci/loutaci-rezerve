<div class="table-responsive">
    <table class="table" id="addresses-table">
        <thead>
        <tr>
            <th>Number</th>
        <th>Postal Code</th>
        <th>Street</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($addresses as $address)
            <tr>
                <td>{{ $address->number }}</td>
            <td>{{ $address->postal_code }}</td>
            <td>{{ $address->street }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['addresses.destroy', $address->addressesID], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('addresses.show', [$address->addressesID]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('addresses.edit', [$address->addressesID]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
