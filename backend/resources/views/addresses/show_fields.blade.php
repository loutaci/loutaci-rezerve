<!-- Number Field -->
<div class="col-sm-12">
    {!! Form::label('number', 'Number:') !!}
    <p>{{ $address->number }}</p>
</div>

<!-- Postal Code Field -->
<div class="col-sm-12">
    {!! Form::label('postal_code', 'Postal Code:') !!}
    <p>{{ $address->postal_code }}</p>
</div>

<!-- Street Field -->
<div class="col-sm-12">
    {!! Form::label('street', 'Street:') !!}
    <p>{{ $address->street }}</p>
</div>

