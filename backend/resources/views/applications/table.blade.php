<div class="table-responsive">
    <table class="table" id="applications-table">
        <thead>
        <tr>
            <th>Url</th>
        <th>Windowsid</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($applications as $application)
            <tr>
                <td>{{ $application->url }}</td>
            <td>{{ $application->windowsID }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['applications.destroy', $application->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('applications.show', [$application->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('applications.edit', [$application->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
