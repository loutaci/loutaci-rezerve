<!-- <li class="nav-item">
    <a href="{{ route('users.index') }}"
       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <p>Users</p>
    </a>
</li> -->

<li class="nav-item">
    <a href="{{ route('customers.index') }}"
       class="nav-link {{ Request::is('customers*') ? 'active' : '' }}">
        <p>Customers</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('addresses.index') }}"
       class="nav-link {{ Request::is('addresses*') ? 'active' : '' }}">
        <p>Addresses</p>
    </a>
</li>


 <li class="nav-item">
    <a href="{{ route('reservations.index') }}"
       class="nav-link {{ Request::is('reservations*') ? 'active' : '' }}">
        <p>Reservations</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('windows.index') }}"
       class="nav-link {{ Request::is('windows*') ? 'active' : '' }}">
        <p>Windows</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('applications.index') }}"
       class="nav-link {{ Request::is('applications*') ? 'active' : '' }}">
        <p>Applications</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('calendarExceptions.index') }}"
       class="nav-link {{ Request::is('calendarExceptions*') ? 'active' : '' }}">
        <p>Calendar Exceptions</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('portalUsers.index') }}"
       class="nav-link {{ Request::is('portalUsers*') ? 'active' : '' }}">
        <p>Portal Users</p>
    </a>
</li>


<li class="nav-item">
    <a href="{{ route('roles.index') }}"
       class="nav-link {{ Request::is('roles*') ? 'active' : '' }}">
        <p>Roles</p>
    </a>
</li>


