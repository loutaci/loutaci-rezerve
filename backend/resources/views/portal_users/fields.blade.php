<!-- Portalsid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('portalsID', 'Portalsid:') !!}
    {!! Form::number('portalsID', null, ['class' => 'form-control']) !!}
</div>

<!-- Usersid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('usersID', 'Usersid:') !!}
    {!! Form::number('usersID', null, ['class' => 'form-control']) !!}
</div>

<!-- Roleid Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('roleID', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('roleID', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('roleID', 'Roleid', ['class' => 'form-check-label']) !!}
    </div>
</div>
