<div class="table-responsive">
    <table class="table" id="portalUsers-table">
        <thead>
        <tr>
            <th>Portalsid</th>
        <th>Usersid</th>
        <th>Roleid</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($portalUsers as $portalUsers)
            <tr>
                <td>{{ $portalUsers->portalsID }}</td>
            <td>{{ $portalUsers->usersID }}</td>
            <td>{{ $portalUsers->roleID }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['portalUsers.destroy', $portalUsers->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('portalUsers.show', [$portalUsers->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('portalUsers.edit', [$portalUsers->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
