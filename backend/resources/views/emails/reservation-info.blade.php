<h1>
    Info o rezervaci
</h1>
<p>
    <strong>Jméno:</strong> {{ $customer->name }}
</p>
<p>
    <strong>Telefon:</strong> {{ $customer->phone ?? "Neuvedeno" }}
</p>
<p>
    <strong>Email:</strong> {{ $customer->email }}
</p>
<p>
    <strong>Datum:</strong> {{ $reservation->date }}
</p>
<p>
    <strong>Čas:</strong> {{ $reservation->datetime }}
</p>

<pre>
{{ json_encode($reservation) }}
</pre>

Aktuální stav rezervace najdete na <a href="{{ 'https://rezervace.loutaci.cz/api/reservations/' . $reservation->reservationsID }}">stránce rezervace</a>.
{{--  <a href="{{ route('reservation.show', $reservation->id) }}">stránce rezervace</a>.  --}}
