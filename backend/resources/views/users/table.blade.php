<div class="table-responsive">
    <table class="table" id="users-table">
        <thead>
        <tr>
        <th>Email</th>
        <th>First Name</th>
        <th>Last Name</th>
        <!-- <th>Password</th> -->
        <th>Phone</th>
        <th>Role</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->email }}</td>
            <td>{{ $user->first_name }}</td>
            <td>{{ $user->last_name }}</td>
            <!-- <td>{{ $user->password }}</td> -->
            <td>{{ $user->phone }}</td>
            <td>{{ $user->role }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['users.destroy', $user->usersID], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('users.show', [$user->usersID]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('users.edit', [$user->usersID]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
