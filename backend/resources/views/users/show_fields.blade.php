<!-- Email Field -->
<div class="col-sm-12">
    {!! Form::label('Email', 'Email:') !!}
    <p>{{ $user->Email }}</p>
</div>

<!-- First Name Field -->
<div class="col-sm-12">
    {!! Form::label('First_name', 'First Name:') !!}
    <p>{{ $user->First_name }}</p>
</div>

<!-- Last Name Field -->
<div class="col-sm-12">
    {!! Form::label('Last_name', 'Last Name:') !!}
    <p>{{ $user->Last_name }}</p>
</div>

<!-- Password Field -->
<div class="col-sm-12">
    {!! Form::label('Password', 'Password:') !!}
    <p>{{ $user->Password }}</p>
</div>

<!-- Phone Field -->
<div class="col-sm-12">
    {!! Form::label('Phone', 'Phone:') !!}
    <p>{{ $user->Phone }}</p>
</div>

<!-- Role Field -->
<div class="col-sm-12">
    {!! Form::label('Role', 'Role:') !!}
    <p>{{ $user->Role }}</p>
</div>

