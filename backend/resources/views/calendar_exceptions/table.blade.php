<div class="table-responsive">
    <table class="table" id="calendarExceptions-table">
        <thead>
        <tr>
            <th>Windowsid</th>
        <th>Date Start</th>
        <th>Date End</th>
        <th>Time Start</th>
        <th>Time End</th>
        <th>Description</th>
        <th>Config</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($calendarExceptions as $calendarExceptions)
            <tr>
                <td>{{ $calendarExceptions->windowsID }}</td>
            <td>{{ $calendarExceptions->date_start }}</td>
            <td>{{ $calendarExceptions->date_end }}</td>
            <td>{{ $calendarExceptions->time_start }}</td>
            <td>{{ $calendarExceptions->time_end }}</td>
            <td>{{ $calendarExceptions->description }}</td>
            <td>{{ $calendarExceptions->config }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['calendarExceptions.destroy', $calendarExceptions->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('calendarExceptions.show', [$calendarExceptions->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('calendarExceptions.edit', [$calendarExceptions->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
