<!-- Windowsid Field -->
<div class="col-sm-12">
    {!! Form::label('windowsID', 'Windowsid:') !!}
    <p>{{ $calendarExceptions->windowsID }}</p>
</div>

<!-- Date Start Field -->
<div class="col-sm-12">
    {!! Form::label('date_start', 'Date Start:') !!}
    <p>{{ $calendarExceptions->date_start }}</p>
</div>

<!-- Date End Field -->
<div class="col-sm-12">
    {!! Form::label('date_end', 'Date End:') !!}
    <p>{{ $calendarExceptions->date_end }}</p>
</div>

<!-- Time Start Field -->
<div class="col-sm-12">
    {!! Form::label('time_start', 'Time Start:') !!}
    <p>{{ $calendarExceptions->time_start }}</p>
</div>

<!-- Time End Field -->
<div class="col-sm-12">
    {!! Form::label('time_end', 'Time End:') !!}
    <p>{{ $calendarExceptions->time_end }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $calendarExceptions->description }}</p>
</div>

<!-- Config Field -->
<div class="col-sm-12">
    {!! Form::label('config', 'Config:') !!}
    <p>{{ $calendarExceptions->config }}</p>
</div>

