<div class="table-responsive">
    <table class="table" id="windows-table">
        <thead>
        <tr>
            <th>Calendarsid</th>
        <th>Formsid</th>
        <th>Portalsid</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($windows as $window)
            <tr>
                <td>{{ $window->calendarsID }}</td>
            <td>{{ $window->formsID }}</td>
            <td>{{ $window->portalsID }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['windows.destroy', $window->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('windows.show', [$window->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('windows.edit', [$window->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
