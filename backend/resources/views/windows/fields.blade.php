<!-- Calendarsid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('calendarsID', 'Calendarsid:') !!}
    {!! Form::number('calendarsID', null, ['class' => 'form-control']) !!}
</div>

<!-- Formsid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('formsID', 'Formsid:') !!}
    {!! Form::number('formsID', null, ['class' => 'form-control']) !!}
</div>

<!-- Portalsid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('portalsID', 'Portalsid:') !!}
    {!! Form::number('portalsID', null, ['class' => 'form-control']) !!}
</div>