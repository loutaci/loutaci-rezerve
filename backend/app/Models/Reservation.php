<?php

namespace App\Models;

use Date;
use Eloquent as Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Reservation
 *
 * @package App\Models
 * @version March 29, 2022, 3:08 pm UTC
 * @property \App\Models\Customer $customersid
 * @property \App\Models\Event $eventsid
 * @property \App\Models\Window $windowsid
 * @property string $data
 * @property string $date_end
 * @property string $date_start
 * @property time $time_end
 * @property time $time_start
 * @property integer $customersID
 * @property integer $eventsID
 * @property integer $windowsID
 * @property int $reservationsID
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Customer|null $customers
 * @property-read \App\Models\Event|null $events
 * @property-read mixed $date
 * @property-read mixed $date_time
 * @property-read mixed $time
 * @property-read \App\Models\Window|null $windows
 * @method static \Database\Factories\ReservationFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation query()
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereCustomersID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereEventsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereReservationsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereTimeEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereTimeStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereWindowsId($value)
 * @mixin Model
 * @property string|null $update_token
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereUpdateToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reservation whereWindowsID($value)
 */
class Reservation extends Model
{

    use HasFactory;

    public $table = 'reservations';
    public $primaryKey = 'reservationsID';


    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'data',
        'date_end',
        'date_start',
        'time_end',
        'time_start',
        'customersID',
        'windowsID',
        'price',
        'paid'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'json',
        'date_end' => 'date',
        'date_start' => 'date',
        'reservationsID' => 'integer',
        'customersID' => 'integer',
        'windowsID' => 'integer',
        'price' => 'string',
        'paid' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'data' => 'nullable|json',
        'date_end' => 'required',
        'date_start' => 'required',
        'time_end' => 'nullable',
        'time_start' => 'nullable',
        'customersID' => 'nullable|integer',
        'windowsID' => 'nullable|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'price' => 'nullable',
        'paid' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customersID');
    }

    // /**
    //  * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    //  **/
    // public function events()
    // {
    //     return $this->belongsTo(\App\Models\Event::class, 'eventsID');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function windows()
    {
        return $this->belongsTo(\App\Models\Window::class, 'windowsID');
    }

    public function getDateAttribute()
    {
        return $this->date_start . ' - ' . $this->date_end;
    }

    public function getTimeAttribute()
    {
        return $this->time_start . ' - ' . $this->time_end;
    }

    public function getDateTimeAttribute()
    {
        return Date::parse($this->time_start)->isoFormat('HH:mm ') . Date::parse($this->date_start)->isoFormat('D.M.Y') . ' - ' . Date::parse($this->time_end)->isoFormat('HH:mm ') . Date::parse($this->date_end)->isoFormat('D.M.Y');
    }

    public function getDataAttribute($value)
    {
        return json_decode($value);
    }

    public function setDataAttribute($value)
    {
        $this->attributes['data'] = $value;
    }
}
