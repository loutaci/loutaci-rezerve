<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CalendarExceptions
 *
 * @package App\Models
 * @version April 17, 2022, 3:57 pm UTC
 * @property \App\Models\Window $windowsid
 * @property integer $windowsID
 * @property string $date_start
 * @property string $date_end
 * @property time $time_start
 * @property time $time_end
 * @property string $description
 * @property string $config
 * @property int $id
 * @property-read \App\Models\Window $windows
 * @method static \Database\Factories\CalendarExceptionsFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions query()
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions whereConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions whereTimeEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions whereTimeStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CalendarExceptions whereWindowsID($value)
 * @mixin Model
 */
class CalendarExceptions extends Model
{
    use HasFactory;

    public $table = 'calendars_exceptions';
    public $primaryKey = 'id';

    public $timestamps = false;


    public $fillable = [
        'windowsID',
        'date_start',
        'date_end',
        'time_start',
        'time_end',
        'description',
        'config'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'windowsID' => 'integer',
        'date_start' => 'date',
        'date_end' => 'date',
        'description' => 'string',
        'config' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'windowsID' => 'required|integer',
        'date_start' => 'required',
        'date_end' => 'required',
        'time_start' => 'nullable',
        'time_end' => 'nullable',
        'description' => 'nullable|string|max:8000',
        'config' => 'required|string'
    ];

    public function getConfigAttribute($value) {
        return json_decode($value);
    }

    public function setConfigAttribute($value) {
        $this->attributes['config'] = $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function windows()
    {
        return $this->belongsTo(\App\Models\Window::class, 'windowsID');
    }
}
