<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Window
 *
 * @package App\Models
 * @version March 29, 2022, 3:14 pm UTC
 * @property \App\Models\Calendar $calendarsid
 * @property \App\Models\Form $formsid
 * @property \App\Models\Portal $portalsid
 * @property \Illuminate\Database\Eloquent\Collection $applications
 * @property \Illuminate\Database\Eloquent\Collection $addresses
 * @property \Illuminate\Database\Eloquent\Collection $reservations
 * @property integer $calendarsID
 * @property integer $formsID
 * @property integer $portalsID
 * @property int $windowsID
 * @property-read int|null $addresses_count
 * @property-read int|null $applications_count
 * @property-read \App\Models\Calendar|null $calendars
 * @property-read \App\Models\Form|null $forms
 * @property-read \App\Models\Portal|null $portals
 * @property-read int|null $reservations_count
 * @method static \Database\Factories\WindowFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Window newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Window newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Window query()
 * @method static \Illuminate\Database\Eloquent\Builder|Window whereCalendarsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Window whereFormsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Window wherePortalsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Window whereWindowsID($value)
 * @mixin Model
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Event[] $events
 * @property-read int|null $events_count
 */
class Window extends Model
{
    use HasFactory;

    public $table = 'windows';
    public $primaryKey = 'windowsID';

    public $timestamps = false;



    public $fillable = [
        'calendarsID',
        'eventsID',
        'formsID',
        'portalsID',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'calendarsID' => 'integer',
        'eventsID' => 'integer',
        'windowsID' => 'integer',
        'formsID' => 'integer',
        'portalsID' => 'integer',
        'description' => 'string',
        'name' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'calendarsID' => 'nullable|integer',
        'eventsID' => 'nullable|integer',
        'formsID' => 'nullable|integer',
        'portalsID' => 'nullable|integer',
        'name' => 'required|string|max:999',
        'description' => 'nullable|string|max:999',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function calendars()
    {
        return $this->belongsTo(\App\Models\Calendar::class, 'calendarsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function forms()
    {
        return $this->belongsTo(\App\Models\Form::class, 'formsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function portals()
    {
        return $this->belongsTo(\App\Models\Portal::class, 'portalsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function applications()
    {
        return $this->hasMany(\App\Models\Application::class, 'windowsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function events()
    {
        return $this->belongsTo(\App\Models\Event::class, 'eventsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function reservations()
    {
        return $this->hasMany(\App\Models\Reservation::class, 'windowsID');
    }
}
