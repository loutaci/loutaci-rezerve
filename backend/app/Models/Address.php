<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Address
 *
 * @package App\Models
 * @version March 23, 2022, 9:11 am UTC
 * @property \Illuminate\Database\Eloquent\Collection $customers
 * @property string $number
 * @property string $postal_code
 * @property string $street
 * @property int $addressesID
 * @property string $city
 * @property string $state
 * @property-read int|null $customers_count
 * @method static \Database\Factories\AddressFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Address query()
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereAddressesID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereStreet($value)
 * @mixin Model
 */
class Address extends Model
{
    use HasFactory;

    public $table = 'addresses';
    public $primaryKey = 'addressesID';

    public $timestamps = false;



    public $fillable = [
        'number',
        'postal_code',
        'street'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'number' => 'string',
        'postal_code' => 'string',
        'street' => 'string',
        'addressesID' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'number' => 'required|string|max:10',
        'postal_code' => 'required|string|max:30',
        'street' => 'required|string|max:50'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function customers()
    {
        return $this->hasMany(\App\Models\Customer::class, 'addressesID');
    }
}
