<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Application
 *
 * @package App\Models
 * @version April 2, 2022, 10:19 pm UTC
 * @property \App\Models\Window $windowsid
 * @property string $url
 * @property integer $windowsID
 * @property int $applicationsID
 * @property-read \App\Models\Window $windows
 * @method static \Database\Factories\ApplicationFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Application newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Application newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Application query()
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereApplicationsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Application whereWindowsID($value)
 * @mixin Model
 */
class Application extends Model
{

    use HasFactory;

    public $table = 'applications';
    public $primaryKey = 'applicationsID';

    public $timestamps = false;




    public $fillable = [
        'url',
        'windowsID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'url' => 'string',
        'applicationsID' => 'integer',
        'windowsID' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'url' => 'required|url|max:200',
        'windowsID' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function windows()
    {
        return $this->belongsTo(\App\Models\Window::class, 'windowsID');
    }
}
