<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PortalUsers
 *
 * @package App\Models
 * @version April 27, 2022, 9:06 am UTC
 * @property \App\Models\Portal $portalsid
 * @property \App\Models\User $usersid
 * @property \App\Models\Role $roleid
 * @property integer $portalsID
 * @property integer $usersID
 * @property boolean $roleID
 * @property int $portalusersID
 * @property-read \App\Models\Portal $portals
 * @property-read \App\Models\User $users
 * @method static \Database\Factories\PortalUsersFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|PortalUsers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PortalUsers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PortalUsers query()
 * @method static \Illuminate\Database\Eloquent\Builder|PortalUsers wherePortalsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortalUsers wherePortalusersID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortalUsers whereRoleID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PortalUsers whereUsersID($value)
 * @mixin Model
 * @property-read \App\Models\Role $role
 */
class PortalUsers extends Model
{
    use HasFactory;

    public $table = 'portalusers';
    public $primaryKey = 'portalusersID';

    public $timestamps = false;


    public $fillable = [
        'portalsID',
        'usersID',
        'roleID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'portalusersID' => 'integer',
        'portalsID' => 'integer',
        'usersID' => 'integer',
        'roleID' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'portalsID' => 'required|integer',
        'usersID' => 'required|integer',
        'roleID' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function portals()
    {
        return $this->belongsTo(\App\Models\Portal::class, 'portalsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function users()
    {
        return $this->belongsTo(\App\Models\User::class, 'usersID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function role()
    {
        return $this->belongsTo(\App\Models\Role::class, 'roleID');
    }
}
