<?php

namespace App\Models;


use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

use \Illuminate\Database\Eloquent\Casts\Attribute;
/**
 * Class Form
 *
 * @package App\Models
 * @version April 2, 2022, 5:41 pm UTC
 * @property \Illuminate\Database\Eloquent\Collection $windows
 * @property string $config
 * @property string $description
 * @property string $name
 * @property int $formsID
 * @property-read int|null $windows_count
 * @method static \Database\Factories\FormFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Form newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Form newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Form query()
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereFormsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Form whereName($value)
 * @mixin Model
 */
class Form extends Model
{
    // use \Eloquence\Behaviours\CamelCasing;

    use HasFactory;

    public $table = 'forms';
    public $primaryKey = 'formsID';

    public $timestamps = false;

    public $fillable = [
        'config',
        'description',
        'name',
        'portalsID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'config' => 'json',
        'description' => 'string',
        'name' => 'string',
        'formsID' => 'integer',
        'portalsID' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'config' => 'nullable|json',
        'description' => 'nullable|string|max:999',
        'name' => 'required|string|max:999',
        'portalsID' => 'nullable|integer',
    ];

    // public function setAttribute($key, $value)
    // {
    //     // super();
    //     if ($key == 'portalsID') {
    //         $value = user_portal();
    //     }
    //     return $value;
    // }

    public function getConfigAttribute($value) {
        return json_decode($value);
    }

    public function setConfigAttribute($value) {
        $this->attributes['config'] = $value;
    }



    // public function name(): Attribute
    // {
    //     return Attribute::set(fn() => 'helsaslo');
    //     // return Attribute::make(
    //     //     get: fn ($value, $attributes) => 'Hello',
    //     //     set: fn (Address $value) => [
    //     //         'address_line_one' => $value->lineOne,
    //     //         'address_line_two' => $value->lineTwo,
    //     //     ],
    //     // );
    // }

    // public function portalsID(): Attribute
    // {
    //     return Attribute::set(fn() => 1);
    //     // return Attribute::make(
    //     //     get: fn ($value, $attributes) => 'Hello',
    //     //     set: fn (Address $value) => [
    //     //         'address_line_one' => $value->lineOne,
    //     //         'address_line_two' => $value->lineTwo,
    //     //     ],
    //     // );
    // }



    // public function getPortalsIDAttribute($value) {
    //     // $this->attributes['portalsID'] = $value;
    //     return 123;
    // }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function windows()
    {
        return $this->hasMany(\App\Models\Window::class, 'formsID');
    }
}
