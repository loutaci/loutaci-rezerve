<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Event
 *
 * @package App\Models
 * @version April 2, 2022, 7:44 pm UTC
 * @property \App\Models\Window $windowsid
 * @property \App\Models\Address $addressesid
 * @property \Illuminate\Database\Eloquent\Collection $reservations
 * @property integer $capacity
 * @property string $date_end
 * @property string $date_start
 * @property string $description
 * @property string $name
 * @property number $price
 * @property time $time_end
 * @property time $time_start
 * @property integer $addressesID
 * @property integer $windowsID
 * @property int $eventsID
 * @property-read \App\Models\Address|null $addresses
 * @property-read int|null $reservations_count
 * @property-read \App\Models\Window|null $windows
 * @method static \Database\Factories\EventFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereAddressesID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDateEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDateStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereEventsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereTimeEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereTimeStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereWindowsID($value)
 * @mixin Model
 */
class Event extends Model
{
    use HasFactory;

    public $table = 'events';
    public $primaryKey = 'eventsID';

    public $timestamps = false;


    public $fillable = [
        'capacity',
        'date_end',
        'date_start',
        'description',
        'name',
        'price',
        'time_end',
        'time_start',
        'addressesID',
        'windowsID',
        'portalsID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'capacity' => 'integer',
        'date_end' => 'date',
        'date_start' => 'date',
        'description' => 'string',
        'name' => 'string',
        'price' => 'float',
        'eventsID' => 'integer',
        'addressesID' => 'integer',
        'windowsID' => 'integer',
        'portalsID' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'capacity' => 'required|integer',
        'date_end' => 'required',
        'date_start' => 'required',
        'description' => 'nullable|string',
        'name' => 'required|string',
        'price' => 'nullable|numeric',
        'time_end' => 'nullable',
        'time_start' => 'nullable',
        'addressesID' => 'nullable|integer',
        'windowsID' => 'nullable|integer',
        'portalsID' => 'integer',
    ];

    // public function setAttribute($key, $value)
    // {
    //     if ($key == 'portalsID') {
    //         $value = user_portal();
    //     }
    //     return $value;
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function windows()
    {
        return $this->hasOne(\App\Models\Window::class, 'windowsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function addresses()
    {
        return $this->belongsTo(\App\Models\Address::class, 'addressesID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function reservations()
    {
        return $this->hasMany(\App\Models\Reservation::class, 'eventsID');
    }
}
