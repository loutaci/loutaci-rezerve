<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Portal
 *
 * @package App\Models
 * @version April 2, 2022, 7:01 pm UTC
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $windows
 * @property int $portalsID
 * @property string $name
 * @property-read int|null $users_count
 * @property-read int|null $windows_count
 * @method static \Database\Factories\PortalFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Portal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Portal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Portal query()
 * @method static \Illuminate\Database\Eloquent\Builder|Portal whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Portal wherePortalsID($value)
 * @mixin Model
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $portalOwner
 * @property-read int|null $portal_owner_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $portalusers
 * @property-read int|null $portalusers_count
 */
class Portal extends Model
{
    use HasFactory;

    public $table = 'portals';
    public $primaryKey = 'portalsID';

    public $timestamps = false;

    // const CREATED_AT = 'created_at';
    // const UPDATED_AT = 'updated_at';


    // protected $dates = ['deleted_at'];

    public $fillable = [
        'name' => 'string'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'portalsID' => 'integer',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
    public function portalusers()
    {
        return $this->hasMany(\App\Models\PortalUsers::class, 'portalsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function windows()
    {
        return $this->hasMany(\App\Models\Window::class, 'portalsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function forms()
    {
        return $this->hasMany(\App\Models\Form::class, 'portalsID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function events()
    {
        return $this->hasMany(\App\Models\Event::class, 'portalsID');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function calendars()
    {
        return $this->hasMany(\App\Models\Calendar::class, 'portalsID');
    }

    // public function portals()
    // {
    //     return $this->belongsTo(\App\Models\Portal::class, 'portalsID');
    // }
    public function portalOwner()
    {
        return $this->hasManyThrough(User::class, PortalUsers::class);
    }
}
