<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Calendar
 *
 * @package App\Models
 * @version April 2, 2022, 5:39 pm UTC
 * @property \Illuminate\Database\Eloquent\Collection $windows
 * @property string $config
 * @property string $description
 * @property string $type
 * @property int $calendarsID
 * @property-read int|null $windows_count
 * @method static \Database\Factories\CalendarFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Calendar newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Calendar newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Calendar query()
 * @method static \Illuminate\Database\Eloquent\Builder|Calendar whereCalendarsID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Calendar whereConfig($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Calendar whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Calendar whereType($value)
 * @mixin Model
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|Calendar whereName($value)
 */
class Calendar extends Model
{
    use HasFactory;

    public $table = 'calendars';
    public $primaryKey = 'calendarsID';

    public $timestamps = false;

    // const CREATED_AT = 'created_at';
    // const UPDATED_AT = 'updated_at';


    // protected $dates = ['deleted_at'];



    public $fillable = [
        'config',
        'description',
        'type',
        'name',
        'portalsID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'config' => 'json',
        'description' => 'string',
        'type' => 'string',
        'calendarsID' => 'integer',
        'name' => 'string',
        'portalsID' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'config' => 'required|json',
        'description' => 'nullable|string|max:100',
        'type' => 'required|string|max:100',
        'name' => 'required|string|max:100',
        'portalsID' => 'nullable|integer'
    ];

    public function getConfigAttribute($value) {
        return json_decode($value);
    }

    public function setConfigAttribute($value) {
        $this->attributes['config'] = $value;
    }

    // public function setAttribute($key, $value)
    // {
    //     if ($key == 'portalsID') {
    //         $value = user_portal();
    //     }
    //     return $value;
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function windows()
    {
        return $this->hasMany(\App\Models\Window::class, 'calendarsID');
    }
}
