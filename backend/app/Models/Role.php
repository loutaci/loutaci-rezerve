<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Role
 *
 * @package App\Models
 * @version May 9, 2022, 11:55 am UTC
 * @property \Illuminate\Database\Eloquent\Collection $portalusers
 * @property string $role
 * @property bool $roleID
 * @method static \Database\Factories\RoleFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereRoleID($value)
 * @mixin Model
 * @property-read int|null $portalusers_count
 */
class Role extends Model
{
    use HasFactory;

    public $table = 'roles';
    public $primaryKey = 'roleID';


    public $timestamps = false;

    public $fillable = [
        'role'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'roleID' => 'integer',
        'role' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'role' => 'required|string|max:6'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function portalusers()
    {
        return $this->hasMany(\App\Models\PortalUsers::class, 'roleID');
    }
}
