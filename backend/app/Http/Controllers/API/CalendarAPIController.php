<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCalendarAPIRequest;
use App\Http\Requests\API\UpdateCalendarAPIRequest;
use App\Models\Calendar;
use App\Repositories\CalendarRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Reservation;
use Response;

/**
 * Class CalendarController
 * @package App\Http\Controllers\API
 */

class CalendarAPIController extends AppBaseController
{
    /** @var  CalendarRepository */
    private $calendarRepository;

    public function __construct(CalendarRepository $calendarRepo)
    {
        $this->calendarRepository = $calendarRepo;
    }

    /**
     * Display a listing of the Calendar.
     * GET|HEAD /calendars
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $calendars = $this->calendarRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );

        return $this->sendResponse(user_portal()->calendars->toArray(), 'Calendars retrieved successfully');
    }

    /**
     * Store a newly created Calendar in storage.
     * POST /calendars
     *
     * @param CreateCalendarAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCalendarAPIRequest $request)
    {
        $input = $request->all();

        $calendar = $this->calendarRepository->create($input);

        return $this->sendResponse($calendar->toArray(), 'Calendar saved successfully');
    }

    /**
     * Display the specified Calendar.
     * GET|HEAD /calendars/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Calendar $calendar */
        $calendar = $this->calendarRepository->find($id);

        if (empty($calendar)) {
            return $this->sendError('Calendar not found');
        }
        $data = $calendar->toArray();
        // $data['occupied'] = Reservation::whereWindowsID();

        return $this->sendResponse($data, 'Calendar retrieved successfully');
    }

    /**
     * Update the specified Calendar in storage.
     * PUT/PATCH /calendars/{id}
     *
     * @param int $id
     * @param UpdateCalendarAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCalendarAPIRequest $request)
    {
        $input = $request->all();

        /** @var Calendar $calendar */
        $calendar = $this->calendarRepository->find($id);

        if (empty($calendar)) {
            return $this->sendError('Calendar not found');
        }

        $calendar = $this->calendarRepository->update($input, $id);

        return $this->sendResponse($calendar->toArray(), 'Calendar updated successfully');
    }

    /**
     * Remove the specified Calendar from storage.
     * DELETE /calendars/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Calendar $calendar */
        $calendar = $this->calendarRepository->find($id);

        if (empty($calendar)) {
            return $this->sendError('Calendar not found');
        }

        $calendar->delete();

        return $this->sendSuccess('Calendar deleted successfully');
    }
}
