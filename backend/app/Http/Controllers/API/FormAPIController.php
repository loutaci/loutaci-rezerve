<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFormAPIRequest;
use App\Http\Requests\API\UpdateFormAPIRequest;
use App\Models\Form;
use App\Repositories\FormRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class FormController
 * @package App\Http\Controllers\API
 */

class FormAPIController extends AppBaseController
{
    /** @var  FormRepository */
    private $formRepository;

    public function __construct(FormRepository $formRepo)
    {
        $this->formRepository = $formRepo;
    }

    /**
     * Display a listing of the Form.
     * GET|HEAD /forms
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $forms = $this->formRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );


        return $this->sendResponse(user_portal()->forms->toArray(), 'Forms retrieved successfully');
    }

    /**
     * Store a newly created Form in storage.
     * POST /forms
     *
     * @param CreateFormAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFormAPIRequest $request)
    {
        $input = $request->all();

        $form = $this->formRepository->create($input);

        return $this->sendResponse($form->toArray(), 'Form saved successfully');
    }

    /**
     * Display the specified Form.
     * GET|HEAD /forms/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Form $form */
        $form = $this->formRepository->find($id);

        if (empty($form)) {
            return $this->sendError('Form not found');
        }

        return $this->sendResponse($form->toArray(), 'Form retrieved successfully');
    }

    /**
     * Update the specified Form in storage.
     * PUT/PATCH /forms/{id}
     *
     * @param int $id
     * @param UpdateFormAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFormAPIRequest $request)
    {
        $input = $request->all();

        /** @var Form $form */
        $form = $this->formRepository->find($id);

        if (empty($form)) {
            return $this->sendError('Form not found');
        }

        $form = $this->formRepository->update($input, $id);

        return $this->sendResponse($form->toArray(), 'Form updated successfully');
    }

    /**
     * Remove the specified Form from storage.
     * DELETE /forms/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Form $form */
        $form = $this->formRepository->find($id);

        if (empty($form)) {
            return $this->sendError('Form not found');
        }

        $form->delete();

        return $this->sendSuccess('Form deleted successfully');
    }
}
