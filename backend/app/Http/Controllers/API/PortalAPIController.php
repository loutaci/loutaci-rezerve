<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePortalAPIRequest;
use App\Http\Requests\API\UpdatePortalAPIRequest;
use App\Models\Portal;
use App\Repositories\PortalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\PortalUsers;
use Response;

/**
 * Class PortalController
 * @package App\Http\Controllers\API
 */

class PortalAPIController extends AppBaseController
{
    /** @var  PortalRepository */
    private $portalRepository;

    public function __construct(PortalRepository $portalRepo)
    {
        $this->portalRepository = $portalRepo;
    }

    /**
     * Display a listing of the Portal.
     * GET|HEAD /portals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $portals = $this->portalRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );
        // return request()->user()->portals();

        return $this->sendResponse(request()->user()->portals(), 'Portals retrieved successfully');
    }

    /**
     * Store a newly created Portal in storage.
     * POST /portals
     *
     * @param CreatePortalAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePortalAPIRequest $request)
    {
        $input = $request->all();

        // $portal = $this->portalRepository->create($input);
        $portal = new Portal();
        $portal['name'] = $input['name'];
        $portal->save();

        $portalusers = new PortalUsers();
        $portalusers['portalsID'] = $portal->portalsID;
        $portalusers['usersID'] = request()->user()->usersID;
        $portalusers['roleID'] = 1;
        $portalusers->save();
        // dd($portal);
        // $portal['']

        return $this->sendResponse($portal->toArray(), 'Portal saved successfully');
    }

    /**
     * Display the specified Portal.
     * GET|HEAD /portals/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Portal $portal */
        $portal = $this->portalRepository->find($id);

        if (empty($portal)) {
            return $this->sendError('Portal not found');
        }

        return $this->sendResponse($portal->toArray(), 'Portal retrieved successfully');
    }

    /**
     * Update the specified Portal in storage.
     * PUT/PATCH /portals/{id}
     *
     * @param int $id
     * @param UpdatePortalAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePortalAPIRequest $request)
    {
        $input = $request->all();

        /** @var Portal $portal */
        $portal = $this->portalRepository->find($id);

        if (empty($portal)) {
            return $this->sendError('Portal not found');
        }

        $portal = $this->portalRepository->update($input, $id);

        return $this->sendResponse($portal->toArray(), 'Portal updated successfully');
    }

    /**
     * Remove the specified Portal from storage.
     * DELETE /portals/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Portal $portal */
        $portal = $this->portalRepository->find($id);

        if (empty($portal)) {
            return $this->sendError('Portal not found');
        }

        $portal->delete();

        return $this->sendSuccess('Portal deleted successfully');
    }
}
