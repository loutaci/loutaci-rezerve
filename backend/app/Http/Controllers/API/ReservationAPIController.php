<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReservationAPIRequest;
use App\Http\Requests\API\UpdateReservationAPIRequest;
use App\Models\Reservation;
use App\Repositories\ReservationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Mail\ReservationInfo;
use App\Models\Customer;
use Mail;
use Response;

/**
 * Class ReservationController
 * @package App\Http\Controllers\API
 */

class ReservationAPIController extends AppBaseController
{
    /** @var  ReservationRepository */
    private $reservationRepository;

    public function __construct(ReservationRepository $reservationRepo)
    {
        $this->reservationRepository = $reservationRepo;
    }

    /**
     * Display a listing of the Reservation.
     * GET|HEAD /reservations
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $reservations = $this->reservationRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($reservations->toArray(), 'Reservations retrieved successfully');
    }

    /**
     * Store a newly created Reservation in storage.
     * POST /reservations
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = request()->all();
        // dd($request->customer);
        // dd($input);
        // $input = request()->get('customer_data');
        // return dd($input);

        // dd($input);

        $reservation = new Reservation($input);
        // $customer = new Customer();
        // $customer->name;
        $customer = Customer::whereEmail($request->customer['email'])->first();
        if (empty($customer)) {
            $customer = new Customer($request->customer);
            $customer->save();
        }
        $reservation->data = $request->data;
        $reservation->customersID = $customer->customersID;
        $reservation->save();
        // $customer = Customer::findOrFail($reservation->customersID);
        Mail::to($customer->email)->send(new ReservationInfo($reservation, $customer));
        // subject('Reservation Info')
        // ->view('emails.reservation-info');

        return $this->sendResponse($reservation->toArray(), 'Reservation saved successfully');
    }

    /**
     * Display the specified Reservation.
     * GET|HEAD /reservations/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Reservation $reservation */
        $reservation = $this->reservationRepository->find($id);

        if (empty($reservation)) {
            return $this->sendError('Reservation not found');
        }

        return $this->sendResponse($reservation->toArray(), 'Reservation retrieved successfully');
    }

    /**
     * Update the specified Reservation in storage.
     * PUT/PATCH /reservations/{id}
     *
     * @param int $id
     * @param UpdateReservationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReservationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Reservation $reservation */
        $reservation = $this->reservationRepository->find($id);

        if (empty($reservation)) {
            return $this->sendError('Reservation not found');
        }

        $reservation = $this->reservationRepository->update($input, $id);

        return $this->sendResponse($reservation->toArray(), 'Reservation updated successfully');
    }

    /**
     * Remove the specified Reservation from storage.
     * DELETE /reservations/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Reservation $reservation */
        $reservation = $this->reservationRepository->find($id);

        if (empty($reservation)) {
            return $this->sendError('Reservation not found');
        }

        $reservation->delete();

        return $this->sendSuccess('Reservation deleted successfully');
    }
}
