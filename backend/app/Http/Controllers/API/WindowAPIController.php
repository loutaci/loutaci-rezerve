<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateWindowAPIRequest;
use App\Http\Requests\API\UpdateWindowAPIRequest;
use App\Models\Window;
use App\Repositories\WindowRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\CalendarExceptions;
use App\Models\Portal;
use App\Models\Reservation;
use Carbon\Carbon;
use Response;

/**
 * Class WindowController
 * @package App\Http\Controllers\API
 */

class WindowAPIController extends AppBaseController
{
    /** @var  WindowRepository */
    private $windowRepository;

    public function __construct(WindowRepository $windowRepo)
    {
        // $this->middleware('auth:sanctum', ['include' => ['index', 'store', 'update', 'destroy'],'exclude' => ['getForm', 'getCalendar', 'getPortal', 'getReservation','getEvents']]);
        $this->windowRepository = $windowRepo;
    }

    /**
     * Display a listing of the Window.
     * GET|HEAD /windows
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // return user_portal();
        // $portalid = intval(request()->header('Portal-Id'));
        // $portal = Portal::find($portalid);
        // return $portal->windows;
        // $windows = $this->windowRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // )->load(['forms']);
        // $t = user_portal();
        // return $t;
        // return 'wtf ';
        // return request()->user();
        return $this->sendResponse(user_portal()->windows->toArray(), 'Windows retrieved successfully');
    }

    /**
     * Store a newly created Window in storage.
     * POST /windows
     *
     * @param CreateWindowAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateWindowAPIRequest $request)
    {
        $input = $request->all();

        $window = $this->windowRepository->create($input);

        return $this->sendResponse($window->toArray(), 'Window saved successfully');
    }

    /**
     * Display the specified Window.
     * GET|HEAD /windows/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Window $window */
        $window = Window::with(['forms', 'calendars', 'events'])->find($id);

        if (empty($window)) {
            return $this->sendError('Window not found');
        }

        return $this->sendResponse($window->toArray(), 'Window retrieved successfully');
    }


    public function getForm($windowId)
    {
        /** @var Window $window */
        $window = Window::find($windowId);

        if (empty($window->forms)) {
            return $this->sendError('Form for this window not found');
        }

        return $this->sendResponse($window->forms->toArray(), 'Forms retrieved successfully');
    }

    public function getCalendar($windowId)
    {
        /** @var Window $window */
        $window = Window::with(['calendars'])->find($windowId);

        if (empty($window->calendars)) {
            return $this->sendError('Calendar for this window not found');
        }
        $data = $window->toArray();

        $data['occupied'] = [];
        $data['exceptions'] = [];

        $reservations = Reservation::where('windowsID', $windowId)->where('date_end', '>', Carbon::now()->subMonth())->get();
        $exceptions = CalendarExceptions::where('windowsID', $windowId)->where('date_end', '>', Carbon::now()->subMonth())->get();

        // dd($reservations,$data);
        foreach ($reservations as $r) {
            array_push($data['occupied'], ['date_start' => $r->date_start, 'date_end' => $r->date_end, 'time_start' => $r->time_start, 'time_end' => $r->time_end]);
        }

        foreach ($exceptions as $e) {
            array_push($data['exceptions'], ['id' => $e->id, 'date_start' => $e->date_start, 'date_end' => $e->date_end, 'time_start' => $e->time_start, 'time_end' => $e->time_end, 'config' => $e->config]);
        }

        return $this->sendResponse($data, 'Calendar retrieved successfully');
    }

    public function getReservations($windowId)
    {
        /** @var Window $window */
        $window = Window::find($windowId);
        $window->reservations->load('customer');
        // if (empty($window->reservations)) {
        //     return $this->sendError('Reservation for this window not found');
        // }


        return $this->sendResponse($window->reservations->toArray(), 'Reservations retrieved successfully');
    }

    public function getEvents($windowId)
    {
        /** @var Window $window */
        $window = Window::find($windowId)->load('events');
        $data = $window->toArray();

        $reservations = Reservation::with('customer')->where('windowsID', $windowId)->get();
        $data['reservations'] = $reservations;
        $data['paidCount'] = $reservations->where("paid", "=", "1")->count();
        $data['reservationsCount'] = $reservations->count();

        if (empty($window->events)) {
            return $this->sendError('Reservation for this window not found');
        }


        return $this->sendResponse($data, 'Events retrieved successfully');
    }

    /**
     * Update the specified Window in storage.
     * PUT/PATCH /windows/{id}
     *
     * @param int $id
     * @param UpdateWindowAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWindowAPIRequest $request)
    {
        $input = $request->all();

        /** @var Window $window */
        $window = $this->windowRepository->find($id);

        if (empty($window)) {
            return $this->sendError('Window not found');
        }

        $window = $this->windowRepository->update($input, $id);

        return $this->sendResponse($window->toArray(), 'Window updated successfully');
    }

    /**
     * Remove the specified Window from storage.
     * DELETE /windows/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Window $window */
        $window = $this->windowRepository->find($id);

        if (empty($window)) {
            return $this->sendError('Window not found');
        }

        $window->delete();

        return $this->sendSuccess('Window deleted successfully');
    }
}
