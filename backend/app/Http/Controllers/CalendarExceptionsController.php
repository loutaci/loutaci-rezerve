<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCalendarExceptionsRequest;
use App\Http\Requests\UpdateCalendarExceptionsRequest;
use App\Repositories\CalendarExceptionsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class CalendarExceptionsController extends AppBaseController
{
    /** @var CalendarExceptionsRepository $calendarExceptionsRepository*/
    private $calendarExceptionsRepository;

    public function __construct(CalendarExceptionsRepository $calendarExceptionsRepo)
    {
        $this->calendarExceptionsRepository = $calendarExceptionsRepo;
    }

    /**
     * Display a listing of the CalendarExceptions.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $calendarExceptions = $this->calendarExceptionsRepository->all();

        return view('calendar_exceptions.index')
            ->with('calendarExceptions', $calendarExceptions);
    }

    /**
     * Show the form for creating a new CalendarExceptions.
     *
     * @return Response
     */
    public function create()
    {
        return view('calendar_exceptions.create');
    }

    /**
     * Store a newly created CalendarExceptions in storage.
     *
     * @param CreateCalendarExceptionsRequest $request
     *
     * @return Response
     */
    public function store(CreateCalendarExceptionsRequest $request)
    {
        $input = $request->all();

        $calendarExceptions = $this->calendarExceptionsRepository->create($input);

        Flash::success('Calendar Exceptions saved successfully.');

        return redirect(route('calendarExceptions.index'));
    }

    /**
     * Display the specified CalendarExceptions.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $calendarExceptions = $this->calendarExceptionsRepository->find($id);

        if (empty($calendarExceptions)) {
            Flash::error('Calendar Exceptions not found');

            return redirect(route('calendarExceptions.index'));
        }

        return view('calendar_exceptions.show')->with('calendarExceptions', $calendarExceptions);
    }

    /**
     * Show the form for editing the specified CalendarExceptions.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $calendarExceptions = $this->calendarExceptionsRepository->find($id);

        if (empty($calendarExceptions)) {
            Flash::error('Calendar Exceptions not found');

            return redirect(route('calendarExceptions.index'));
        }

        return view('calendar_exceptions.edit')->with('calendarExceptions', $calendarExceptions);
    }

    /**
     * Update the specified CalendarExceptions in storage.
     *
     * @param int $id
     * @param UpdateCalendarExceptionsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCalendarExceptionsRequest $request)
    {
        $calendarExceptions = $this->calendarExceptionsRepository->find($id);

        if (empty($calendarExceptions)) {
            Flash::error('Calendar Exceptions not found');

            return redirect(route('calendarExceptions.index'));
        }

        $calendarExceptions = $this->calendarExceptionsRepository->update($request->all(), $id);

        Flash::success('Calendar Exceptions updated successfully.');

        return redirect(route('calendarExceptions.index'));
    }

    /**
     * Remove the specified CalendarExceptions from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $calendarExceptions = $this->calendarExceptionsRepository->find($id);

        if (empty($calendarExceptions)) {
            Flash::error('Calendar Exceptions not found');

            return redirect(route('calendarExceptions.index'));
        }

        $this->calendarExceptionsRepository->delete($id);

        Flash::success('Calendar Exceptions deleted successfully.');

        return redirect(route('calendarExceptions.index'));
    }
}
