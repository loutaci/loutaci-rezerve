<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePortalUsersRequest;
use App\Http\Requests\UpdatePortalUsersRequest;
use App\Repositories\PortalUsersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class PortalUsersController extends AppBaseController
{
    /** @var PortalUsersRepository $portalUsersRepository*/
    private $portalUsersRepository;

    public function __construct(PortalUsersRepository $portalUsersRepo)
    {
        $this->portalUsersRepository = $portalUsersRepo;
    }

    /**
     * Display a listing of the PortalUsers.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $portalUsers = $this->portalUsersRepository->all();

        return view('portal_users.index')
            ->with('portalUsers', $portalUsers);
    }

    /**
     * Show the form for creating a new PortalUsers.
     *
     * @return Response
     */
    public function create()
    {
        return view('portal_users.create');
    }

    /**
     * Store a newly created PortalUsers in storage.
     *
     * @param CreatePortalUsersRequest $request
     *
     * @return Response
     */
    public function store(CreatePortalUsersRequest $request)
    {
        $input = $request->all();

        $portalUsers = $this->portalUsersRepository->create($input);

        Flash::success('Portal Users saved successfully.');

        return redirect(route('portalUsers.index'));
    }

    /**
     * Display the specified PortalUsers.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $portalUsers = $this->portalUsersRepository->find($id);

        if (empty($portalUsers)) {
            Flash::error('Portal Users not found');

            return redirect(route('portalUsers.index'));
        }

        return view('portal_users.show')->with('portalUsers', $portalUsers);
    }

    /**
     * Show the form for editing the specified PortalUsers.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $portalUsers = $this->portalUsersRepository->find($id);

        if (empty($portalUsers)) {
            Flash::error('Portal Users not found');

            return redirect(route('portalUsers.index'));
        }

        return view('portal_users.edit')->with('portalUsers', $portalUsers);
    }

    /**
     * Update the specified PortalUsers in storage.
     *
     * @param int $id
     * @param UpdatePortalUsersRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePortalUsersRequest $request)
    {
        $portalUsers = $this->portalUsersRepository->find($id);

        if (empty($portalUsers)) {
            Flash::error('Portal Users not found');

            return redirect(route('portalUsers.index'));
        }

        $portalUsers = $this->portalUsersRepository->update($request->all(), $id);

        Flash::success('Portal Users updated successfully.');

        return redirect(route('portalUsers.index'));
    }

    /**
     * Remove the specified PortalUsers from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $portalUsers = $this->portalUsersRepository->find($id);

        if (empty($portalUsers)) {
            Flash::error('Portal Users not found');

            return redirect(route('portalUsers.index'));
        }

        $this->portalUsersRepository->delete($id);

        Flash::success('Portal Users deleted successfully.');

        return redirect(route('portalUsers.index'));
    }
}
