<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWindowRequest;
use App\Http\Requests\UpdateWindowRequest;
use App\Repositories\WindowRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class WindowController extends AppBaseController
{
    /** @var WindowRepository $windowRepository*/
    private $windowRepository;

    public function __construct(WindowRepository $windowRepo)
    {
        $this->windowRepository = $windowRepo;
    }

    /**
     * Display a listing of the Window.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $windows = $this->windowRepository->all();

        return view('windows.index')
            ->with('windows', $windows);
    }

    /**
     * Show the form for creating a new Window.
     *
     * @return Response
     */
    public function create()
    {
        return view('windows.create');
    }

    /**
     * Store a newly created Window in storage.
     *
     * @param CreateWindowRequest $request
     *
     * @return Response
     */
    public function store(CreateWindowRequest $request)
    {
        $input = $request->all();

        $window = $this->windowRepository->create($input);

        Flash::success('Window saved successfully.');

        return redirect(route('windows.index'));
    }

    /**
     * Display the specified Window.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $window = $this->windowRepository->find($id);

        if (empty($window)) {
            Flash::error('Window not found');

            return redirect(route('windows.index'));
        }

        return view('windows.show')->with('window', $window);
    }

    /**
     * Show the form for editing the specified Window.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $window = $this->windowRepository->find($id);

        if (empty($window)) {
            Flash::error('Window not found');

            return redirect(route('windows.index'));
        }

        return view('windows.edit')->with('window', $window);
    }

    /**
     * Update the specified Window in storage.
     *
     * @param int $id
     * @param UpdateWindowRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWindowRequest $request)
    {
        $window = $this->windowRepository->find($id);

        if (empty($window)) {
            Flash::error('Window not found');

            return redirect(route('windows.index'));
        }

        $window = $this->windowRepository->update($request->all(), $id);

        Flash::success('Window updated successfully.');

        return redirect(route('windows.index'));
    }

    /**
     * Remove the specified Window from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $window = $this->windowRepository->find($id);

        if (empty($window)) {
            Flash::error('Window not found');

            return redirect(route('windows.index'));
        }

        $this->windowRepository->delete($id);

        Flash::success('Window deleted successfully.');

        return redirect(route('windows.index'));
    }
}
