<?php

use App\Models\Portal;



/**
 * Get an instance of the current request or an input item from the request.
 *
 * @param  array|string|null  $key
 * @param  mixed  $default
 * @return Portal
 */
function user_portal($do_not_throw = false)
{
    // return request()->user();
    $portalid = intval(request()->header('Portal-Id'));
    $portal = Portal::findOrFail($portalid);
    $portals = request()->user()?->portals;
    // dd($portals);

    $data = $portals?->filter(fn ($value, $key) => $value->portalsID == $portalid);
    // return $data;
    // dd($data);
    if ($do_not_throw) {
        return $portal;
    } else {
        return empty($data) ? response("You don't have access to this portal")->throwResponse() : $portal;
    }

    // return 'hello';
    // if (is_null($key)) {
    //     return app('request');
    // }

    // if (is_array($key)) {
    //     return app('request')->only($key);
    // }

    // $value = app('request')->__get($key);

    // return is_null($value) ? value($default) : $value;
}
