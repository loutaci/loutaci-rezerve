<?php

namespace App\Repositories;

use App\Models\PortalUsers;
use App\Repositories\BaseRepository;

/**
 * Class PortalUsersRepository
 * @package App\Repositories
 * @version April 27, 2022, 9:06 am UTC
*/

class PortalUsersRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'portalsID',
        'usersID',
        'roleID'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PortalUsers::class;
    }
}
