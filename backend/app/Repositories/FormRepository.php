<?php

namespace App\Repositories;

use App\Models\Form;
use App\Repositories\BaseRepository;

/**
 * Class FormRepository
 * @package App\Repositories
 * @version April 2, 2022, 5:41 pm UTC
*/

class FormRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'config',
        'description',
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Form::class;
    }
}
