<?php

namespace App\Repositories;

use App\Models\Window;
use App\Repositories\BaseRepository;

/**
 * Class WindowRepository
 * @package App\Repositories
 * @version April 2, 2022, 5:21 pm UTC
*/

class WindowRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'calendarsID',
        'formsID',
        'portalsID'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Window::class;
    }
}
