<?php

namespace App\Repositories;

use App\Models\CalendarExceptions;
use App\Repositories\BaseRepository;

/**
 * Class CalendarExceptionsRepository
 * @package App\Repositories
 * @version April 17, 2022, 3:57 pm UTC
*/

class CalendarExceptionsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'windowsID',
        'date_start',
        'date_end',
        'time_start',
        'time_end',
        'description',
        'config'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CalendarExceptions::class;
    }
}
