<?php

namespace App\Repositories;

use App\Models\Calendar;
use App\Repositories\BaseRepository;

/**
 * Class CalendarRepository
 * @package App\Repositories
 * @version April 2, 2022, 5:39 pm UTC
*/

class CalendarRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'config',
        'description',
        'type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Calendar::class;
    }
}
