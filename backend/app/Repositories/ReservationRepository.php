<?php

namespace App\Repositories;

use App\Models\Reservation;
use App\Repositories\BaseRepository;

/**
 * Class ReservationRepository
 * @package App\Repositories
 * @version April 2, 2022, 7:49 pm UTC
*/

class ReservationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'data',
        'date_end',
        'date_start',
        'time_end',
        'time_start',
        'customersID',
        'eventsID',
        'windowsID'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reservation::class;
    }
}
