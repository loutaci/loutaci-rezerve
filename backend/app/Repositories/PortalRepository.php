<?php

namespace App\Repositories;

use App\Models\Portal;
use App\Repositories\BaseRepository;

/**
 * Class PortalRepository
 * @package App\Repositories
 * @version April 2, 2022, 7:01 pm UTC
*/

class PortalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name' => 'like'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Portal::class;
    }
}
