<?php

namespace Database\Factories;

use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'capacity' => $this->faker->randomDigitNotNull,
        'date_end' => $this->faker->word,
        'date_start' => $this->faker->word,
        'description' => $this->faker->word,
        'name' => $this->faker->word,
        'price' => $this->faker->randomDigitNotNull,
        'time_end' => $this->faker->word,
        'time_start' => $this->faker->word,
        'addressesID' => $this->faker->randomDigitNotNull,
        'windowsID' => $this->faker->randomDigitNotNull
        ];
    }
}
