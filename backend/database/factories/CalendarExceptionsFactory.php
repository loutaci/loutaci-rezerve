<?php

namespace Database\Factories;

use App\Models\CalendarExceptions;
use Illuminate\Database\Eloquent\Factories\Factory;

class CalendarExceptionsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CalendarExceptions::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'windowsID' => $this->faker->randomDigitNotNull,
        'date_start' => $this->faker->word,
        'date_end' => $this->faker->word,
        'time_start' => $this->faker->word,
        'time_end' => $this->faker->word,
        'description' => $this->faker->word,
        'config' => $this->faker->text
        ];
    }
}
