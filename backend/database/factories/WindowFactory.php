<?php

namespace Database\Factories;

use App\Models\Window;
use Illuminate\Database\Eloquent\Factories\Factory;

class WindowFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Window::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'calendarsID' => $this->faker->randomDigitNotNull,
        'formsID' => $this->faker->randomDigitNotNull,
        'portalsID' => $this->faker->randomDigitNotNull
        ];
    }
}
