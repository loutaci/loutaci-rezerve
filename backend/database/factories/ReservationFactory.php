<?php

namespace Database\Factories;

use App\Models\Reservation;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReservationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reservation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'data' => $this->faker->word,
        'date_end' => $this->faker->word,
        'date_start' => $this->faker->word,
        'time_end' => $this->faker->word,
        'time_start' => $this->faker->word,
        'customersID' => $this->faker->randomDigitNotNull,
        'eventsID' => $this->faker->randomDigitNotNull,
        'windowsID' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
