<?php

namespace Database\Factories;

use App\Models\PortalUsers;
use Illuminate\Database\Eloquent\Factories\Factory;

class PortalUsersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PortalUsers::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'portalsID' => $this->faker->randomDigitNotNull,
        'usersID' => $this->faker->randomDigitNotNull,
        'roleID' => $this->faker->word
        ];
    }
}
