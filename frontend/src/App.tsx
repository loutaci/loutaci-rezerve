import React from "react";
import logo from "./logo.svg";
import "./App.css";

import { BrowserRouter as Router } from "react-router-dom";
import Routing from "./Routing";
import AppUpdateService from "./AppUpdateService";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

function App() {
  return (
    <>
      <QueryClientProvider client={queryClient}>
        <Router>
          <Routing />
        </Router>
      </QueryClientProvider>
      <AppUpdateService />
    </>
  );
}

export default App;
