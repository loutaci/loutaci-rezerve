import React, { useEffect, useState } from 'react'

export default function AppUpdateService() {
  const [reloadShow, setReloadShow] = useState(false)

  useEffect(() => {
    const bc = typeof BroadcastChannel !== 'undefined' ? new BroadcastChannel('internal_notification') : "";
    if (bc) {
      bc.onmessage = () => setReloadShow(true);
      return () => {
        bc.close();
      }
    }
  }, [])

  return (
    reloadShow ?
      <div style={{ position: 'fixed', bottom: 0, left: 0, width: "100%", padding: 19, background: "#222", color: "white", zIndex: 10, textAlign: 'center' }}>
        Nová verze připravena: <a href="/" style={{ color: "white", fontWeight: "bolder" }}>Načíst</a>
      </div> : null
  )
}