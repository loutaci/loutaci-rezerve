import React from 'react';
import { Routes, Route } from "react-router-dom";
import  {  BrowserView ,  MobileView ,  isBrowser ,  isMobile  }  from  'react-device-detect' ;

import Home from './components/Home';
import Login from './components/Login';
import Registration from './components/Registration';

import RezerveRouting from './components/Routing';

import { useUser } from './components/calls';
import CancelReservation from './pages/reservations/CancelReservation';

export default function Routing(){



  return(
    <Routes>
      <Route path="registration" element={<Registration />} />
      <Route path="reservation/:reservation_token/*" element={<Reservation />} />
      <Route path="app/registration" element={<Registration />} />
      <Route path="app/reservation/:reservation_token/*" element={<Reservation />} />
      <Route path="*" element={<SecuredRoutes />} />
    </Routes>
  );
}

function Reservation() {
  return <Routes>
    <Route path="cancel" element={<CancelReservation />} />
    <Route path="edit" element={<h1>Edit Reservation</h1>} />
  </Routes>
}

function SecuredRoutes(){
  const {user,isLoading, refresh} = useUser();

  if(isLoading) return <div className="">Loading...</div>

  if(!user) return <Login refresh={refresh} />

  return(
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="rezervace/*" element={<RezerveRouting />} />
      <Route path="app/*" element={<Routes>
        <Route path="/" element={<Home />} />
        <Route path="rezervace/*" element={<RezerveRouting />} />
      </Routes>} />
      {/* <h1>Secured Routes</h1> */}
      {/* <p>You are logged in</p> */}
    </Routes>
  );
}