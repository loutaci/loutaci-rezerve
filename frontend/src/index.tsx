import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from './reportWebVitals';
//@ts-ignore
import reactToWebComponent from "react-to-webcomponent";
import Loutaciform from "./components/web-component/LoutaciForm";


// const LoutaciForm = reactToWebComponent(Loutaciform, React, ReactDOM);
// customElements.define("loutacky-formular", LoutaciForm);

// root.render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>
// );
console.log("Vitek ma vzdy pravdu", document.querySelector("loutacky-formular"))

window.onload = () => {
  if (document.querySelector("loutacky-formular")) {
    const LoutaciForm = reactToWebComponent(Loutaciform, React, ReactDOM);
    customElements.define("loutacky-formular", LoutaciForm);
  } else {
    const root = ReactDOM.createRoot(
      document.getElementById('root') as HTMLElement
    );
    root.render(
      <React.StrictMode>
        <App />
      </React.StrictMode>
    );
  }
};

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
