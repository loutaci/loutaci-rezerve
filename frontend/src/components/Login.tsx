import { Button, TextField } from "@mui/material";
import React, { useState } from "react";
import style from "./style.module.css";
import { laravelCall } from "./calls";
import { useNavigate } from "react-router-dom";

export default function Login({ refresh }: { refresh: () => void }) {
  const [status, setStatus] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  function sendLogin() {
    const options = {
      method: "POST",
      headers: {
        // Authorization: 'Bearer epRzZU1ydbE0lGQzlEyXg8cUIce9aswQzsD9RPpp',
        "content-type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
        device_name: "web" + Math.random(),
      }),
    };

    fetch(`${laravelCall.url}/sanctum/token`, options)
      .then((v) => v.json())
      .then(({ token, message }: any) => {
        if (token) {
          laravelCall.setToken(token);
          setTimeout(() => {
            navigate("/");
            refresh();
          }, 2000);
        }
        if (message) {
          setStatus(message);
        }
        // console.log(token);
      })
      .catch((v) => {
        console.warn(v);
      });
    // Bearer UrngUlLePFZuajJ80kXx0xlTBu2inBGpqW5GQvZa
  }

  return (
    <div className={style.body}>
      <div className={style.login}>
        <TextField
          variant="outlined"
          required
          className={style.Block}
          label={"Email"}
          autoComplete="email"
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          variant="outlined"
          required
          className={style.Block}
          label={"Heslo"}
          autoComplete="password"
          type="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <br></br>
        <Button variant="contained" className={style.Button} onClick={sendLogin}>
          Přihlásit se
        </Button>
        <p style={{ color: "white" }}>{status}</p>
        <span
          onClick={() => {
            window.location.pathname = "registration";
          }}
          style={{ color: "white", cursor: "pointer" }}
        >
          Registrovat
        </span>
      </div>
      <p style={{ textAlign: "center", color: "white" }}>
        Vytvořil tým {"@DingDingDong"} 2022 <br />
        <a href="https://gitlab.com/loutaci/loutaci-rezerve" style={{ color: "white" }}>
          GitLab
        </a>
        <br />
      </p>
    </div>
  );
}
