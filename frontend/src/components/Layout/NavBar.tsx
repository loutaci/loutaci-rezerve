import React, { useState } from "react";
import style from "./style.module.css";
import { Link, useNavigate } from "react-router-dom";
import { Avatar, Menu, MenuItem } from "@mui/material";
import Button from "@mui/material/Button";
import { useUser } from "../calls";

function logout() {
  localStorage.removeItem("token");
  window.location.reload();
}

export default function NavBar() {
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const { user, logout } = useUser();
  console.log("user", user);

  return (
    <div className={style.NavBar}>
      <div className={style.Logo}> <span>Rezervační systém</span> </div>
      <div></div>
      {user ? (
        <div
          onClick={(event: React.MouseEvent<HTMLElement>) => {
            setAnchorEl(event.currentTarget);
          }}
          className={style.NavBar_user}
        >
          <Avatar src={user?.image} />
          <div style={{ marginLeft: 10, textAlign: "center", cursor: "pointer" }}>
            <h2 style={{ margin: 0 }}>
              {user?.first_name} {user?.last_name}
            </h2>
            <p style={{ margin: 0 }}>{user?.email}</p>
          </div>
        </div>
      ) : (
        <Button
          onClick={() => {
            window.location.pathname = "/login";
          }}
          variant="contained"
        >
          Login
        </Button>
      )}

      <Menu
        id="demo-positioned-menu"
        aria-labelledby="demo-positioned-button"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={() => {
          setAnchorEl(null);
        }}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
      >
        <MenuItem
          onClick={() => {
            window.location.pathname = "/rezervace/profile/edit";
          }}
        >
          Profile
        </MenuItem>
        <MenuItem onClick={logout}>Logout</MenuItem>
      </Menu>
    </div>
  );
}
