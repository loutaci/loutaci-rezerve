import React, { useState } from "react";
import style from "./style.module.css";
import { NavLink } from "react-router-dom";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";
import HomeIcon from "@mui/icons-material/Home";
import LoginIcon from "@mui/icons-material/Login";

import DirectionsCarIcon from "@mui/icons-material/DirectionsCar";
import TodayIcon from "@mui/icons-material/Today";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import PeopleIcon from "@mui/icons-material/People";
import AppRegistrationIcon from "@mui/icons-material/AppRegistration";
import SettingsIcon from "@mui/icons-material/Settings";
import LogoutIcon from "@mui/icons-material/Logout";
import Button from "@mui/material/Button";

function activeLink({ isActive }: { isActive: boolean }) {
  return style.sidebarLink + " " + (isActive ? style.isActive : "");
}

export default function SideBar() {
  return (
    <div className={style.SideBar}>
      <NavLink to="/" className={activeLink}>
        <Button startIcon={<LogoutIcon />}>Seznam portálů</Button>
      </NavLink>
      <NavLink to="dashboard" className={activeLink}>
        <Button startIcon={<HomeIcon />}>Dashboard</Button>
      </NavLink>
      <NavLink to="reservation" className={activeLink}>
        <Button startIcon={<CalendarMonthIcon />}>Rezervace</Button>
      </NavLink>
      <NavLink to="action" className={activeLink}>
        <Button startIcon={<PeopleIcon />}>Akce</Button>
      </NavLink>
      <NavLink to="management" className={activeLink}>
        <Button startIcon={<AppRegistrationIcon />}>Managment</Button>
      </NavLink>
      {true ? (
        <div>
          <NavLink to="management/form" className={activeLink}>
            <Button>Formuláře</Button>
          </NavLink>
          <NavLink to="management/calendar" className={activeLink}>
            <Button>Kalendáře</Button>
          </NavLink>
          <NavLink to="management/event" className={activeLink}>
            <Button>Události</Button>
          </NavLink>
          <NavLink to="management/window" className={activeLink}>
            <Button>Okna</Button>
          </NavLink>
        </div>
      ) : (
        ""
      )}
      <NavLink to="settings" className={activeLink}>
        <Button startIcon={<SettingsIcon />}>Nastavení</Button>
      </NavLink>
    </div>
  );
}
