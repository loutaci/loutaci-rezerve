import React from "react";
import style from "./style.module.css";
import { Link, useNavigate } from "react-router-dom";
import { Avatar, Menu, MenuItem } from "@mui/material";
import Button from "@mui/material/Button";
import { useUser } from "../calls";

function logout() {
  localStorage.removeItem("token");
  window.location.reload();
}

export default function NavBar() {
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const { user, logout } = useUser();
  console.log("user", user);

  return (
    <div className={style.NavBar}>
      {user ? (
        <>
          {/*<pre style={{height:50, overflowY:'auto', width:'80vw'}}>
      {JSON.stringify(user,null,2)}
      </pre>*/}
          <div
            style={{
              padding: 3,
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              marginLeft: 7,
            }}
          >
            <Link to="/rezervace/profile/edit">
              <Avatar src={user?.image} />
            </Link>
            <div style={{ marginLeft: 10 }}>
              <h2 style={{ margin: 0 }}>
                {user?.first_name} {user?.last_name}
              </h2>
              <p style={{ margin: 0 }}>{user?.email}</p>
            </div>
          </div>
        </>
      ) : (
        <Button
          onClick={() => {
            window.location.pathname = "/login";
          }}
          variant="contained"
        >
          Login
        </Button>
      )}
      <Button onClick={logout} variant="contained">
        Logout
      </Button>
    </div>
  );
}
