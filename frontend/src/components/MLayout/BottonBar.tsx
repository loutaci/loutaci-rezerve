import React from "react";
import style from "./style.module.css";

import { NavLink } from "react-router-dom";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";
import HomeIcon from "@mui/icons-material/Home";
import LoginIcon from "@mui/icons-material/Login";
import SettingsIcon from "@mui/icons-material/Settings";

import DirectionsCarIcon from "@mui/icons-material/DirectionsCar";
import TodayIcon from "@mui/icons-material/Today";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import PeopleIcon from "@mui/icons-material/People";
import AppRegistrationIcon from "@mui/icons-material/AppRegistration";

function activeLink({ isActive }: { isActive: boolean }) {
  return style.sidebarLink + " " + (isActive ? style.isActive : "");
}

export default function BottonBar() {
  return (
    <div className={style.BottonBar}>
      <NavLink to="reservation" className={activeLink}>
        <Tooltip title="Rezervace" placement="top">
          <IconButton color="inherit">
            <CalendarMonthIcon />
          </IconButton>
        </Tooltip>
      </NavLink>
      {/* <NavLink to="action" className={activeLink}>
        <Tooltip title="Akce" placement="top">
          <IconButton color="inherit">
            <PeopleIcon />
          </IconButton>
        </Tooltip>
      </NavLink> */}
      {/* <NavLink to="dashboard" className={activeLink}>
        <Tooltip title="Domů" placement="top">
          <IconButton color="inherit">
            <HomeIcon />
          </IconButton>
        </Tooltip>
      </NavLink> */}
      <NavLink to="management" className={activeLink}>
        <Tooltip title="Přidat" placement="top">
          <IconButton color="inherit">
            <AppRegistrationIcon />
          </IconButton>
        </Tooltip>
      </NavLink>
      <NavLink to="settings" className={activeLink}>
        <Tooltip title="Nasatvení" placement="top">
          <IconButton color="inherit">
            <SettingsIcon />
          </IconButton>
        </Tooltip>
      </NavLink>
    </div>
  );
}
