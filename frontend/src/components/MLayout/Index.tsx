import React from 'react';
import BottonBar from './BottonBar';
import NavBar from './NavBar';
import style from './style.module.css';


export default function index({ children }: React.PropsWithChildren<{}>) {
  return (
    <div className={style.Layout}>
      <NavBar />
      <div className={style.Body}>{children}</div>
      <BottonBar />
    </div>
  );
}
