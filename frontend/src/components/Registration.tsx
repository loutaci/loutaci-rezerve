import React, { useState } from 'react';
import style from './style.module.css';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { useNavigate } from "react-router-dom";

import {laravelCall} from './calls';

const sx = {
  mt: "10px",
  borderRadius: 0,
  width: "100%"
}

export default function Registration(){
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    phone: ""
  });
  const navigate = useNavigate();
  const [status, setStatus] = useState("");


  async function save(){
    const userData = await laravelCall.add("register",formData)();
    console.log(userData);
    setStatus("Uživatel úspěšně vytvořen.");

    setTimeout(() => {
      navigate("/login");
    },500)
  }
 
  const setForm = (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setFormData({ ...formData, [name]: event.target.value });
  };

  return(
    <div className={style.body}>
      <div className={style.login}>
        <TextField
          variant="outlined"
          required
          className={style.Block}
          label={"Jméno"}
          value={formData.first_name}
          onChange={setForm("first_name")}
        />
        <TextField
          variant="outlined"
          required
          className={style.Block}
          label={"Příjmení"}
          value={formData.last_name}
          onChange={setForm("last_name")}
        />
        <TextField
          variant="outlined"
          required
          className={style.Block}
          label={"Email"}
          autoComplete="email"
          type="email"
          value={formData.email}
          onChange={setForm("email")}
        />
        <TextField
          variant="outlined"
          className={style.Block}
          label={"Telefon"}
          autoComplete="phone"
          type="phone"
          value={formData.phone}
          onChange={setForm("phone")}
        />
        <TextField
          variant="outlined"
          required
          className={style.Block}
          label={"Heslo"}
          autoComplete="password"
          type="password"
          value={formData.password}
          onChange={setForm("password")}
        />
        <br></br>
        <Button
          variant="contained"
          className={style.Button}
          onClick={save}
        >
          Registrovat
        </Button>
      </div>
    </div>
  )
}