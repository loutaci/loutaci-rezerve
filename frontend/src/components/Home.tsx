import React, { useState } from "react";
import style from "./style.module.css";
import styleM from "./portal/management/components/style.module.css";

import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import { NavLink } from "react-router-dom";

import { useUser, laravelCall } from "./calls";

const sx = {
  mt: "10px",
  borderRadius: 0,
  width: "100%",
};

export default function Home() {
  const [open, setOpen] = useState(false);
  const { user, refresh } = useUser();

  console.log(user?.portals);

  return (
    <div className={style.home}>
      <div className={style.home_header}>
        <span>Portály</span>
      </div>

      <div>
        {user ? (
          <>
            {user.portals.map(({ name, portalsID }) => {
              return (
                <div
                  key={portalsID}
                  className={style.portals}
                  onClick={() => {
                    laravelCall.setPortalID(portalsID);
                    window.location.pathname = window.location.pathname + "rezervace/management";
                  }}
                >
                  {name}
                </div>
              );
            })}
          </>
        ) : (
          ""
        )}
      </div>

      <Button
        sx={sx}
        onClick={() => {
          setOpen(true);
        }}
        variant="outlined"
      >
        Vytvořit portál
      </Button>

      {open ? <AddPortal open={open} setOpen={setOpen} refresh={refresh} /> : ""}
    </div>
  );
}

function AddPortal({
  open,
  setOpen,
  refresh,
}: {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  refresh: () => void;
}) {
  const [formData, setFormData] = useState({
    name: "",
  });

  const setForm =
    (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setFormData({ ...formData, [name]: event.target.value });
    };

  const buttonStyle = {
    borderRadius: 0,
    width: "100%",
  };
  const sx = {
    width: "100%",
    pb: "10px",
  };

  async function save() {
    await laravelCall.add("portals", formData)();
    setOpen(false);
    refresh();
  }

  return (
    <Modal open={open} aria-labelledby="Add portal" aria-describedby="Add portal">
      <div className={styleM.modal}>
        <div className={styleM.header}>
          Nový portál
          <IconButton
            onClick={() => {
              setOpen(false);
            }}
            color="primary"
            component="span"
          >
            <CloseIcon />
          </IconButton>
        </div>
        <div className={styleM.body}>
          <TextField
            id="outlined-error-helper-text"
            size="small"
            name={"Název"}
            label={"Název"}
            sx={sx}
            value={formData.name}
            onChange={setForm("name")}
          />

          <Button sx={buttonStyle} variant="contained" onClick={save}>
            Vytvořit portál
          </Button>
        </div>
      </div>
    </Modal>
  );
}
