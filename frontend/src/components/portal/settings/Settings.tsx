import React, { useState } from "react";
import { Link, useParams } from "react-router-dom";
import Button from "@mui/material/Button";

import { laravelCall, User, useUser } from "../../calls";
import { useQuery } from "react-query";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import moment from "moment";

const buttonStyle = {
  borderRadius: 0,
  width: "100%",
};

export default function Settings() {
  const [msg, setMsg] = useState("");
  const { user } = useUser();

  console.log("id portal", laravelCall.portalID);
  async function deletePortal() {
    try {
      console.log("smazat portal", laravelCall.portalID);
      await laravelCall.delete("portals", laravelCall.portalID)();
      window.location.pathname = "/";
    } catch {
      setMsg(
        "Nepodařilo se smazat portál. Před smazáním portálu je třeba smazat veškeré jeho data (formuláře, kalendáře apod)."
      );
    }
  }

  return (
    <div>
      <span>Settings</span>
      <div>
        <Button
          variant="contained"
          sx={buttonStyle}
          onClick={deletePortal}
          disabled={user?.roleID !== 1}
        >
          Odstranit portál
        </Button>
        <small>{msg}</small>
      </div>
      <br />
      <Link to="/">
        <Button variant="contained" sx={buttonStyle}>
          Seznam portálů
        </Button>
      </Link>
      {user?.roleID === 1 && <ManageUsers />}
    </div>
  );
}

function getRoleName(roleID: number) {
  switch (roleID) {
    case 1:
      return "Administrátor";
    case 2:
      return "Editor";
  }
}

const columns: GridColDef[] = [
  {
    field: "email",
    headerName: "Email",
    width: 200,
  },
  {
    field: "first_name",
    headerName: "Jméno",
    width: 100,
  },
  {
    field: "last_name",
    headerName: "Přijmení",
    width: 100,
  },
  {
    field: "roleID",
    headerName: "Role",
    width: 130,
    renderCell: (params) => <pre>{getRoleName(params.value)}</pre>,
  },
];

function ManageUsers() {
  const { data, isLoading, refetch } = useQuery<any>(
    `portal-users`,
    laravelCall.get("portal-users")
  );
  const [selectedIds, setSelectedIds] = useState<number[]>([]);
  if (isLoading) return <div>Loading...</div>;

  function removeUsers() {
    let promises: Promise<any>[] = [];

    if (window.confirm("Chcete opravdu smazat " + selectedIds.length + " uživatelů?")) {
      selectedIds.forEach((id: any) => {
        promises.push(laravelCall.delete(`portal-users`, id)());
      });
    }

    Promise.all(promises).then(() => {
      refetch();
      setSelectedIds([]);
    });
    // laravelCall.delete("portalusers", selectedIds)().then(() => {
    //   refetch();
    // });
  }

  return (
    <>
      <h1>Správa uživatelů v portálu</h1>
      {/* <pre>{JSON.stringify(data, null, 2)}</pre> */}
      <div>
        <div style={{ height: "400px" }}>
          <DataGrid
            getRowId={(row: any) => row.portalusersID}
            rows={data}
            columns={columns}
            pageSize={50}
            rowsPerPageOptions={[10]}
            checkboxSelection={true}
            autoHeight={false}
            rowHeight={50}
            selectionModel={selectedIds}
            onSelectionModelChange={(selectionsIDs) => {
              // console.log({ selectionModel });
              setSelectedIds(selectionsIDs as any);
              // console.log({ selectionModel });
              //setSelectedIds(selectionsIDs as any);
            }}
          />
          <Button
            variant="contained"
            color="error"
            onClick={() => {
              removeUsers();
            }}
          >
            Smazat vybrané {selectedIds.length}
          </Button>

          <Button
            variant="contained"
            color="info"
            onClick={async () => {
              try {
                await laravelCall.add("portal-users", {
                  email: prompt("Zadejte email uživatele:"),
                  roleID: 2,
                  portalsID: laravelCall.portalID,
                })();
              } catch {
                alert(
                  "Nepodařilo se přidat uživatele. Zkontrolujte zda tento účet existuje, či již není v seznamu."
                );
              }
              refetch();
            }}
          >
            Přidat uživatele
          </Button>
        </div>
      </div>
    </>
  );
}
