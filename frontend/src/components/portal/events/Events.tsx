import React, { useState } from "react";
import style from "./style.module.css";
import { laravelCall } from "../../calls";
import { useQuery } from "react-query";
import moment from "moment";

import IconButton from "@mui/material/IconButton";
import PhotoCamera from "@mui/icons-material/PhotoCamera";
import EditIcon from "@mui/icons-material/Edit";
import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";

import OpenEvent from "../management/components/OpenEvent";
import { Button } from "@mui/material";

interface eventsData {
  addressesID: null;
  capacity: number;
  date_end: string;
  date_start: string;
  description: string;
  eventsID: number;
  name: string;
  portalsID: number;
  price: number;
  time_end: string;
  time_start: string;
  windowsID: number;
  customer: Object;
}
const columns: GridColDef[] = [
  // {
  //   field: "reservationsID",
  //   headerName: "ID",
  //   width: 30,
  // },
  {
    field: "paid",
    headerName: "Zaplaceno",
    width: 85,
    renderCell: (params) => <>{params.value ? "Ano" : "Ne"}</>,
  },
  {
    field: "first_name",
    headerName: "Jméno",
    width: 100,
  },
  {
    field: "last_name",
    headerName: "Přijmení",
    width: 100,
  },
  {
    field: "data",
    headerName: "Data",
    width: 260,
    renderCell: (params) => <pre>{JSON.stringify(params.value, null, 2)}</pre>,
  },
  {
    field: "price",
    headerName: "Cena",
    width: 75,
    renderCell: (params) => <pre>{params.value || "0"}</pre>,
  },
  {
    field: "created_at",
    headerName: "Vytvořena",
    width: 160,
    valueFormatter: ({ value }) => moment(value).format("HH:MM:ss d.M.Y"),
  },

  // {
  //   field: "age",
  //   headerName: "Age",
  //   type: "number",
  //   width: 90,
  // },
  // {
  //   field: "fullName",
  //   headerName: "Full name",
  //   description: "This column has a value getter and is not sortable.",
  //   sortable: false,
  //   width: 160,
  //   valueGetter: (params: GridValueGetterParams) =>
  //     `${params.row.firstName || ""} ${params.row.lastName || ""}`,
  // },
];

export default function Events() {
  const { isLoading, error, data } = useQuery<any>(`events`, laravelCall.get(`events`));
  const [edit, setEdit] = useState({
    open: false,
    id: 0,
  });

  console.log(isLoading, error);

  if (isLoading) return <div>Loading...</div>;
  console.log("events", data);

  return (
    <div className={style.body}>
      <h1>Seznamy akcí</h1>

      {data.map((event: eventsData) => {
        return (
          <React.Fragment key={event.eventsID}>
            <div className={style.block_header}>
              <span>{event.name} </span>
              <IconButton
                color="primary"
                aria-label="upload picture"
                component="span"
                onClick={() => {
                  setEdit({ open: true, id: event.eventsID });
                }}
              >
                <EditIcon />
              </IconButton>
              <span>
                {" "}
                {moment(event.date_start).format("D.M.Y")}-{event.time_start} -_-{" "}
                {moment(event.date_end).format("D.M.Y")}-{event.time_end}
              </span>
              <hr></hr>
              {event.description}
            </div>
            <Table windowID={event.windowsID} />
            {edit.open ? (
              <OpenEvent
                open={edit.open}
                id={edit.id}
                onClose={() => {
                  setEdit({ open: false, id: 0 });
                }}
              />
            ) : (
              ""
            )}
          </React.Fragment>
        );
      })}
    </div>
  );
}

function Overview({ reservationsCount, capacity, paidCount }: any) {
  return (
    <div>
      <b>Registrovano:</b> {reservationsCount} / {capacity}
      <br />
      <b>Zaplaceno:</b> {paidCount} / {reservationsCount}
    </div>
  );
}

function Table({ windowID }: { windowID: number }) {
  const [selectedIds, setSelectedIds] = useState<number[]>([]);

  const { isLoading, error, data, refetch } = useQuery<any>(
    `windows/${windowID}/events`,
    laravelCall.get(`windows/${windowID}/events`),
    { refetchInterval: 30000, retry: false }
  );

  console.log(isLoading, error);

  if (isLoading) return <div>Loading...</div>;
  if (error)
    return (
      <>
        Rezervace pro toto okno se nepodařilo načíst. <br />
        Zkontrolujte zda máte k němu přidělené okno.
      </>
    );

  console.log(data);

  function deleteReservations() {
    let promises: Promise<any>[] = [];

    if (window.confirm("Chcete opravdu smazat " + selectedIds.length + " rezervací?")) {
      selectedIds.forEach((id: any) => {
        promises.push(laravelCall.delete(`reservations`, id)());
      });
    }
    Promise.all(promises).then(() => {
      refetch();
      setSelectedIds([]);
    });
  }

  function payReservation(markPaid: boolean) {
    let promises: Promise<any>[] = [];
    selectedIds.forEach((id: any) => {
      promises.push(laravelCall.update(`reservations-paid`, id, { markPaid })());
    });
    Promise.all(promises).then(() => {
      refetch();
      setSelectedIds([]);
    });
  }

  return (
    <div className={style.block_body}>
      <div className={style.block_body_block}>
        <Overview
          reservationsCount={data.reservationsCount}
          capacity={data?.events?.capacity}
          paidCount={data.paidCount}
        />
      </div>
      <div className={style.block_body_block2}>
        <div style={{ height: "400px" }}>
          <DataGrid
            getRowId={(row: any) => row.reservationsID}
            rows={data.reservations.map((reservation: eventsData) => ({
              ...reservation.customer,
              ...reservation,
            }))}
            columns={columns}
            pageSize={50}
            rowsPerPageOptions={[10]}
            checkboxSelection={true}
            autoHeight={false}
            rowHeight={50}
            selectionModel={selectedIds}
            onSelectionModelChange={(selectionsIDs) => {
              // console.log({ selectionModel });
              setSelectedIds(selectionsIDs as any);
              // console.log({ selectionModel });
              //setSelectedIds(selectionsIDs as any);
            }}
          />
          <Button
            variant="contained"
            color="secondary"
            onClick={() => {
              payReservation(true);
            }}
          >
            Označit jako Zaplacené
          </Button>
          <Button
            variant="contained"
            color="warning"
            onClick={() => {
              payReservation(false);
            }}
          >
            Označit jako NEzaplacené
          </Button>
          <Button
            variant="contained"
            color="error"
            onClick={() => {
              deleteReservations();
            }}
          >
            Smazat vybrané {selectedIds.length}
          </Button>
        </div>
      </div>
    </div>
  );
}
