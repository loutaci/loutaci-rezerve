import React from "react";
import style from "./style.module.css";

export default function Management() {
  console.log(window.location.pathname);

  return (
    <div className={style.body}>
      <span>Prvky</span>

      <div
        onClick={() => {
          window.location.pathname = window.location.pathname + "/form";
        }}
        className={style.block}
      >
        Formuláře
      </div>

      <div
        onClick={() => {
          window.location.pathname = window.location.pathname + "/calendar";
        }}
        className={style.block}
      >
        Kalendáře
      </div>

      <span>Rezervační prostory</span>
      <div
        onClick={() => {
          window.location.pathname = window.location.pathname + "/window";
        }}
        className={style.block}
      >
        Rezervační kalendáře
      </div>
      <div
        onClick={() => {
          window.location.pathname = window.location.pathname + "/event";
        }}
        className={style.block}
      >
        Události
      </div>

      {/* <span>Seznamy</span>
      <div
        onClick={() => {
          window.location.pathname = window.location.pathname + "/list_event";
        }}
        className={style.block}
      >
        Seznamy událostí
      </div> */}
    </div>
  );
}
