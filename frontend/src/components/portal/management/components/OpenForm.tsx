import React, { useState, useEffect } from "react";
import style from "./style.module.css";

import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Editor from "@monaco-editor/react";

import DeleteIcon from "@mui/icons-material/Delete";

import { laravelCall } from "../../../calls";
import Approval from "./Approval";
interface propsAddFromInterface {
  open: boolean;
  onClose: () => void;
  id: number;
}

interface fomrDataInterface {
  name: string;
  description: string | null;
  config: string;
}

const sx = {
  width: "100%",
  pb: "10px",
};

const buttonStyle = {
  borderRadius: 0,
  width: "100%",
};

export default function OpenForm(props: propsAddFromInterface) {
  const { onClose, open, id } = props;
  const [openApproval, setOpenApproval] = useState(false);

  const [formData, setFormData] = useState<fomrDataInterface>({
    name: "",
    description: null,
    config: "[]",
  });

  async function save() {
    console.log("save");
    await laravelCall.update("forms", id, { ...formData, config: formData.config })();
    onClose();
  }

  async function remove() {
    console.log("remove");
    await laravelCall.delete("forms", id)();
    onClose();
  }

  useEffect(() => {
    if (id !== 0) {
      laravelCall
        .get(`forms/${id}`)()
        .then((data: any) => {
          console.log(data);
          setFormData({ ...data, config: JSON.stringify(data.config, null, 2) });
        })
        .catch((err) => {});
    }
  }, [id]);

  const setForm =
    (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setFormData({ ...formData, [name]: event.target.value });
    };

  return (
    <div>
      <Modal open={open} aria-labelledby="Add Form" aria-describedby="Add Form">
        <div className={style.modal}>
          <div className={style.header}>
            {formData.name}
            <IconButton onClick={onClose} color="primary" component="span">
              <CloseIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                setOpenApproval(true);
              }}
              color="primary"
              component="span"
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <div className={style.body}>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Název"}
              label={"Název"}
              sx={sx}
              value={formData.name}
              onChange={setForm("name")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Popis"}
              label={"Popis"}
              sx={sx}
              multiline
              rows={4}
              value={formData.description}
              onChange={setForm("description")}
            />
            <Editor
              height="200px"
              defaultLanguage="json"
              value={formData.config}
              // @ts-ignore
              onChange={(value) => setForm("config")({ target: { value } })}
            />

            <Button sx={buttonStyle} variant="contained" onClick={save}>
              Aktualizovat formulář
            </Button>
          </div>
          <Approval
            open={openApproval}
            onClose={() => {
              setOpenApproval(false);
            }}
            name={formData.name}
            remove={remove}
          />
        </div>
      </Modal>
    </div>
  );
}
