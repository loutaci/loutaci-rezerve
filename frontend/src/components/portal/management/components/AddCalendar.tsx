import React, {useState} from 'react';
import style from './style.module.css';

import Modal from '@mui/material/Modal';
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';

import {laravelCall} from '../../../calls';

interface propsAddFromInterface{
  open: boolean,
  onClose: () => void
}

interface fomrDataInterface{
  name?: string,        // Název kalendáře
  desc?: string,        // Popis kalendáře
  type: string,        // Vyrianta kalendáře
  rezerveMax: number,  // Maximální počet rezervací na jeden termín 

  days: boolean,
  half: boolean,

  styleDayDisabledColor: string,
  styleDayDisabledBackground: string,

  styleDaySelectColor: string,
  styleDaySelectBackground: string,

  styleDayReserveFullColor: string,
  styleDayReserveFullBackground: string,

  styleDayReservePartColor: string,
  styleDayReservePartBackground: string,

}

const sx = {
  width: "100%",
  pb: "10px"

}

const sx50 = {
  width: "50%",
  pb: "10px",
  mt: "10px"

}

const buttonStyle = {
  borderRadius: 0,
  width: "100%"
}

export default function AddCalendar(props: propsAddFromInterface){
  const { onClose, open } = props;

  const [formData, setFormData] = useState<fomrDataInterface>({
    rezerveMax: 1,
    type: "day",

    days: false,
    half: false,

    styleDayDisabledColor: "white",
    styleDayDisabledBackground: "#bdbdbd",

    styleDaySelectColor: "white",
    styleDaySelectBackground: "#4d9afe",

    styleDayReserveFullColor: "white",
    styleDayReserveFullBackground: "#ff0000",

    styleDayReservePartColor: "white",
    styleDayReservePartBackground: "#ff9742",
  });

  async function save(){
    console.log("save");
    const config = {
      days: formData.days,
      half: formData.half,
      rezerveMax: formData.rezerveMax,
      style: {
        day: {
          disabled: {
            backgroundColor: formData.styleDayDisabledBackground,
            color: formData.styleDayDisabledColor
          },
          reserve: {
            full: {
              color: formData.styleDayReserveFullColor,
              backgroundColor: formData.styleDayReserveFullBackground
            },
            part: {
              color: formData.styleDayReservePartColor,
              backgroundColor: formData.styleDayReservePartBackground
            }            
          },
          select: {
            color: formData.styleDaySelectColor,
            backgroundColor: formData.styleDaySelectBackground              
          }
        }
      }
    }
    await laravelCall.add("calendars",{
      name: formData.name,
      description: formData.desc,
      type: formData.type,
      config: JSON.stringify(config)
    })();
    onClose();
  }

  const setForm = (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement> | SelectChangeEvent<string>) => {
    setFormData({ ...formData, [name]: event.target.value });
  };
  


  return(
    <div>
      <Modal
        open={open}
        aria-labelledby="Add Calendar"
        aria-describedby="Add Calendar"
      >
        <div className={style.modal} >
          <div className={style.header} >
            Nový kalendář
            <IconButton onClick={onClose}  color="primary" component="span">
              <CloseIcon />
            </IconButton>
          </div>
          <div className={style.body}>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Název"}
              label={"Název"}
              sx={sx}
              value={formData.name}
              onChange={setForm("name")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Popis"}
              label={"Popis"}
              sx={sx}
              multiline
              rows={4}
              value={formData.desc}
              onChange={setForm("desc")}
            />

            <FormControl sx={sx} fullWidth size="small" >
              <InputLabel id="demo-simple-select-label">Typ kalendáře</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={formData.type}
                label="Typ kalendáře"
                onChange={setForm("type")}
              >
                <MenuItem value={"day"}>Dny</MenuItem>
              </Select>
            </FormControl>

            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="number"
              name={"Max rezervací na jeden den"}
              label={"Max rezervací na jeden den"}
              sx={sx}
              value={formData.rezerveMax}
              onChange={setForm("rezerveMax")}
            />

            <FormControlLabel
              control={
                <Switch
                  checked={formData.days}
                  onChange={(event)=>{
                    setFormData({ ...formData, days: event.target.checked });
                  }}
                  name="Výber více dnů"
                />
              }
              label="Výber více dnů"
            />

            <FormControlLabel
              control={
                <Switch
                  checked={formData.half}
                  onChange={(event)=>{
                    setFormData({ ...formData, half: event.target.checked });
                  }}
                  name="Půlení dnů"
                />
              }
              label="Půlení dnů"
            />


            <h3>Styly</h3>
            <hr></hr>
            <span>Zakázaný den</span> <br></br>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Pozadí"}
              label={"Pozadí"}
              sx={sx50}
              value={formData.styleDayDisabledBackground}
              onChange={setForm("styleDayDisabledBackground")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Text"}
              label={"Text"}
              sx={sx50}
              value={formData.styleDayDisabledColor}
              onChange={setForm("styleDayDisabledColor")}
            />

            <span>Zvolený den</span> <br></br>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Pozadí"}
              label={"Pozadí"}
              sx={sx50}
              value={formData.styleDaySelectBackground}
              onChange={setForm("styleDaySelectBackground")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Text"}
              label={"Text"}
              sx={sx50}
              value={formData.styleDaySelectColor}
              onChange={setForm("styleDaySelectColor")}
            />

            {formData.rezerveMax > 1 ? 
              <React.Fragment>
                <span>Částečně obsazený den</span> <br></br>
                  <TextField
                    id="outlined-error-helper-text"
                    size="small"
                    type="color"
                    name={"Pozadí"}
                    label={"Pozadí"}
                    sx={sx50}
                    value={formData.styleDayReservePartBackground}
                    onChange={setForm("styleDayReservePartBackground")}
                  />
                  <TextField
                    id="outlined-error-helper-text"
                    size="small"
                    type="color"
                    name={"Text"}
                    label={"Text"}
                    sx={sx50}
                    value={formData.styleDayReservePartColor}
                    onChange={setForm("styleDayReservePartColor")}
                  />
              </React.Fragment>
              : ""
            }
            
            <span>Obsazený den</span> <br></br>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Pozadí"}
              label={"Pozadí"}
              sx={sx50}
              value={formData.styleDayReserveFullBackground}
              onChange={setForm("styleDayReserveFullBackground")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Text"}
              label={"Text"}
              sx={sx50}
              value={formData.styleDayReserveFullColor}
              onChange={setForm("styleDayReserveFullColor")}
            />




            <Button
              sx={buttonStyle}
              variant="contained"
              onClick={save}
            >
              Vytvořit kalendář
            </Button>

          </div>
        </div>
      </Modal>
    </div>
  );
  
}