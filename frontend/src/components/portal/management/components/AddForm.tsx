import React, { useState } from "react";
import style from "./style.module.css";

import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Editor from "@monaco-editor/react";

import { laravelCall } from "../../../calls";

interface propsAddFromInterface {
  open: boolean;
  onClose: () => void;
}

interface fomrDataInterface {
  name?: string;
  desc?: string;
  config?: string;
}

const sx = {
  width: "100%",
  pb: "10px",
};

const buttonStyle = {
  borderRadius: 0,
  width: "100%",
};

export default function AddForm(props: propsAddFromInterface) {
  const { onClose, open } = props;

  const [formData, setFormData] = useState<fomrDataInterface>({
    config: JSON.stringify(
      [
        {
          key: "email",
          text: { name: "email", readable: true, helperText: "vložte email" },
          type: "text",
          style: { width: 50 },
        },
      ],
      null,
      2
    ),
  });

  async function save() {
    console.log("save");
    await laravelCall.add("forms", {
      name: formData.name,
      description: formData.desc,
      config: formData.config,
    })();
    onClose();
  }

  const setForm =
    (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setFormData({ ...formData, [name]: event.target.value });
    };

  return (
    <div>
      <Modal open={open} aria-labelledby="Add Form" aria-describedby="Add Form">
        <div className={style.modal}>
          <div className={style.header}>
            Nový formulář
            <IconButton onClick={onClose} color="primary" component="span">
              <CloseIcon />
            </IconButton>
          </div>
          <div className={style.body}>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Název"}
              label={"Název"}
              sx={sx}
              value={formData.name}
              onChange={setForm("name")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Popis"}
              label={"Popis"}
              sx={sx}
              multiline
              rows={4}
              value={formData.desc}
              onChange={setForm("desc")}
            />
            <Editor
              height="200px"
              defaultLanguage="json"
              value={formData.config}
              defaultValue={formData.config}
              // @ts-ignore
              onChange={(value) => setForm("config")({ target: { value } })}
            />

            <Button sx={buttonStyle} variant="contained" onClick={save}>
              Vytvořit formulář
            </Button>
          </div>
        </div>
      </Modal>
    </div>
  );
}
