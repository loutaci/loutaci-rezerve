import React, {useState,useEffect} from 'react';
import style from './style.module.css';

import Modal from '@mui/material/Modal';
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import moment from 'moment/moment';

import DeleteIcon from '@mui/icons-material/Delete';

import {laravelCall} from '../../../calls';
import Approval from './Approval';

interface propsFromInterface{
  open: boolean,
  onClose: () => void,
  id: number
}

interface fomrDataInterface{
  name: string|null,
  description: string|null,
  price: number|null
  time_end: string,
  time_start: string,
  date_end: string,
  date_start: string,
  capacity: number|null,
  addresesID: number,
  windowsID: number
}

const sx = {
  width: "100%",
  pb: "10px"
}

const sx50 = {
  width: "50%",
  pb: "10px",
  mt: "10px"

}

const buttonStyle = {
  borderRadius: 0,
  width: "100%"
}

export default function OpenEvent(props: propsFromInterface){
  const { onClose, open, id } = props;
  const [openApproval, setOpenApproval] = useState(false);

  const [formData, setFormData] = useState<fomrDataInterface>({
    name: null,
    description: null,
    price: null,
    time_end: moment().format('HH:mm'),
    time_start: moment().format('HH:mm'),
    date_end: moment().format('YYYY-MM-DD'),
    date_start: moment().format('YYYY-MM-DD'),
    capacity: null,
    addresesID: 1,
    windowsID: 1
  });

  async function save(){
    console.log("save");
    await laravelCall.update("events",id,formData)();
    onClose();
  }

  async function remove(){
    console.log("remove");
    await laravelCall.delete("events",id)();
    onClose();
  }

  useEffect(() => {
    if(id !== 0){
      laravelCall
      .get(`events/${id}`)()
      .then((data:any) => {
        console.log(data);
				setFormData(data);
			})
			.catch((err)=>{
			
			})
    }
  },[id]);

  const setForm = (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setFormData({ ...formData, [name]: event.target.value });
  };
  

  console.log(formData)


  return(
    <div>
      <Modal
        open={open}
        aria-labelledby="Open Event"
        aria-describedby="Open Event"
      >
        <div className={style.modal} >
          <div className={style.header} >
            {formData.name}
            <IconButton onClick={onClose}  color="primary" component="span">
              <CloseIcon />
            </IconButton>
            <IconButton onClick={()=>{setOpenApproval(true)}}  color="primary" component="span">
              <DeleteIcon />
            </IconButton>
          </div>
          <div className={style.body}>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Název"}
              label={"Název"}
              sx={sx}
              value={formData.name}
              onChange={setForm("name")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Popis"}
              label={"Popis"}
              sx={sx}
              multiline
              rows={4}
              value={formData.description}
              onChange={setForm("description")}
            />

            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="date"
              label={"Od"}
              sx={sx50}
              value={moment(formData.date_start).format('YYYY-MM-DD')}
              onChange={setForm("date_start")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="time"
              label={"Od"}
              sx={sx50}
              value={formData.time_start}
              onChange={setForm("time_start")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="date"
              label={"Do"}
              sx={sx50}
              value={moment(formData.date_end).format('YYYY-MM-DD')}
              onChange={setForm("date_end")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="time"
              label={"Do"}
              sx={sx50}
              value={formData.time_end}
              onChange={setForm("time_end")}
            />

            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="number"
              label={"Maximální počet míst"}
              sx={sx50}
              value={formData.capacity}
              onChange={setForm("capacity")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="number"
              label={"Cena za místo"}
              sx={sx50}
              value={formData.price}
              onChange={setForm("price")}
            />

            <Button
              sx={buttonStyle}
              variant="contained"
              onClick={save}
            >
              Uložit událost
            </Button>

          </div>
          <Approval
            open={openApproval}
            onClose={()=>{setOpenApproval(false)}}
            name={formData.name}
            remove={remove}
          />
        </div>
      </Modal>
    </div>
  );
}