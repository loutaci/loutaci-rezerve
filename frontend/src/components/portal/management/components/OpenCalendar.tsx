import React, { useState, useEffect } from "react";
import style from "./style.module.css";

import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";

import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";

import { laravelCall } from "../../../calls";
import Approval from "./Approval";
interface propsOpenFromInterface {
  open: boolean;
  onClose: () => void;
  id: number;
}

interface fomrDataInterface {
  name: string | null; // Název kalendáře
  desc: string | null; // Popis kalendáře
  type: string; // Vyrianta kalendáře
  rezerveMax: number; // Maximální počet rezervací na jeden termín

  days: boolean;
  half: boolean;

  styleDayDisabledColor: string;
  styleDayDisabledBackground: string;

  styleDaySelectColor: string;
  styleDaySelectBackground: string;

  styleDayReserveFullColor: string;
  styleDayReserveFullBackground: string;

  styleDayReservePartColor: string;
  styleDayReservePartBackground: string;
}

const sx = {
  width: "100%",
  pb: "10px",
};

const sx50 = {
  width: "50%",
  pb: "10px",
  mt: "10px",
};

const buttonStyle = {
  borderRadius: 0,
  width: "100%",
};

export default function OpenCalendar(props: propsOpenFromInterface) {
  const { onClose, open, id } = props;
  const [openApproval, setOpenApproval] = useState(false);

  const [formData, setFormData] = useState<fomrDataInterface>({
    name: "",
    desc: "",
    rezerveMax: 1,
    type: "day",

    days: false,
    half: false,

    styleDayDisabledColor: "white",
    styleDayDisabledBackground: "rgb(220, 220, 220)",

    styleDaySelectColor: "white",
    styleDaySelectBackground: "rgb(220, 220, 220)",

    styleDayReserveFullColor: "white",
    styleDayReserveFullBackground: "red",

    styleDayReservePartColor: "white",
    styleDayReservePartBackground: "orange",
  });

  async function save() {
    console.log("save");
    const config = {
      days: formData.days,
      half: formData.half,
      rezerveMax: Number(formData.rezerveMax),
      style: {
        day: {
          disabled: {
            backgroundColor: formData.styleDayDisabledBackground,
            color: formData.styleDayDisabledColor,
          },
          reserve: {
            full: {
              color: formData.styleDayReserveFullColor,
              backgroundColor: formData.styleDayReserveFullBackground,
            },
            part: {
              color: formData.styleDayReservePartColor,
              backgroundColor: formData.styleDayReservePartBackground,
            },
          },
          select: {
            color: formData.styleDaySelectColor,
            backgroundColor: formData.styleDaySelectBackground,
          },
        },
      },
    };
    await laravelCall.update("calendars", id, {
      name: formData.name,
      description: formData.desc,
      type: formData.type,
      config: JSON.stringify(config),
    })();
    onClose();
  }

  async function remove() {
    console.log("remove");
    await laravelCall.delete("calendars", id)();
    onClose();
  }

  useEffect(() => {
    if (id !== 0) {
      laravelCall
        .get(`calendars/${id}`)()
        .then((data: any) => {
          console.log(data);

          setFormData({
            name: data.name, // Název kalendáře
            desc: data.description, // Popis kalendáře
            type: data.type,

            days: data.config.days,
            half: data.config.half,
            rezerveMax: data.config.rezerveMax,

            styleDayDisabledColor: data.config.style.day.disabled.color,
            styleDayDisabledBackground: data.config.style.day.disabled.backgroundColor,

            styleDaySelectColor: data.config.style.day.select.color,
            styleDaySelectBackground: data.config.style.day.select.backgroundColor,

            styleDayReserveFullColor: data.config.style.day.reserve.full.color,
            styleDayReserveFullBackground: data.config.style.day.reserve.full.backgroundColor,

            styleDayReservePartColor: data.config.style.day.reserve.part.color,
            styleDayReservePartBackground: data.config.style.day.reserve.part.backgroundColor,
          });
        })
        .catch((err) => {});
    }
  }, [id]);

  const setForm =
    (name: string) =>
    (
      event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement> | SelectChangeEvent<string>
    ) => {
      setFormData({ ...formData, [name]: event.target.value });
    };

  return (
    <div>
      <Modal open={open} aria-labelledby="Add Calendar" aria-describedby="Add Calendar">
        <div className={style.modal}>
          <div className={style.header}>
            {formData.name}
            <IconButton onClick={onClose} color="primary" component="span">
              <CloseIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                setOpenApproval(true);
              }}
              color="primary"
              component="span"
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <div className={style.body}>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Název"}
              label={"Název"}
              sx={sx}
              value={formData.name}
              onChange={setForm("name")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Popis"}
              label={"Popis"}
              sx={sx}
              multiline
              rows={4}
              value={formData.desc}
              onChange={setForm("desc")}
            />
            <FormControl sx={sx} fullWidth size="small">
              <InputLabel id="demo-simple-select-label">Typ kalendáře</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={formData.type}
                label="Typ kalendáře"
                onChange={setForm("type")}
              >
                <MenuItem value={"day"}>Dny</MenuItem>
              </Select>
            </FormControl>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="number"
              name={"Max rezervací na jeden den"}
              label={"Max rezervací na jeden den"}
              sx={sx}
              value={formData.rezerveMax}
              onChange={setForm("rezerveMax")}
            />
            <FormControlLabel
              control={
                <Switch
                  checked={formData.days}
                  onChange={(event) => {
                    setFormData({ ...formData, days: event.target.checked });
                  }}
                  name="Výber více dnů"
                />
              }
              label="Výber více dnů"
            />
            <FormControlLabel
              control={
                <Switch
                  checked={formData.half}
                  onChange={(event) => {
                    setFormData({ ...formData, half: event.target.checked });
                  }}
                  name="Půlení dnů"
                />
              }
              label="Půlení dnů"
            />
            <h3>Styly</h3>
            <hr></hr>
            <span>Zakázaný den</span> <br></br>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Pozadí"}
              label={"Pozadí"}
              sx={sx50}
              value={formData.styleDayDisabledBackground}
              onChange={setForm("styleDayDisabledBackground")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Text"}
              label={"Text"}
              sx={sx50}
              value={formData.styleDayDisabledColor}
              onChange={setForm("styleDayDisabledColor")}
            />
            <span>Zvolený den</span> <br></br>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Pozadí"}
              label={"Pozadí"}
              sx={sx50}
              value={formData.styleDaySelectBackground}
              onChange={setForm("styleDaySelectBackground")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Text"}
              label={"Text"}
              sx={sx50}
              value={formData.styleDaySelectColor}
              onChange={setForm("styleDaySelectColor")}
            />
            {formData.rezerveMax > 1 ? (
              <React.Fragment>
                <span>Částečně obsazený den</span> <br></br>
                <TextField
                  id="outlined-error-helper-text"
                  size="small"
                  type="color"
                  name={"Pozadí"}
                  label={"Pozadí"}
                  sx={sx50}
                  value={formData.styleDayReservePartBackground}
                  onChange={setForm("styleDayReservePartBackground")}
                />
                <TextField
                  id="outlined-error-helper-text"
                  size="small"
                  type="color"
                  name={"Text"}
                  label={"Text"}
                  sx={sx50}
                  value={formData.styleDayReservePartColor}
                  onChange={setForm("styleDayReservePartColor")}
                />
              </React.Fragment>
            ) : (
              ""
            )}
            <span>Obsazený den</span> <br></br>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Pozadí"}
              label={"Pozadí"}
              sx={sx50}
              value={formData.styleDayReserveFullBackground}
              onChange={setForm("styleDayReserveFullBackground")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="color"
              name={"Text"}
              label={"Text"}
              sx={sx50}
              value={formData.styleDayReserveFullColor}
              onChange={setForm("styleDayReserveFullColor")}
            />
            <Button sx={buttonStyle} variant="contained" onClick={save}>
              Aktualizovat kalendář
            </Button>
          </div>
          <Approval
            open={openApproval}
            onClose={() => {
              setOpenApproval(false);
            }}
            name={formData.name}
            remove={remove}
          />
        </div>
      </Modal>
    </div>
  );
}
