import React, {useState} from 'react';
import style from './style.module.css';

import Modal from '@mui/material/Modal';
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import moment from 'moment/moment';

import {laravelCall} from '../../../calls';

interface propsFromInterface{
  open: boolean,
  onClose: () => void
}

interface fomrDataInterface{
  name: string|null,
  description: string|null,
  price: number|null
  time_end: string,
  time_start: string,
  date_end: string,
  date_start: string,
  capacity: number|null,
  addresesID: number,
  windowsID: number
}

const sx = {
  width: "100%",
  pb: "10px"
}

const sx50 = {
  width: "50%",
  pb: "10px",
  mt: "10px"

}

const buttonStyle = {
  borderRadius: 0,
  width: "100%"
}

export default function AddEvent(props: propsFromInterface){
  const { onClose, open } = props;

  const [formData, setFormData] = useState<fomrDataInterface>({
    name: null,
    description: null,
    price: null,
    time_end: moment().format('HH:mm'),
    time_start: moment().format('HH:mm'),
    date_end: moment().format('YYYY-MM-DD'),
    date_start: moment().format('YYYY-MM-DD'),
    capacity: null,
    addresesID: 1,
    windowsID: 1
  });

  async function save(){
    console.log("save");
    await laravelCall.add("events",formData)();
    onClose();
  }

  const setForm = (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setFormData({ ...formData, [name]: event.target.value });
  };
  

  console.log(formData)


  return(
    <div>
      <Modal
        open={open}
        aria-labelledby="Add Form"
        aria-describedby="Add Form"
      >
        <div className={style.modal} >
          <div className={style.header} >
            Nová událost
            <IconButton onClick={onClose}  color="primary" component="span">
              <CloseIcon />
            </IconButton>
          </div>
          <div className={style.body}>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Název"}
              label={"Název"}
              sx={sx}
              value={formData.name}
              onChange={setForm("name")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Popis"}
              label={"Popis"}
              sx={sx}
              multiline
              rows={4}
              value={formData.description}
              onChange={setForm("description")}
            />

            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="date"
              label={"Od"}
              sx={sx50}
              value={formData.date_start}
              onChange={setForm("date_start")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="time"
              label={"Od"}
              sx={sx50}
              value={formData.time_start}
              onChange={setForm("time_start")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="date"
              label={"Do"}
              sx={sx50}
              value={formData.date_end}
              onChange={setForm("date_end")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="time"
              label={"Do"}
              sx={sx50}
              value={formData.time_end}
              onChange={setForm("time_end")}
            />

            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="number"
              label={"Maximální počet míst"}
              sx={sx50}
              value={formData.capacity}
              onChange={setForm("capacity")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              type="number"
              label={"Cena za místo"}
              sx={sx50}
              value={formData.price}
              onChange={setForm("price")}
            />

            <Button
              sx={buttonStyle}
              variant="contained"
              onClick={save}
            >
              Vytvořit událost
            </Button>

          </div>
        </div>
      </Modal>
    </div>
  );
  
}