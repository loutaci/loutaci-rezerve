import React, { useEffect, useState } from "react";
import style from "./style.module.css";

import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";

import { laravelCall } from "../../../calls";
import { formDataInterface, SelectEvent } from "./OpenWindow";

interface propsAddFormInterface {
  open: boolean;
  onClose: () => void;
}

// interface fomrDataInterface{
//   calendarsID: number | null,
//   formsID: number,
//   description: string | null,
//   name: string,
// }

const sx = {
  width: "100%",
  pb: "10px",
};

const buttonStyle = {
  borderRadius: 0,
  width: "100%",
};

export default function AddWindow(props: propsAddFormInterface) {
  const { onClose, open } = props;
  const [windowType, setWindowType] = useState("calendar");

  const [formData, setFormData] = useState<formDataInterface>({
    calendarsID: null,
    formsID: 0,
    description: null,
    name: "",
    eventsID: 0,
    windowsID: 0,
  });

  async function save() {
    console.log("save windows");
    await laravelCall.add("windows", formData)();
    onClose();
  }

  const setForm =
    (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setFormData({ ...formData, [name]: event.target.value });
    };

  return (
    <div>
      <Modal open={open} aria-labelledby="Add Window" aria-describedby="Add Window">
        <div className={style.modal}>
          <div className={style.header}>
            Nový window
            <IconButton onClick={onClose} color="primary" component="span">
              <CloseIcon />
            </IconButton>
          </div>
          <div className={style.body}>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Název"}
              label={"Název"}
              sx={sx}
              value={formData.name}
              onChange={setForm("name")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Popis"}
              label={"Popis"}
              sx={sx}
              multiline
              rows={4}
              value={formData.description}
              onChange={setForm("description")}
            />

            <SelectForm
              value={formData.formsID}
              onChange={(event) => {
                setFormData({ ...formData, formsID: Number(event.target.value) });
              }}
            />
            <FormControl sx={sx} size="small" fullWidth>
              <InputLabel id="demo-simple-select-label">Typ okna</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={windowType}
                label="Akce"
                onChange={(e) => {
                  setWindowType(e.target.value);
                  setFormData({ ...formData, calendarsID: null, eventsID: null });
                }}
              >
                {[
                  { name: "Kalendář", value: "calendar" },
                  { name: "Akce", value: "event" },
                ].map(({ name, value }) => {
                  return <MenuItem value={value}>{name}</MenuItem>;
                })}
              </Select>
            </FormControl>
            {windowType === "calendar" ? (
              <SelectCalendar
                value={formData.calendarsID}
                onChange={(event) => {
                  setFormData({ ...formData, calendarsID: Number(event.target.value) });
                }}
              />
            ) : (
              <SelectEvent
                value={formData.eventsID}
                onChange={(event) => {
                  setFormData({ ...formData, eventsID: Number(event.target.value) });
                }}
              />
            )}

            <Button sx={buttonStyle} variant="contained" onClick={save}>
              Vytvořit window
            </Button>
          </div>
        </div>
      </Modal>
    </div>
  );
}

interface selectFormDataInterface {
  name: string;
  description: string | null;
  formsID: number;
  config: [];
}

function SelectForm({
  value,
  onChange,
}: {
  value: number;
  onChange: (event: SelectChangeEvent<number>) => void;
}) {
  const [data, setData] = useState<selectFormDataInterface[]>([]);

  useEffect(() => {
    laravelCall
      .get(`forms`)()
      .then((data: any) => {
        console.log(data);
        setData(data);
      })
      .catch((err) => {});
  }, []);

  return (
    <FormControl sx={sx} size="small" fullWidth>
      <InputLabel id="demo-simple-select-label">Formulář</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        label="Formulář"
        onChange={onChange}
      >
        {data.map(({ name, formsID }) => {
          return <MenuItem value={formsID}>{name}</MenuItem>;
        })}
        <MenuItem value={0}>Vyberte</MenuItem>
      </Select>
    </FormControl>
  );
}

interface selectCalendarDataInterface {
  name: string;
  config: object;
  description: string;
  type: string;
  calendarsID: number;
}

function SelectCalendar({
  value,
  onChange,
}: {
  value: number | null;
  onChange: (event: SelectChangeEvent<number>) => void;
}) {
  const [data, setData] = useState<selectCalendarDataInterface[]>([]);

  useEffect(() => {
    laravelCall
      .get(`calendars`)()
      .then((data: any) => {
        console.log(data);
        setData(data);
      })
      .catch((err) => {});
  }, []);

  return (
    <FormControl sx={sx} size="small" fullWidth>
      <InputLabel id="demo-simple-select-label">Kalendář</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value || 0}
        label="Kalendář"
        onChange={onChange}
      >
        {data.map(({ name, calendarsID }) => {
          return <MenuItem value={calendarsID}>{name}</MenuItem>;
        })}
        <MenuItem value={0}>Vyberte</MenuItem>
      </Select>
    </FormControl>
  );
}
