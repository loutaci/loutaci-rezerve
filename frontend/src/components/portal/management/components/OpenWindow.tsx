import React, { useState, useEffect } from "react";
import style from "./style.module.css";

import Modal from "@mui/material/Modal";
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";

import DeleteIcon from "@mui/icons-material/Delete";

import { laravelCall } from "../../../calls";
import Approval from "./Approval";
import Editor from "@monaco-editor/react";

interface propsAddFromInterface {
  open: boolean;
  onClose: () => void;
  id: number;
}

export interface formDataInterface {
  calendarsID: number | null;
  eventsID: number | null;
  windowsID?: number | null;
  formsID: number;
  description: string | null;
  name: string;
}

const sx = {
  width: "100%",
  pb: "10px",
};

const buttonStyle = {
  borderRadius: 0,
  width: "100%",
};

export default function OpenWindow(props: propsAddFromInterface) {
  const { onClose, open, id } = props;
  const [openApproval, setOpenApproval] = useState(false);
  const [code, setCode] = useState("");
  const [windowType, setWindowType] = useState("calendar");

  const [formData, setFormData] = useState<formDataInterface>({
    calendarsID: null,
    eventsID: null,
    formsID: 0,
    description: "",
    name: "",
    windowsID: id,
  });

  async function save() {
    console.log("save");
    await laravelCall.update("windows", id, formData)();
    onClose();
  }

  async function remove() {
    console.log("remove");
    await laravelCall.delete("windows", id)();
    onClose();
  }

  useEffect(() => {
    if (id !== 0) {
      laravelCall
        .get(`windows/${id}`)()
        .then((data: any) => {
          console.log(data);
          if (data.calendarsID) {
            setWindowType("calendar");
          } else {
            setWindowType("event");
          }
          setFormData(data);
        })
        .catch((err) => {});
    }
  }, [id]);

  const setForm =
    (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setFormData({ ...formData, [name]: event.target.value });
    };

  return (
    <div>
      <Modal open={open} aria-labelledby="Add Form" aria-describedby="Add Form">
        <div className={style.modal}>
          <div className={style.header}>
            {formData.name}
            <IconButton onClick={onClose} color="primary" component="span">
              <CloseIcon />
            </IconButton>
            <IconButton
              onClick={() => {
                setOpenApproval(true);
              }}
              color="primary"
              component="span"
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <div className={style.body}>
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Název"}
              label={"Název"}
              sx={sx}
              value={formData.name}
              onChange={setForm("name")}
            />
            <TextField
              id="outlined-error-helper-text"
              size="small"
              name={"Popis"}
              label={"Popis"}
              sx={sx}
              multiline
              rows={4}
              value={formData.description}
              onChange={setForm("description")}
            />
            <SelectForm
              value={formData.formsID}
              onChange={(event) => {
                setFormData({ ...formData, formsID: Number(event.target.value) });
              }}
            />
            <FormControl sx={sx} size="small" fullWidth>
              <InputLabel id="demo-simple-select-label">Typ okna</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={windowType}
                label="Akce"
                onChange={(e) => {
                  setWindowType(e.target.value);
                  setFormData({ ...formData, calendarsID: null, eventsID: null });
                }}
              >
                {[
                  { name: "Kalendář", value: "calendar" },
                  { name: "Akce", value: "event" },
                ].map(({ name, value }) => {
                  return <MenuItem value={value}>{name}</MenuItem>;
                })}
              </Select>
            </FormControl>
            {windowType === "calendar" ? (
              <SelectCalendar
                value={formData.calendarsID}
                onChange={(event) => {
                  setFormData({ ...formData, calendarsID: Number(event.target.value) });
                }}
              />
            ) : (
              <SelectEvent
                value={formData.eventsID}
                onChange={(event) => {
                  setFormData({ ...formData, eventsID: Number(event.target.value) });
                }}
              />
            )}
            <Button sx={buttonStyle} variant="contained" onClick={save}>
              Aktualizovat Okno
            </Button>
            <Button
              style={{ marginTop: "10px" }}
              sx={buttonStyle}
              variant="contained"
              color="secondary"
              onClick={() => {
                setCode(`
<!-- Vložte tento html kód na místo ve stránce kde to chcete vložit -->
<script defer="defer" src="https://test-vitek.loutaci.cz/static/js/bundle.js"></script>
  <div style="width:100%; height:300px" >
  <loutacky-formular windowid="${formData.windowsID}" />
</div>`);
              }}
            >
              Generovat kód pro vložení do stránky
            </Button>
            {code && (
              <Editor
                height="200px"
                defaultLanguage="html"
                value={code}
                // @ts-ignore
                onChange={(value) => setCode(value)}
              />
            )}
          </div>
          <Approval
            open={openApproval}
            onClose={() => {
              setOpenApproval(false);
            }}
            name={formData.name}
            remove={remove}
          />
        </div>
      </Modal>
    </div>
  );
}

interface selectFormDataInterface {
  name: string;
  description: string | null;
  formsID: number;
  config: [];
}

function SelectForm({
  value,
  onChange,
}: {
  value: number;
  onChange: (event: SelectChangeEvent<number>) => void;
}) {
  const [data, setData] = useState<selectFormDataInterface[]>([]);

  useEffect(() => {
    laravelCall
      .get(`forms`)()
      .then((data: any) => {
        console.log(data);
        setData(data);
      })
      .catch((err) => {});
  }, []);

  return (
    <FormControl sx={sx} size="small" fullWidth>
      <InputLabel id="demo-simple-select-label">Formulář</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value}
        label="Formulář"
        onChange={onChange}
      >
        {data.map(({ name, formsID }) => {
          return <MenuItem value={formsID}>{name}</MenuItem>;
        })}
        <MenuItem value={0}>Vyberte</MenuItem>
      </Select>
    </FormControl>
  );
}

interface selectCalendarDataInterface {
  name: string;
  config: object;
  description: string;
  type: string;
  calendarsID: number;
}

function SelectCalendar({
  value,
  onChange,
}: {
  value: number | null;
  onChange: (event: SelectChangeEvent<number>) => void;
}) {
  const [data, setData] = useState<selectCalendarDataInterface[]>([]);

  useEffect(() => {
    laravelCall
      .get(`calendars`)()
      .then((data: any) => {
        console.log(data);
        setData(data);
      })
      .catch((err) => {});
  }, []);

  return (
    <FormControl sx={sx} size="small" fullWidth>
      <InputLabel id="demo-simple-select-label">Kalendář</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value || 0}
        label="Kalendář"
        onChange={onChange}
      >
        {data.map(({ name, calendarsID }) => {
          return <MenuItem value={calendarsID}>{name}</MenuItem>;
        })}
        <MenuItem value={0}>Vyberte</MenuItem>
      </Select>
    </FormControl>
  );
}

export function SelectEvent({
  value,
  onChange,
}: {
  value: number | null;
  onChange: (event: SelectChangeEvent<number>) => void;
}) {
  const [data, setData] = useState<any[]>([]);

  useEffect(() => {
    laravelCall
      .get(`events`)()
      .then((data: any) => {
        console.log(data);
        setData(data);
      })
      .catch((err) => {});
  }, []);

  return (
    <FormControl sx={sx} size="small" fullWidth>
      <InputLabel id="demo-simple-select-label">Akce</InputLabel>
      <Select
        labelId="demo-simple-select-label"
        id="demo-simple-select"
        value={value || 0}
        label="Akce"
        onChange={onChange}
      >
        {data.map(({ name, eventsID }) => {
          return <MenuItem value={eventsID}>{name}</MenuItem>;
        })}
        <MenuItem value={0}>Vyberte</MenuItem>
      </Select>
    </FormControl>
  );
}
