import React, {useState,useEffect} from 'react';
import style from './style.module.css';

import Modal from '@mui/material/Modal';
import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import Button from '@mui/material/Button';

import DeleteIcon from '@mui/icons-material/Delete';

interface propsFromInterface{
  open: boolean,
  onClose: () => void,
  remove: () => void,
  name: string|null
}

const buttonStyle = {
  borderRadius: 0,
  width: "100%"
}

export default function Approval(props: propsFromInterface){
  const { onClose, open, name, remove} = props;


  return(
    <div>
      <Modal
        open={open}
        aria-labelledby="Open Event"
        aria-describedby="Open Event"
      >
        <div className={style.modal} >
          <div className={style.header} >
            {name}
            <IconButton onClick={onClose}  color="primary" component="span">
              <CloseIcon />
            </IconButton>
          </div>
          <div className={style.body}>

            <span>Odpravdu chcete odstanit?</span>
            <br></br>
            <span>{name}</span>

            <Button
              sx={buttonStyle}
              variant="contained"
              onClick={remove}
            >
              Odstranit
            </Button>

          </div>
        </div>
      </Modal>
    </div>
  );
  
}