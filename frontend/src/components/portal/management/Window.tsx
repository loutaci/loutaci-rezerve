import React, { useEffect, useState } from 'react';
import style from './style.module.css';

import Button from '@mui/material/Button';

import {laravelCall} from '../../calls';
import AddWindow from './components/AddWindow';
import OpenWindow from './components/OpenWindow';

interface dataInterface {
  calendarsID: number,
  formsID: number,
  windowsID: number,

  description: string | null,
  name: string,
}

const buttonStyle = {
  borderRadius: 0
}

export default function Window(){
  const [data, setData] = useState<dataInterface[]>([]);
  const [openAddForm, setOpenAddForm] = useState(false);
  const [openOpenForm, setOpenOpenForm] = useState({
    open: false,
    id: 0
  });

  useEffect(() => {
    loadData();
  },[]);

  function loadData() {
    laravelCall
      .get(`windows`)()
      .then((data:any) => {
				setData(data)
			})
			.catch((err)=>{
			
			})
  }

  console.log("windows data",data);

  return(
    <div>
      <Button
        sx={buttonStyle}
        variant="contained"
        onClick={()=>{setOpenAddForm(true)}}
      >
        Nový window
      </Button>

      <div className={style.list} >
        {data?.map(({description,name,windowsID})=>{
          return(
            <div key={windowsID} onClick={()=>{setOpenOpenForm({open:true,id:windowsID})}} >
              <span>{name}</span> <span>ID: {windowsID}</span> 
              <br></br>
              <span>{description}</span>
            </div>
          )
        })}
      </div>
      <AddWindow
        open={openAddForm}
        onClose={()=>{
          setOpenAddForm(false);
          loadData();
        }}
      />
      <OpenWindow
        open={openOpenForm.open}
        id={openOpenForm.id}
        onClose={()=>{
          setOpenOpenForm({open:false,id:0});
          loadData();
        }}
      />
    </div>
  )
}