import React, { useEffect, useState } from 'react';
import style from './style.module.css';

import Button from '@mui/material/Button';

import {laravelCall} from '../../calls';
import AddForm from './components/AddForm';
import OpenForm from './components/OpenForm';

interface formDataInterface {
  description: string | null,
  name: string,
  formsID: number,
  config: [],
}

const buttonStyle = {
  borderRadius: 0
}

export default function Form(){
  const [data, setData] = useState<formDataInterface[]>([]);
  const [openAddFrom, setOpenAddForm] = useState(false);
  const [openOpenFrom, setOpenOpenForm] = useState({
    open: false,
    id: 0
  });

  useEffect(() => {
    loadData();
  },[]);

  function loadData() {
    laravelCall
      .get(`forms`)()
      .then((data:any) => {
				setData(data)
			})
			.catch((err)=>{
			
			})
  }

  console.log("forms data",data);

  return(
    <div>
      <Button
        sx={buttonStyle}
        variant="contained"
        onClick={()=>{setOpenAddForm(true)}}
      >
        Nový formulář
      </Button>

      <div className={style.list} >
        {data?.map(({description,name,formsID})=>{
          return(
            <div key={formsID} onClick={()=>{setOpenOpenForm({open:true,id:formsID})}} >
              <span>{name}</span> <span>ID: {formsID}</span> 
              <br></br>
              <span>{description}</span>
            </div>
          )
        })}
      </div>
      <AddForm 
        open={openAddFrom}
        onClose={()=>{
          setOpenAddForm(false);
          loadData();
        }}
      />
      <OpenForm 
        open={openOpenFrom.open}
        id={openOpenFrom.id}
        onClose={()=>{
          setOpenOpenForm({open:false,id:0});
          loadData();
        }}
      />
    </div>
  )
}