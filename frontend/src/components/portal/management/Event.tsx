import React, { useEffect, useState } from "react";
import style from "./style.module.css";

import Button from "@mui/material/Button";
import moment from "moment/moment";

import { laravelCall } from "../../calls";
import AddEvent from "./components/AddEvent";
import OpenEvent from "./components/OpenEvent";

interface actionDataInterface {
  eventsID: number;
  name: string;
  description: string;
  price: number;
  time_end: string;
  time_start: string;
  date_end: string;
  date_start: string;
  capacity: number;
  addresesID: number;
  windowsID: number;
}

const buttonStyle = {
  borderRadius: 0,
};

export default function Event() {
  const [data, setData] = useState<actionDataInterface[]>([]);
  const [openAdd, setOpenAdd] = useState(false);
  const [openOpen, setOpenOpen] = useState({
    open: false,
    id: 0,
  });

  useEffect(() => {
    loadData();
  }, []);

  function loadData() {
    laravelCall
      .get("events")()
      .then((data: any) => {
        setData(data);
      })
      .catch((err) => {});
  }

  console.log("Events data", data);

  return (
    <div>
      <Button
        sx={buttonStyle}
        variant="contained"
        onClick={() => {
          setOpenAdd(true);
        }}
      >
        Nová událost
      </Button>

      <div className={style.list}>
        <span>Budoucí</span>
        {data?.map(
          ({ description, name, eventsID, date_start, date_end, time_start, time_end }) => {
            return (
              <div
                key={eventsID}
                onClick={() => {
                  setOpenOpen({ open: true, id: eventsID });
                }}
              >
                <span>{name}</span> <span>ID: {eventsID}</span>
                <br></br>
                <span style={{ fontWeight: "bold" }}> od: </span>{" "}
                <span>
                  {" "}
                  {moment(date_start).format("DD.MM.YYYY")} {time_start}
                </span>
                <span style={{ fontWeight: "bold" }}> do: </span>{" "}
                <span>
                  {" "}
                  {moment(date_end).format("DD.MM.YYYY")} {time_end}{" "}
                </span>
                <br></br>
                <span>{description}</span>
              </div>
            );
          }
        )}
        <br />
        <span>Minulé</span>
        {/* {data?.map(({description,name,eventsID, date_start,date_end,time_start,time_end})=>{
          if(false)return <></>
          return(
            <div key={eventsID} onClick={()=>{setOpenOpen({open:true,id:eventsID})}} >
              <span>{name}</span> <span>ID: {eventsID}</span> 
              <br></br>
              <span style={{fontWeight:"bold"}} > od: </span> <span> {moment(date_start).format('DD.MM.YYYY')} {time_start}</span>
              <span style={{fontWeight:"bold"}} > do: </span> <span> {moment(date_end).format('DD.MM.YYYY')} {time_end} </span>
              <br></br>
              <span>{description}</span>
            </div>
          )
        })} */}
      </div>
      <AddEvent
        open={openAdd}
        onClose={() => {
          setOpenAdd(false);
          loadData();
        }}
      />
      <OpenEvent
        open={openOpen.open}
        id={openOpen.id}
        onClose={() => {
          setOpenOpen({ open: false, id: 0 });
          loadData();
        }}
      />
    </div>
  );
}
