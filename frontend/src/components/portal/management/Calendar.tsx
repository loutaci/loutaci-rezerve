import React, {useEffect, useState} from 'react';
import style from './style.module.css';

import Button from '@mui/material/Button';

import {laravelCall} from '../../calls';
import AddCalendar from './components/AddCalendar';
import OpenCalendar from './components/OpenCalendar';

interface calendarDataInterface{
  name: string,
  config: object,
  description: string,
  type: string,
  calendarsID: number
}

const buttonStyle = {
  borderRadius: 0
}

export default function Calendar(){
  const [data, setData] = useState<calendarDataInterface[]>([]);
  const [openAddCalendar, setOpenAddCalendar] = useState(false);
  const [openOpenCalendar, setOpenOpenCalendar] = useState({
    open: false,
    id: 0
  });

  useEffect(() => {
    loadData();
  },[]);

  function loadData() {
    laravelCall
      .get(`calendars`)()
      .then((data:any) => {
				setData(data)
			})
			.catch((err)=>{
			
			})
  }

  console.log("Kalendáře data",data);

  return(
    <div>
      <Button
        sx={buttonStyle}
        variant="contained"
        onClick={()=>{setOpenAddCalendar(true)}}
      >
        Nový kalendář
      </Button>

      <div className={style.list} >
        {data?.map(({description,name,calendarsID,type})=>{
          return(
            <div key={calendarsID} onClick={()=>{setOpenOpenCalendar({open:true,id:calendarsID})}} >
              <span>{name}</span> <span>ID: {calendarsID}</span> 
              <br></br>
              <span>{description}</span>
            </div>
          )
        })}
      </div>
      <AddCalendar
        open={openAddCalendar}
        onClose={()=>{
          setOpenAddCalendar(false);
          loadData();
        }}
      />
      <OpenCalendar
        open={openOpenCalendar.open}
        id={openOpenCalendar.id}
        onClose={()=>{
          setOpenOpenCalendar({
            open: false,
            id: 0
          });
          loadData();
        }}
      />
    </div>
  )
}