import { Avatar, Box, Button, Input } from "@mui/material";
import React, { useEffect, useState } from "react";
import { laravelCall, useUser } from "../../calls";

export default function Profile() {
  const [file, setFile] = useState(null);
  const { user, refresh } = useUser();
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    email: "",
    image: null,
  });

  useEffect(() => {
    // @ts-ignore
    setFormData({ ...user });
  }, [user]);

  const setForm =
    (name: string) => (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
      setFormData({ ...formData, [name]: event.target.value });
    };

  async function save() {
    await laravelCall.update("users", user?.usersID as number, formData)();
    refresh();
  }

  return (
    <div>
      <h2>Správa profilu</h2>
      <Box className="" style={{ display: "flex", marginBottom: 15 }}>
        <Button variant="contained" component="label">
          Upload Photo
          <input
            style={{ marginLeft: 10 }}
            type="file"
            hidden
            onChange={(e: any) => {
              const file = e.target.files[0];
              console.log(file);

              if (file.type.includes("image")) {
                const reader = new FileReader();
                reader.addEventListener("load", (event) => {
                  // console.log(event?.target?.result);
                  // @ts-ignore
                  setFormData((data) => ({ ...data, image: event?.target?.result }));
                });
                reader.readAsDataURL(file);
                // img.src = event.target.result;
              } else {
                alert("Neplatný typ souboru, přiložte obrázek ve formátu jpg nebo png.");
              }
            }}
          />
        </Button>
        {/* @ts-ignore */}
        <Avatar src={formData?.image}></Avatar>
      </Box>

      <div style={{ marginLeft: 10 }}>
        <Input placeholder="jmeno" value={formData.first_name} onChange={setForm("first_name")} />
        <Input placeholder="prijmeni" value={formData.last_name} onChange={setForm("last_name")} />
        <Input placeholder="email" value={formData.email} onChange={setForm("email")} />
        <Button variant={"contained"} size="large" onClick={save}>
          Uložit
        </Button>
        {/* <h2 style={{ margin: 0 }}>
            {user?.first_name} {user?.last_name}
          </h2>
          
          <p style={{ margin: 0 }}>{user?.email}</p> */}
      </div>
    </div>
  );
}
