import React, { useState } from "react";
import { laravelCall } from "../../calls";
import { useQuery } from "react-query";

import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";
import moment from "moment";
import { setSelectionRange } from "@testing-library/user-event/dist/utils";
import { Button } from "@mui/material";
import { ShareOutlined } from "@mui/icons-material";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";

import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import DehazeIcon from "@mui/icons-material/Dehaze";

import Calendar from "../../web-component/calendar/Calendar";

export default function ReservationIndex() {
  const { isLoading, error, data } = useQuery<any>(`windows`, laravelCall.get(`windows`));

  console.log(isLoading, error);

  if (isLoading) return <div>Loading...</div>;

  return (
    <div>
      <h1>Seznamy rezerva</h1>

      {data.map((window_data: any) => {
        if (window_data.calendarsID) {
          console.log({ window_data });
          // return "hello";
          return (
            <div key={window_data.windowsID}>
              <CalendarReservations window_data={window_data} />
            </div>
          );
        }
      })}
    </div>
  );
}

const columns: GridColDef[] = [
  {
    field: "reservationsID",
    headerName: "ID",
    width: 70,
  },
  {
    field: "date_start",
    headerName: "Začátek",
    width: 200,
    valueFormatter: ({ value }) => moment(value).format("d.M.Y"),
  },
  {
    field: "date_end",
    headerName: "Konec",
    width: 200,
    valueFormatter: ({ value }) => moment(value).format("d.M.Y"),
  },
  {
    field: "customer",
    headerName: "Jméno a přijmení",
    width: 200,
    renderCell: (params) => <pre>{`${params.value.first_name} ${params.value.last_name}`}</pre>,
  },
  {
    field: "data",
    headerName: "Data",
    width: 500,
    renderCell: (params) => <pre>{JSON.stringify(params.value, null, 2)}</pre>,
  },
  {
    field: "created_at",
    headerName: "Vytvořena",
    width: 160,
    valueFormatter: ({ value }) => moment(value).format("HH:MM:ss D.M.Y"),
  },

  // {
  //   field: "age",
  //   headerName: "Age",
  //   type: "number",
  //   width: 90,
  // },
  // {
  //   field: "fullName",
  //   headerName: "Full name",
  //   description: "This column has a value getter and is not sortable.",
  //   sortable: false,
  //   width: 160,
  //   valueGetter: (params: GridValueGetterParams) =>
  //     `${params.row.firstName || ""} ${params.row.lastName || ""}`,
  // },
];

function CalendarReservations({ window_data }: any) {
  const { isLoading, error, data, refetch } = useQuery<any>(
    `windows/${window_data.windowsID}/reservations`,
    laravelCall.get(`windows/${window_data.windowsID}/reservations`)
  );
  const [selectedIds, setSelectedIds] = useState<number[]>([]);
  const [display, setDisplay] = useState<string>("calendar");

  function deleteReservations() {
    let promises: Promise<any>[] = [];

    if (window.confirm("Chcete opravdu smazat " + selectedIds.length + " rezervací?")) {
      selectedIds.forEach((id: any) => {
        promises.push(laravelCall.delete(`reservations`, id)());
      });
    }
    Promise.all(promises).then(() => {
      refetch();
      setSelectedIds([]);
    });
  }

  const handleAlignment = (event: React.MouseEvent<HTMLElement>, newAlignment: string | null) => {
    if (newAlignment !== null) {
      setDisplay(newAlignment);
    }
  };

  if (isLoading) return <div>Loading...</div>;

  // const reservations = data.map((v: any) => v);

  return (
    <div>
      <h2 style={{ marginBottom: 0 }}>
        {window_data.name}{" "}
        <a
          target={"_blank"}
          href={"https://rezervace.loutaci.cz/calendar/test/" + window_data.windowsID}
        >
          <ShareOutlined />
        </a>{" "}
      </h2>
      <small style={{ margin: 10, marginBottom: 0, padding: 0, display: "block" }}>
        {window_data.description}
      </small>
      <br />
      <ToggleButtonGroup
        value={display}
        exclusive
        onChange={handleAlignment}
        aria-label="text alignment"
      >
        <ToggleButton value="table" aria-label="left aligned">
          <DehazeIcon />
        </ToggleButton>
        <ToggleButton value="calendar" aria-label="centered">
          <CalendarMonthIcon />
        </ToggleButton>
      </ToggleButtonGroup>

      {display === "calendar" ? (
        <div style={{ width: "100%", height: "400px", backgroundColor: "red" }}>
          <Calendar windowid={window_data.windowsID.toString()} />
        </div>
      ) : (
        ""
      )}
      {display === "table" ? (
        <>
          <div style={{ height: 400, width: "100%" }}>
            <DataGrid
              getRowId={(row: any) => row.reservationsID}
              rows={data}
              columns={columns}
              pageSize={10}
              rowsPerPageOptions={[10]}
              checkboxSelection={true}
              autoHeight={false}
              rowHeight={180}
              selectionModel={selectedIds}
              onSelectionModelChange={(selectionsIDs) => {
                // console.log({ selectionModel });
                setSelectedIds(selectionsIDs as any);
              }}
            />
          </div>
          <Button
            variant="contained"
            color="error"
            onClick={() => {
              deleteReservations();
            }}
          >
            Smazat vybrané {selectedIds.length}
          </Button>
        </>
      ) : (
        ""
      )}
    </div>
  );
}
