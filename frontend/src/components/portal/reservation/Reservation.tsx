import React, { useState } from "react";
import { laravelCall } from "../../calls";
import { useQuery } from "react-query";
import style from "./style.module.css";

import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";
import moment from "moment";
import { setSelectionRange } from "@testing-library/user-event/dist/utils";
import { Button } from "@mui/material";
import { ShareOutlined } from "@mui/icons-material";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";

import DehazeIcon from "@mui/icons-material/Dehaze";

import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import ContentPasteIcon from '@mui/icons-material/ContentPaste';
import EditIcon from '@mui/icons-material/Edit';
import Tooltip from '@mui/material/Tooltip';

import Calendar from "../../web-component/calendar/Calendar";
import TableReservation from "./components/TableReservation";
import CalendarReservation from "./components/CalendarReservation";

interface windowDataInterface {
  portalsID: number,
  windowsID: number,
  calendarsID: number,
  formsID: number,
  eventsID: number|null,
  name: string,
  description: string|null
}

export default function Reservation() {
  const { isLoading, error, data } = useQuery<any>(`windows`, laravelCall.get(`windows`));

  console.log(isLoading, error);

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error...</div>;
  if (data) {console.log("windows data",data)};

  return (
    <div className={style.Body}>
      <h1>Seznamy rezervací</h1>

      {data.map((window_data: windowDataInterface) => {
        if (window_data.calendarsID) {
          return (
            <div key={window_data.windowsID}>
              <h3>{window_data.name}</h3>
              <ReserveNavbar window_data={window_data}/>
              <ReserveWindow  window_data={window_data}/>
            </div>
          );
        }
      })}
    </div>
  );
}

function ReserveWindow({window_data}:{window_data:windowDataInterface}){
  const [selectID,setSelectID] = useState<number[]>([]);

  return(
    <div className={style.Reserve}>
      <CalendarReservation windowsID={window_data.windowsID} selectID={selectID} setSelectID={setSelectID}/>
      <TableReservation windowsID={window_data.windowsID} selectID={selectID} setSelectID={setSelectID}/>
    </div>
  );
}

function ReserveNavbar({window_data}:{window_data:windowDataInterface}){
  return(
    <div className={style.Navbar}>
      <div>
        ID: {window_data.windowsID} - {window_data.description}
        <Tooltip title="Editovat kalendáře" placement="top" arrow>
          <IconButton aria-label="delete">
            <CalendarMonthIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Editovat formuláře" placement="top" arrow>
          <IconButton aria-label="delete">
            <ContentPasteIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="Editovat" placement="top" arrow>
          <IconButton aria-label="delete">
            <EditIcon />
          </IconButton>
        </Tooltip>
      </div>
    </div>
  );
}