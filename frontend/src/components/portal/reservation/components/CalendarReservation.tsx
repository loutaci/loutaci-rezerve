import React, { useState } from "react";
import { laravelCall } from "../../../calls";
import { useQuery } from "react-query";
import style from "../style.module.css";

import Calendar from "../../../web-component/calendar/Calendar";
interface dataCalendarInterface {
  windowsID: number,
  selectID: number[],
  setSelectID: React.Dispatch<React.SetStateAction<number[]>>
}

export default function CalendarReservation({windowsID,selectID,setSelectID}:dataCalendarInterface){


  return(
    <div className={style.Calendar}>
      <div style={{height:"100%",backgroundColor:"#ff5151"}}>
        <div style={{height:"calc(100% - 20px)", width:"calc(100% - 20px)"}}>
          <Calendar windowid={windowsID.toString()} />
        </div>
      </div>
    </div>
  );
}