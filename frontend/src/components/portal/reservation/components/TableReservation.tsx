import React, { useState } from "react";
import { laravelCall } from "../../../calls";
import { useQuery } from "react-query";
import style from "../style.module.css";

import { DataGrid, GridColDef, GridValueGetterParams } from "@mui/x-data-grid";
import moment from "moment";

const columns: GridColDef[] = [
  {
    field: "reservationsID",
    headerName: "ID",
    width: 70,
  },
  {
    field: "date_start",
    headerName: "Začátek",
    width: 100,
    valueFormatter: ({ value }) => moment(value).format("d.M.Y"),
  },
  {
    field: "date_end",
    headerName: "Konec",
    width: 100,
    valueFormatter: ({ value }) => moment(value).format("d.M.Y"),
  },
  {
    field: "customer",
    headerName: "Jméno a přijmení",
    width: 200,
    renderCell: (params) => <pre>{`${params.value.first_name} ${params.value.last_name}`}</pre>,
  },
  {
    field: "data",
    headerName: "Data",
    
    renderCell: (params) => <pre>{JSON.stringify(params.value, null, 2)}</pre>,
  },
  {
    field: "created_at",
    headerName: "Vytvořena",
    width: 160,
    valueFormatter: ({ value }) => moment(value).format("HH:MM:ss D.M.Y"),
  },

  // {
  //   field: "age",
  //   headerName: "Age",
  //   type: "number",
  //   width: 90,
  // },
  // {
  //   field: "fullName",
  //   headerName: "Full name",
  //   description: "This column has a value getter and is not sortable.",
  //   sortable: false,
  //   width: 160,
  //   valueGetter: (params: GridValueGetterParams) =>
  //     `${params.row.firstName || ""} ${params.row.lastName || ""}`,
  // },
];

interface dataTableInterface {
  windowsID: number,
  selectID: number[],
  setSelectID: React.Dispatch<React.SetStateAction<number[]>>
}

export default function TableReservation({windowsID,selectID,setSelectID}:dataTableInterface){
  const { isLoading, error, data, refetch } = useQuery<any>(
    `windows/${windowsID}/reservations`,
    laravelCall.get(`windows/${windowsID}/reservations`)
  );

  const [selectedIds, setSelectedIds] = useState<number[]>([]);

  if (isLoading) return <div>Loading...</div>;
  console.log("Tabulka data",data);

  return(
    <div className={style.Table}>
      <div style={{ height: 400 }}>
        <DataGrid
          getRowId={(row: any) => row.reservationsID}
          rows={data}
          columns={columns}
          pageSize={10}
          rowsPerPageOptions={[10]}
          checkboxSelection={true}
          autoHeight={false}
          rowHeight={40}
          selectionModel={selectedIds}
          onSelectionModelChange={(selectionsIDs) => {
            // console.log({ selectionModel });
            setSelectedIds(selectionsIDs as any);
          }}
        />
      </div>
    </div>
  )
}