import { Message } from "@mui/icons-material";
import { useEffect, useState } from "react";
import { useQuery } from "react-query";

class LaravelCall {
  public url: string;
  public token: string | null;
  public portalID: number = 1;

  constructor(url: string) {
    this.url = url;
    this.token = localStorage.getItem("reservation_login_token");
    this.portalID = Number(localStorage.getItem("reservation_portal_id")) || 27;
  }

  setPortalID(id: number) {
    localStorage.setItem("reservation_portal_id", id.toString());
    this.portalID = id;
  }

  setToken(token: string) {
    localStorage.setItem("reservation_login_token", token);
    this.token = token;
  }

  // TODO
  register() {}

  // TODO
  login() {}

  // TODO
  logout() {}

  // vylistování položek
  get(path: "forms" | "calendars" | "events" | "windows" | "user" | "portal-users" | any) {
    return () => this.fetchApi(path);
  }

  // vytvoření nové položky
  add(path: "forms" | "calendars" | "events" | "portals" | "register" | "windows" | "reservations" | "portal-users", body: Object) {
    return () => this.fetchApi(path, body, "POST");
  }

  // smazání položky
  delete(path: "forms" | "calendars" | "events" | "portals" | "windows" | "reservations" | "portal-users", id: number) {
    return () => this.fetchApi(path + "/" + id, undefined, "DELETE");
  }

  // aktualizace položky
  update(path: "forms" | "calendars" | "events" | "windows" | "users" | "reservations-paid", id: number, body: Object) {
    return () => this.fetchApi(path + "/" + id, body, "PATCH");
  }

  fetchApi(path: string, body = undefined as Object | undefined, method = "GET"): Promise<Object> {
    if (body) {
      // @ts-ignore
      body["_method"] = method;
      // @ts-ignore
      body["portalsID"] = this.portalID;
    }

    let stringBody = typeof body === "object" ? JSON.stringify(body) : body;

    return fetch(this.url + "/" + path, {
      body: stringBody || undefined,
      method: method.toUpperCase() === "GET" ? "GET" : method.toUpperCase() === "DELETE" ? "DELETE" : "POST",
      //  @ts-ignore
      headers: {
        "Content-Type": "application/json",
        Authorization: this.token ? `Bearer ${this.token}` : "",
        "Portal-Id": this.portalID,
      },
    })
      .then(v => v.json())
      .then(v => {
        if (v.success === true) {
          return v.data;
        } else {
          throw new Error(v.message);
        }
      });
  }
}

interface Portal {
  portalsID: number;
  name: string;
  laravel_through_key: number;
}

export interface User {
  email: string;
  first_name: string;
  last_name: string;
  phone?: any;
  usersID: number;
  email_verified_at?: any;
  created_at: Date;
  updated_at: Date;
  portals: Portal[];
  roleID: number;
  image?: string;
}

export function useUser() {
  // const [user, setUser] = useQuery<User | null>(`users`, laravelCall.get('user'));
  const [user, setUser] = useState<User | null>(null);
  const [isLoading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    laravelCall
      .get("user")()
      .then((user: any) => {
        if (user) {
          setUser(user);
          setLoading(false);
        }
      })
      .catch(e => {
        setLoading(false);
      });
  }, []);

  function refresh() {
    laravelCall
      .get("user")()
      .then((user: any) => {
        if (user) {
          setUser(user);
        }
      })
      .catch(e => {});
  }
  // useEffect(() => {
  //   laravelCall.get('user')().then((user: any) => {
  //     if (user) {
  //       setUser(user);
  //       setLoading(false);
  //     }
  //   }).catch((e) => {
  //     setLoading(false);
  //   });
  // }, []);

  // function refresh() {
  //   laravelCall.get('user')().then((user: any) => {
  //     if (user) {
  //       setUser(user);
  //     }
  //   }).catch((e) => {
  //   });
  // }

  // console.log({user});

  function login(email: string, password: string) {
    const options = {
      method: "POST",
      headers: {
        // Authorization: 'Bearer epRzZU1ydbE0lGQzlEyXg8cUIce9aswQzsD9RPpp',
        "content-type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
        device_name: "web" + Math.random(),
      }),
    };

    return fetch(`${laravelCall.url}/sanctum/token`, options)
      .then(v => v.json())
      .then(({ token, message }: any) => {
        if (token) {
          localStorage.setItem("reservation_login_token", token);
          laravelCall.setToken(token);
        }
        return message;
      })
      .catch(v => {
        // console.warn(v);
        return { message: v.message };
      });
  }

  function logout() {
    localStorage.removeItem("reservation_login_token");
    localStorage.removeItem("reservation_portal_id");

    window.location.reload();
  }

  function register() {}

  function getPortals() {}

  return { user: user, isLoading, login, logout, register, getPortals, refresh };
}

export const laravelCall = new LaravelCall("https://rezervace-api.loutaci.cz/api");
// export const laravelCall = new LaravelCall('http://localhost:8000/api');

//@ts-ignore
window.laravelCall = laravelCall;
