import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Calendar from "./calendar/Calendar";
import { laravelCall } from "../calls";
import Events from "./events/Events";

function LoutaciForm(props: any) {
  const [data, setData] = useState<any>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");
  useEffect(() => {
    laravelCall
      .get(`windows/${props.windowid}`)()
      .then((data: any) => {
        setData(data);
        console.log("data window_id " + props.windowid, data);
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
        setError(err.toString());
      });
  }, []);

  return (
    <>{data?.calendarsID ? <Calendar {...props} /> : <Events {...props} />}</>
  );
}

LoutaciForm.propTypes = {
  windowid: PropTypes.string,
};

export default LoutaciForm;
