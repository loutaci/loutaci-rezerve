import React, { useEffect, useState } from "react";
import styleForm from "./form.module.css";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";

import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import SaveIcon from "@mui/icons-material/Save";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import CircularProgress from '@mui/material/CircularProgress';

import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';

import Text, { textInterface } from "./fields/Text";
import NumberField, { numberInterface } from "./fields/Number";
import { laravelCall } from "../../calls";
import CalendarData from "./steps/CalendarData";
import Customer, {customerDataInterface} from "./steps/Customer";
import UserData, { configFieldInterface } from "./steps/UserData";

export interface formInterface {
  open: boolean;
  window_id: number;
  startDate: string;
  startTime: string;
  endDate: string;
  endTime: string;
}

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "700px",
  height: "600px",
  bgcolor: "white",
  boxShadow: 24,
  outline: "none !important",
};

// Data formuláře
interface formDataInterface {
  portalsID: number,              // ID portálu
  formsID: number,                // ID formuláře
  name: string,                   // Název formuláře
  description: string,            // Popis formuláře
  config: configFieldInterface[], // Konfigurace políček uživatelské části formuláře
}

export default function Form({modalData, setModalData}:{modalData:formInterface, setModalData:React.Dispatch<React.SetStateAction<formInterface>>}){
  const [data,setData] = useState<formDataInterface>();             // Konfigurační data formuláře
  const [customer,setCustomer] = useState<customerDataInterface>({  // Data o zákaznékoj
    email: "",
    first_name: "",
    last_name: "",
    phone: ""
  });
  const [dataForm,setDataForm] = useState<any>({});                 // Data uživateslky definovaného formuláře
  const [stepNumber,setStepNumber] = useState(0);                   // Číslo etapy (stránky) formuláře

  const [loading,setLoading] = useState(true);
	const [error,setError] = useState("");

  function close(){
    setModalData({...modalData, open:false})
  }

  /*useEffect(() => {
    let datta = {};
    for (let i = 1; data.config.length > i; i++) {
      //@ts-ignore
      datta[data.config[i].key] = null;
    }
    setDataForm(datta);
  },[data]);*/

  // Načtení konfiguračních dat formuláře 
	useEffect(() => {
    laravelCall
      .get(`windows/${modalData.window_id}/form`)()
      .then((data:any) => {
				setData(data);
				console.log(data);
				setLoading(false);
			})
			.catch((err)=>{
				setLoading(false);
				setError(err.toString());
			})
  }, []);

  // Odeslání formuláře
  async function sendReserve() {
    const saveData = {
      data: JSON.stringify(dataForm),
      date_end: modalData.endDate,
      date_start: modalData.startDate,
      time_end: modalData.endTime,
      time_start: modalData.startTime,
      customer: customer,
      windowsID:  modalData.window_id
    }
    console.log(saveData)
    await laravelCall.add(`reservations`, saveData)().then(data => {console.log("then",data)}).catch(data => {console.log("catch",data)});
    alert("Rezervace dokončete, pro další informace zkontrolujte vaši emailovou schranku");
    close();
  }

  const steps = ['Čas rezervace', 'Zákazník', 'Doplňující informace', 'Fakturační údaje', 'Dokončení'];

  return(
    <Modal
      open={modalData.open}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box sx={style}>
        <div className={styleForm.header}>
          Dokončení rezervace
            <IconButton onClick={close} color="primary" component="span">
              <CloseIcon />
            </IconButton>
        </div>
        
        <div className={styleForm.body}>
          <Stepper
            activeStep={stepNumber}
            alternativeLabel
            sx={{
              ml: "-10px",
              mr: "-10px",
              mb: "15px"
            }}
          >
            {steps.map((label) => {
              return (
                <Step key={label}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              );
            })}
          </Stepper>
          
          
          {stepNumber === 0 && // Stránka dat z kalendáře
            <CalendarData modalData={modalData} setModalData={setModalData} />
          }

          {stepNumber === 1 && // Stránka zákazníka
            <Customer customer={customer} setCustomer={setCustomer}/>
          }

          {stepNumber === 2 && // Stránka uživatelsky definovaných polí
            <UserData config={data?.config} dataForm={dataForm} setDataForm={setDataForm} />
          }

          {stepNumber === 3 && // Stránka fakturačních údajů
            <React.Fragment>

            </React.Fragment>
          }

          {stepNumber === 4 && // Dokončení
            <React.Fragment>

            </React.Fragment>
          }

        </div>

        <div className={styleForm.footer}>
          <Button
            disabled={stepNumber === 0}
            onClick={()=>{setStepNumber((prevStepNumber) => prevStepNumber - 1)}}
          >
            Zpět
          </Button>
          <Button
            onClick={()=>{
              if(stepNumber === steps.length - 1){
                sendReserve();
              }else{
                setStepNumber((prevStepNumber) => prevStepNumber + 1)
              }
            }}
          >
            {stepNumber === steps.length - 1 ? 'Dokončit rezervaci' : 'Další'}
          </Button>
        </div>
      </Box>
    </Modal>
  );
}
