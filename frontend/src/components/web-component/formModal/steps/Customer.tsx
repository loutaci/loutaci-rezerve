import React, { useState } from "react";
import { styleInterface } from "./UserData";
import Text from "../fields/Text";

export interface customerDataInterface {
  email: string,
  first_name: string,
  last_name: string,
  phone: string
}

const textStyle:styleInterface = {
  width: 50
}

export default function Customer({customer,setCustomer}:{customer:customerDataInterface,setCustomer:React.Dispatch<React.SetStateAction<customerDataInterface>>}){
  
  return(
    <React.Fragment>
      <Text
        style={textStyle}
        config={{
          name: "Jméno",
          readable: true
        }}
        value={customer.first_name}
        onChange={(event:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>)=>{
          setCustomer({...customer, first_name: event.target.value});
        }}
      />
      <Text
        style={textStyle}
        config={{
          name: "Příjmení",
          readable: true
        }}
        value={customer.last_name}
        onChange={(event:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>)=>{
          setCustomer({...customer, last_name: event.target.value});
        }}
      />
      <Text
        style={textStyle}
        config={{
          name: "email",
          regexExpression: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/,
          readable: true
        }}
        value={customer.email}
        onChange={(event:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>)=>{
          setCustomer({...customer, email: event.target.value});
        }}
      />
      <Text
        style={textStyle}
        config={{
          name: "Telefon",
          readable: true
        }}
        value={customer.phone}
        onChange={(event:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>)=>{
          setCustomer({...customer, phone: event.target.value});
        }}
      />
    </React.Fragment>
  );
}