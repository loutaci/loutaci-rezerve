import React from "react";

import Text, { textInterface } from "../fields/Text";
import NumberField, { numberInterface } from "../fields/Number";


// Konfigurace políček uživatelské části formuláře
export interface configFieldInterface {
  key: string;              // Identifikátor políčka
  type: "text"|"number"|"date"|"time"|"select"|"file"; // Typ políčka
  style: styleInterface;    // Úpravy vzhedu a rozložení

  text?: textInterface;     // Rozšiřující data, pouze u textového políčka
  number?: numberInterface; // Rozšiřující data, pouze u číselného políčka
  date?: {                  
    helperText?: string; // text nápovědy
    date: string;
  };
  time?: {
    helperText?: string; // text nápovědy
    time: string;
  };
}

export interface styleInterface {
  width?: number; // procentuální šířka
}

export default function UserData({config,dataForm,setDataForm}:{config?:configFieldInterface[],dataForm:any,setDataForm:React.Dispatch<any>}){

  return(
    <React.Fragment>
      {config?.map(({type,style,text,number,key})=>{
        switch(type){
          case "text":
            if(text){
              return(
                <Text
                  key={key}
                  style={style}
                  config={text}
                  value={dataForm[key]}
                  onChange={(event:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>)=>{
                    let data = {...dataForm};
                    data[key] = event.target.value;
                    setDataForm(data);
                  }}
                />
              )
            }
            break;
          case "number":
            if(number){
              return(
                <NumberField
                  key={key}
                  style={style}
                  config={number}
                  value={dataForm[key]}
                  onChange={(event:React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>)=>{
                    let data = {...dataForm};
                    data[key] = event.target.value;
                    setDataForm(data);
                  }}
                />
              )
            }
            break;
          case "select":
            break;
          case "file":
            break;
          case "time":
            break;
          case "date":
            break;
        }
      })}
    </React.Fragment>
  )
}