import React from "react";

import TextField from "@mui/material/TextField";
import { formInterface } from "../Form";

const sxDate = {
  width: "calc(50% - 10px)",
  margin: "5px"
}

export default function CalendarData({modalData, setModalData}:{modalData:formInterface, setModalData:React.Dispatch<React.SetStateAction<formInterface>>}){

  return(
    <React.Fragment>
      {modalData.startDate === modalData.endDate ? 
        <TextField
          id="outlined-error-helper-text"
          size="small"
          sx={{
            width: "calc(100% - 10px)",
            margin: "5px"
          }}
          label="Datum"
          type="date"
          value={modalData.startDate}
          //onChange={onChange}
        />
      :
        <React.Fragment>
          <TextField
            id="outlined-error-helper-text"
            size="small"
            sx={sxDate}
            label="Od data"
            type="date"
            value={modalData.startDate}
            //onChange={onChange}
          />
          <TextField
            id="outlined-error-helper-text"
            size="small"
            sx={sxDate}
            label="Do data"
            type="date"
            value={modalData.endDate}
            //onChange={onChange}
          />
        </React.Fragment>
      }
      
      {modalData.startTime === "00:00" ? 
        "" 
      :
        <TextField
          id="outlined-error-helper-text"
          size="small"
          sx={sxDate}
          label="Od času"
          type="time"
          value={modalData.startTime}
          //onChange={onChange}
        />
      }
      
      {modalData.endTime === "00:00" ? 
        "" 
      :
        <TextField
          id="outlined-error-helper-text"
          size="small"
          sx={sxDate}
          label="Do času"
          type="time"
          value={modalData.endTime}
          //onChange={onChange}
        />
      }
    </React.Fragment>
  );
}