import React, { useEffect, useState } from "react";
import TextField from "@mui/material/TextField";

import { styleInterface } from "../steps/UserData";

export interface textInterface {
  name: string,
  helperText?: string; // text nápovědy
  regexExpression?: RegExp; // validační výraz
  defaultValue?: string;
  readable: boolean; // jestli je obsah čitelný
}

// políčko pro zadání textu/hesla
export default function Text({
  style,
  config,
  value,
  onChange,
}: {
  style: styleInterface;
  config: textInterface;
  value: any;
  onChange: (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => void;
}) {
  function errorControl(value?: string) {
    if (value) {
      if (config.regexExpression && value.length !== 0) {
        return !config.regexExpression.test(value);
      }
    }
    return false;
  }

  function sirka() {
    if (style.width) {
      return "calc(" + style.width + "% - 10px)";
    } else {
      return;
    }
  }

  const sx = {
    width: sirka() || "calc(100% - 10px)",
    margin: "5px",
  };

  return (
    <TextField
      id="outlined-error-helper-text"
      size="small"
      name={config.name}
      sx={sx}
      label={config.name}
      helperText={config.helperText}
      type={config.readable ? "text" : "password"}
      error={errorControl(value)}
      value={value}
      defaultValue={config.defaultValue}
      onChange={onChange}
    />
  );
}
