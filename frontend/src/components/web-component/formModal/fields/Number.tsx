import React, { useEffect, useState } from "react";
import TextField from "@mui/material/TextField";

import { styleInterface } from "../steps/UserData";

export interface numberInterface {
  name: string,
  helperText?: string; // text nápovědy
  min?: number;
  max?: number;
  defaultValue?: number;
}

// políčko pro zadání čísla
export default function NumberField({
  style,
  config,
  value,
  onChange,
}: {
  style: styleInterface;
  config: numberInterface;
  value: any;
  onChange: (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => void;
}) {
  function errorControl(value?: number) {
    let state = true;
    if (value) {
      if (config.min) {
        if (config.min >= value) {
          state = false;
        }
      }
      if (config.max) {
        if (config.max >= value) {
          state = false;
        }
      }
    }
    return state;
  }

  function sirka() {
    if (style.width) {
      return "calc(" + style.width + "% - 10px)";
    } else {
      return;
    }
  }

  const sx = {
    width: sirka() || "calc(100% - 10px)",
    margin: "5px",
  };

  return (
    <TextField
      id="outlined-error-helper-text"
      size="small"
      sx={sx}
      name={config.name}
      label={config.name}
      helperText={config.helperText}
      type="number"
      error={errorControl(value)}
      value={value}
      defaultValue={config.defaultValue}
      onChange={onChange}
      InputProps={{ inputProps: { ...config } }}
    />
  );
}
