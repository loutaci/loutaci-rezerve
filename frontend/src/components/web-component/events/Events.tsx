import React, { useEffect, useState } from "react";
import FormModal, { formInterface } from "../formModal/Form";
import { laravelCall } from "../../calls";
import { CircularProgress } from "@mui/material";
import style from "../calendar/calendar.module.css";
import moment from "moment";

interface propsInterface {
  windowid: number;
}

interface RootObject {
  calendarsID?: any;
  eventsID: number;
  name: string;
  description: string;
  windowsID: number;
  formsID: number;
  portalsID: number;
  events: Events;
  reservations: any[];
  paidCount: number;
  reservationsCount: number;
}

interface Events {
  capacity: number;
  date_end?: Date;
  date_start?: Date;
  description: string;
  name: string;
  price: number;
  time_end?: string;
  time_start?: string;
  eventsID: number;
  addressesID?: any;
  windowsID: number;
  portalsID: number;
}

export default function Events(props: propsInterface | any) {
  const [formModal, setFormModal] = useState<formInterface>({
    open: false,
    window_id: 0,
    startDate: "", //moment().format('YYYY-MM-DD')
    startTime: "",
    endDate: "",
    endTime: "",
  });
  const [data, setData] = useState<RootObject>({} as RootObject);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  useEffect(() => {
    laravelCall
      .get(`windows/${props.windowid}/events`)()
      .then((data: any) => {
        setData(data);
        setFormModal({
          ...formModal,
          endTime: data.events?.time_end,
          startTime: data.events?.time_start,
          startDate: moment(data.events?.date_start).format("YYYY-MM-DD"),
          endDate: moment(data.events?.date_end).format("YYYY-MM-DD"),
          window_id: data.windowsID,
        });
        setLoading(false);
      })
      .catch(err => {
        setLoading(false);
        setError("Nepodařilo se načíst akci");
      });
  }, []);

  if (loading) {
    return (
      <div className={style.calendar}>
        <CircularProgress />
      </div>
    );
  }
  if (error) {
    return (
      <div className={style.calendar}>
        <div style={{ backgroundColor: "white" }}>{error}</div>
      </div>
    );
  }

  return (
    <div className={style.calendar}>
      {/* <div>{JSON.stringify(data)}</div> */}
      <div
        className={style.event}
        onClick={() => setFormModal(v => ({ ...v, open: true }))}
      >
        <h1>{data.events?.name}</h1>
        <pre>{data.events?.description}</pre>
      </div>
      {formModal.open ? (
        <FormModal modalData={formModal} setModalData={setFormModal} />
      ) : (
        ""
      )}
    </div>
  );
}
