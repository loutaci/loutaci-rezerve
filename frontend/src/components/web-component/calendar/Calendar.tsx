import React, { useEffect, useState } from "react";
import style from "./calendar.module.css";
import PropTypes from "prop-types";
import c from "calendar";
import moment from "moment/moment";
import io, {Socket} from 'socket.io-client';

import Button from '@mui/material/Button';
import IconButton from "@mui/material/IconButton";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import CircularProgress from "@mui/material/CircularProgress";
import Tooltip from '@mui/material/Tooltip';

import { laravelCall } from "../../calls";
import FormModal, { formInterface } from "../formModal/Form";
import { lockedRegenerate, objectRegenerate, occupiedRegenerate } from "./utils";


const week = ["Po", "Út", "St", "Čt", "Pá", "So", "Ne"];
const month = [
  "Leden",
  "Únor",
  "Březen",
  "Duben",
  "Květen",
  "Červen",
  "Červenec",
  "Srpen",
  "Září",
  "Říjen",
  "Listopad",
  "Prosinec",
];

const cal = new c.Calendar(1);
const date = new Date();

const conf = {
  daysAllowed: [0, 1, 3, 4, 5],
};

interface selectDateInterface {
  startDate: Date | null;
  endDate: Date | null;
}

interface dataInterface {
  calendars: {
    type: string;           // Typ kalendáře (dny,hodiny)
    description: string;    // Popis kalendáře
    config: {
      days: boolean;        // Povolení výběru více dnů
      half: boolean;        // Povolení půlky dne (pouze u více dnů)
      rezerveMax: number;   // Maximání počet rezervací na jeden termín
      style: {
        day: {
          select: {
            color: string;
            backgroundColor: string;
          };
          reserve: {
            part: {
              color: string;
              backgroundColor: string;
            };
            full: {
              color: string;
              backgroundColor: string;
            };
          },
          disabled: {
            color: string;
            backgroundColor: string;
          },
          locked: {
            color: string;
            backgroundColor: string;
          }
        };
      };
    };
  };
  occupied: occupiedInterface[],
  exceptions: {
    date_start: string;
    date_end: string;
    time_start: string;
    time_end: string;
    desc: string;
    config: any;
  }[];
}

export interface occupiedInterface {
  date_start: string,
  date_end: string,
  time_start: string,
  time_end: string
};

export interface obsazeniInterface {
  color: string,
  number: number,
  start: boolean,
  end: boolean,
  locked: boolean
};

interface propsInterface {
  windowid: number;
};

export interface lockedDayInterface {
  socketId: string,
  select: selectDateInterface
};


function Calendar(props: propsInterface | any) {
  console.log("id kalendáře", props.windowid);
  const [formModal, setFormModal] = useState<formInterface>({
    open: false,
    window_id: 0,
    startDate: "2021-10-12", //moment().format('YYYY-MM-DD')
    startTime: "10:30",
    endDate: "2021-10-15",
    endTime: "09:00",
  });
  const [yearNumber, setYearNumber] = useState(date.getFullYear());
  const [monthNumber, setMonthNumber] = useState(date.getMonth());
  const [selectDate, setSelectDate] = useState<selectDateInterface>({
    startDate: null,
    endDate: null,
  });
  const [lockedDay, setLockedDay] = useState<lockedDayInterface[]>([]);
  const [data, setData] = useState<dataInterface>();
  const [obsazeni, setObsazeni] = useState<obsazeniInterface[][]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  const [calMonth,setCalMonth] = useState<Date[][]>(cal.monthDates(yearNumber, monthNumber));

  const [socket, setSocket] = useState<Socket>();

  // Načtení konfigurace kalendáře a rezervací ze serveru
  useEffect(() => {
    laravelCall
      .get(`windows/${props.windowid}/calendar`)()
      .then((data: any) => {
        setData(data);
        console.log("data window_id " + props.windowid, data);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        setError(err.toString());
      });
    if(!formModal.open){
      setSelectDate({
        endDate: null,
        startDate: null
      });
    }
  }, [formModal]);

  // Změna měsíce
  useEffect(()=>{
    const month = cal.monthDates(yearNumber, monthNumber);
    setCalMonth(month);
    setObsazeni(()=>{
      const obsazeni = objectRegenerate(month);
      if(data){
        occupiedRegenerate(data.occupied,obsazeni,month);
      }
      if(socket){
        lockedRegenerate(lockedDay,obsazeni,calMonth,socket.id);
      }
      return [...obsazeni];
    });
  },[yearNumber, monthNumber]);

  // Vypočítání obsazených dnů
  useEffect(() => {
    setObsazeni((obsazeniOrg)=>{
      if(data){
        occupiedRegenerate(data.occupied,obsazeniOrg,calMonth);
      }
      return [...obsazeniOrg];
    });
  },[data]);

  // Vypočítání zamčených dnů
  useEffect(()=>{
    setObsazeni((obsazeniOrg)=>{
      if(socket){
        lockedRegenerate(lockedDay,obsazeniOrg,calMonth,socket.id);
      }
      return [...obsazeniOrg];
    });
  },[lockedDay]);

  // Připojení socketu
  useEffect(()=>{
    const socket = io(process.env.REACT_APP_SOCKET_URL as string,{
      query: {
        windowId: props.windowid
      }
    });
    setSocket(socket);

    socket.connect();

    socket.on("connect",()=>{
      console.log("socket windowID: "+props.windowid+" připojen");
    });
    socket.on("disconnect",()=>{
      console.log("socket windowID: "+props.windowid+" odpojen");
    });
    socket.on("lockedDay",(data)=>{
      console.log("Socket windowID: "+props.windowid+" LockedDay:",data);
      setLockedDay(data);
    });

    return ()=>{
      socket.disconnect();
    }
  },[]);

  // Odeslání změny výběru ostatním kalendářům
  useEffect(()=>{
    if(socket){
      socket.emit("lockedData",selectDate);
    }
  },[selectDate]);

  // Předchozí měsíc
  function last() {
    if (monthNumber === 0) {
      setMonthNumber(11);
      setYearNumber(yearNumber - 1);
    } else {
      setMonthNumber(monthNumber - 1);
    }
  }
  // Následující měsíc
  function next() {
    if (monthNumber === 11) {
      setMonthNumber(0);
      setYearNumber(yearNumber + 1);
    } else {
      setMonthNumber(monthNumber + 1);
    }
  }

  function clickDay(date: Date) {
    console.log("click", date);
    if (data === undefined) return;

    // povolení více dnů
    if (data.calendars.config.days) {
      if (selectDate.startDate === null) {
        setSelectDate({
          startDate: date,
          endDate: date,
        });
      } else {
        if (date.getTime() < selectDate.startDate.getTime()) {
          setSelectDate({
            startDate: date,
            endDate: selectDate.startDate,
          });
        } else if (selectDate.startDate.getTime() === date.getTime()) {
          setSelectDate({
            startDate: null,
            endDate: null,
          });
        } else {
          setSelectDate({ ...selectDate, endDate: date });
        }
      }
    }
    // povolení jednoho dne
    else {
      // přidání dne
      if (selectDate.startDate === null || selectDate.startDate.getTime() !== date.getTime()) {
        setSelectDate({
          startDate: date,
          endDate: date,
        });
      }
      // odebrání dne
      else {
        setSelectDate({
          startDate: null,
          endDate: null,
        });
      }
    }
  }

  function openModal() {
    setFormModal({
      open: true,
      window_id: props.windowid,
      startDate: moment(selectDate.startDate).format("YYYY-MM-DD"),
      startTime: moment(selectDate.startDate).format("HH:mm"),
      endDate: moment(selectDate.endDate).format("YYYY-MM-DD"),
      endTime: moment(selectDate.startDate).format("HH:mm"),
    });
  }

  interface styleInterface {
    color: string;
    backgroundColor: string;
    border?: string;
    fontWeight?: string;
  }

  if (loading) {
    return (
      <div className={style.calendar}>
        <CircularProgress />
      </div>
    );
  }
  if (error) {
    return (
      <div className={style.calendar}>
        <div style={{ backgroundColor: "white" }}>{error}</div>
      </div>
    );
  }

  return (
    <div className={style.calendar}>
      <div className={style.header}>
        <div className={style.headerName}>
          
          <span style={{ flex: "1", padding: "5px" }}>
            {month[monthNumber]} {yearNumber}
          </span>

          {selectDate.startDate !== null || selectDate.endDate !== null ? (
            <Button 
              onClick={openModal}
              variant="contained"
              sx={{...data?.calendars.config.style.day.select, padding:"5px !important", margin:"2px" }}
            >
              Rezervovat
            </Button>
          ) : (
            ""
          )}
                    
          <IconButton
            color="primary"
            onClick={() => {
              last();
            }}
            aria-label="upload picture"
            component="button"
          >
            <ArrowBackIosNewIcon />
          </IconButton>

          <IconButton
            color="primary"
            onClick={() => {
              next();
            }}
            aria-label="upload picture"
            component="button"
          >
            <ArrowForwardIosIcon />
          </IconButton>
        </div>
        <div className={style.headerDays}>
          {week.map((day) => {
            return (
              <div key={day} className={style.headerDay}>
                <span>{day}</span>
              </div>
            );
          })}
        </div>
      </div>
      <div className={style.body}>
        {/* @ts-ignore */}
        {calMonth.map((week, index) => {
          const height = 100 / calMonth.length;
          return (
            <div className={style.week} style={{ height: height.toString() + "%" }} key={index}>
              {/* @ts-ignore */}
              {week.map((day: Date, indexB) => {
                let meziDen = false;
                if (selectDate?.startDate && selectDate?.endDate) {
                  if (
                    day.getTime() > selectDate.startDate.getTime() &&
                    day.getTime() < selectDate.endDate.getTime()
                  ) {
                    meziDen = true;
                  }
                }

                const styleDay: styleInterface = {
                  color: "black",
                  backgroundColor: "white",
                };

                if (data) {
                  // obsazený den
                  if (obsazeni[index][indexB].number == data.calendars.config.rezerveMax) {
                    styleDay["color"] = data.calendars.config.style.day.reserve.full.color;
                    styleDay["backgroundColor"] =
                      data.calendars.config.style.day.reserve.full.backgroundColor;
                  } else if (obsazeni[index][indexB].number > 0) {
                    styleDay["color"] = data.calendars.config.style.day.reserve.part.color;
                    styleDay["backgroundColor"] =
                      data.calendars.config.style.day.reserve.part.backgroundColor;
                  }

                  // vybraný den
                  if (
                    meziDen ||
                    day.getTime() === selectDate.startDate?.getTime() ||
                    day.getTime() === selectDate.endDate?.getTime()
                  ) {
                    styleDay["color"] = data.calendars.config.style.day.select.color;
                    styleDay["backgroundColor"] =
                      data.calendars.config.style.day.select.backgroundColor;
                  }

                  // zakázaný den
                  if (
                    conf.daysAllowed.findIndex((element) => {
                      return element == day.getDay();
                    }) === -1
                  ) {
                    styleDay["color"] = data.calendars.config.style.day.disabled.color;
                    styleDay["backgroundColor"] = data.calendars.config.style.day.disabled.backgroundColor;
                  }

                  // Zamčený den
                  if(obsazeni[index][indexB].locked){
                    styleDay["color"] = data.calendars.config.style.day.locked.color;
                    styleDay["backgroundColor"] = data.calendars.config.style.day.locked.backgroundColor;
                  }

                }

                // aktuální den
                if (
                  day.getFullYear() === date.getFullYear() &&
                  day.getMonth() === date.getMonth() &&
                  day.getDate() === date.getDate()
                ) {
                  styleDay["border"] = "1px solid black";
                  styleDay["fontWeight"] = "bold";
                }

                // Proměné dne
                let dayPropt = {
                  tooltipTitle: "",         // Popis dne (když je zakázán)
                  viewDay: true,            // Jestli je den povolen
                  viewRezerveNumber: true,  // Jestli se má zobrazit počet rezervací
                }

                // Popis tooltipu a stav zobrazení počtu rezeravcí 
                // Jestli je dosaženo maximálního počtu rezervací
                if(obsazeni[index][indexB].number === data?.calendars.config.rezerveMax){
                  dayPropt.tooltipTitle = "Dosažen maximální počet rezervací";
                  dayPropt.viewDay = false;
                }
                // Jestli je den zamčený
                else if(obsazeni[index][indexB].locked){
                  dayPropt.tooltipTitle = "Den má aktuálně někdo otevřen";
                  dayPropt.viewDay = false;
                }
                // Jestli je den zakázán
                else if(conf.daysAllowed.findIndex((element) => {
                  return element == day.getDay();
                }) == -1){
                  dayPropt.tooltipTitle = "Den je zakázán";
                  dayPropt.viewDay = false;
                  dayPropt.viewRezerveNumber = false;
                }

                
                return(
                  <React.Fragment>
                    {data?.calendars.config.half === false || meziDen ? (
                      <Tooltip title={dayPropt.tooltipTitle} placement="top">
                        <div
                          onClick={()=>{ if(dayPropt.viewDay){clickDay(day)} }}
                          className={
                            style.day
                            + " " +
                            (dayPropt.viewDay ? style.dayHover:"")
                          }
                          style={styleDay}
                          key={day.getTime()}
                        >
                          <span>{day.getDate()}</span>
                          {data?.calendars.config.rezerveMax !== 1 && dayPropt.viewRezerveNumber ? (
                            <span
                              style={{
                                color: "gray",
                                fontSize: "small",
                                bottom: "5px",
                                right: "5px",
                                position: "absolute",
                              }}
                            >
                              {obsazeni[index][indexB].number}/{data?.calendars.config.rezerveMax}
                            </span>
                          ) : (
                            ""
                          )}
                        </div>
                      </Tooltip>
                    ) : (
                      <div
                        onClick={()=>{ if(dayPropt.viewDay){clickDay(day)} }}
                        className={style.day}
                        style={styleDay}
                        key={day.getTime()}
                      >
                        <span>{day.getDate()}</span>
                        <div
                          className={style.halDay}
                          style={
                            day.getTime() === selectDate.endDate?.getTime()
                              ? { backgroundColor: "red" }
                              : {}
                          }
                        ></div>
                        <div
                          className={style.halDay}
                          style={
                            day.getTime() === selectDate.startDate?.getTime()
                              ? { backgroundColor: "red" }
                              : {}
                          }
                        ></div>
                      </div>
                    )}
                  </React.Fragment>
                )
                

                

              })}
            </div>
          );
        })}
      </div>
      {formModal.open ? <FormModal modalData={formModal} setModalData={setFormModal} /> : ""}
    </div>
  );
}

Calendar.propTypes = {
  windowid: PropTypes.string,
};

export default Calendar;
