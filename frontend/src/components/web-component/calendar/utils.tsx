import { lockedDayInterface, obsazeniInterface, occupiedInterface } from "./Calendar";

import moment from "moment/moment";

// Vytvoření polí a vypočítání obsazených dnů
export function objectRegenerate(month:Date[][]){
  let obsazeni: obsazeniInterface[][] = [];

  // Vytvoření pole odpovídající kalendáři
  for (let i = 0; month.length > i; i++) {
    let obsazeniTyden = [];
    for (let e = 0; month[i].length > e; e++) {
      let day:obsazeniInterface = {
        color: "white",
        number: 0,
        start: false,
        end: false,
        locked: false
      };
      // přidání dnů
      obsazeniTyden.push(day);
    }
    // přidání týdnů
    obsazeni.push(obsazeniTyden);
  }
  console.log("object regenerate",month,obsazeni);
  return(obsazeni);
}

// Přegenerování zarezervovaných dní
export function occupiedRegenerate(occupied:occupiedInterface[],confDay:obsazeniInterface[][],month:Date[][]){
  // vyčištění pole dní
  for(let i = 0; confDay.length > i; i++){
    for(let e = 0; confDay[i].length > e; e++){
      confDay[i][e].number = 0;
    }
  }
  // zpracování existujících rezervací
  for (let i = 0; occupied.length > i; i++) {
    const start_date = new Date(occupied[i].date_start);
    const end_date = new Date(occupied[i].date_end);

    for (let e = 0; month.length > e; e++) {
      for (let a = 0; month[e].length > a; a++) {
        if (
          moment(month[e][a]).format("YYYY-MM-DD") === moment(start_date).format("YYYY-MM-DD") ||
          moment(month[e][a]).format("YYYY-MM-DD") === moment(end_date).format("YYYY-MM-DD")
        ) {
          confDay[e][a].number = confDay[e][a].number + 1;
        } else if (
          Date.parse(moment(start_date).format("YYYY-MM-DD")) < Date.parse(moment(month[e][a]).format("YYYY-MM-DD")) &&
          Date.parse(moment(month[e][a]).format("YYYY-MM-DD")) < Date.parse(moment(end_date).format("YYYY-MM-DD"))
        ) {
          confDay[e][a].number = confDay[e][a].number + 1;
        }
      }
    }
  }
  console.log("occopied regenerate");
}

// Přegenerování zamčených dní
export function lockedRegenerate(lockedDay:lockedDayInterface[],confDay:obsazeniInterface[][],month:Date[][],socketId:string){
  // vyčištění pole dní
  for(let i = 0; confDay.length > i; i++){
    for(let e = 0; confDay[i].length > e; e++){
      confDay[i][e].locked = false;
    }
  }
  // zpracování zamčených dní
  for (let i = 0; lockedDay.length > i; i++) {
    if(lockedDay[i].select.startDate !== null && lockedDay[i].select.endDate !== null && socketId !== lockedDay[i].socketId){
      const start_date = lockedDay[i].select.startDate;
      const end_date = lockedDay[i].select.endDate;

      for (let e = 0; month.length > e; e++) {
        for (let a = 0; month[e].length > a; a++) {
          if (
            moment(month[e][a]).format("YYYY-MM-DD") === moment(start_date).format("YYYY-MM-DD") ||
            moment(month[e][a]).format("YYYY-MM-DD") === moment(end_date).format("YYYY-MM-DD")
          ) {
            confDay[e][a].locked = true;
          } else if (
            Date.parse(moment(start_date).format("YYYY-MM-DD")) < Date.parse(moment(month[e][a]).format("YYYY-MM-DD")) &&
            Date.parse(moment(month[e][a]).format("YYYY-MM-DD")) < Date.parse(moment(end_date).format("YYYY-MM-DD"))
          ) {
            confDay[e][a].locked = true;
          }
        }
      }
    }
  }
  console.log("locked regenerate");
}

