import React from "react";
import { Routes, Route } from "react-router-dom";
import { BrowserView, MobileView, TabletView, isBrowser, isMobile } from "react-device-detect";

import Layout from "./Layout";
import MLayout from "./MLayout/Index";

import Dashboard from "./portal/dashboard/Dashboadr";

import Management from "./portal/management/Management";
import Form from "./portal/management/Form";
import Calendar from "./portal/management/Calendar";
import Event from "./portal/management/Event";
import Window from "./portal/management/Window";

import Settings from "./portal/settings/Settings";
import ReservationIndex from "./portal/reservation/ReservationIndex";
import Reservation from "./portal/reservation/Reservation";
import Profile from "./portal/profile/Profile";
import Events from "./portal/events/Events";

export default function Routing() {
  //   {/*Pouze v prohlížečích*/}
  //   <BrowserView>
  //   <Layout>
  //     <Routes>
  //       <Route path="dashboard" element={<Dashboard />} />
  //       {/* <Route path="" element={<Users />} /> */}
  //     </Routes>
  //   </Layout>
  // </BrowserView>

  // {/*Pouze na tabletu*/}
  // <TabletView>
  //   <Layout>
  //     <Routes>
  //       <Route path="dashboard" element={<Dashboard />} />
  //       {/* <Route path="" element={<Users />} /> */}
  //     </Routes>
  //   </Layout>
  // </TabletView>

  return (
    <React.Fragment>
      {/*Pouze na mobilech*/}
      <MobileView>
        <MLayout>
          <Routes>
            <Route path="dashboard" element={<Dashboard />} />

            <Route path="management" element={<Management />} />
            <Route path="management/form" element={<Form />} />
            <Route path="management/calendar" element={<Calendar />} />
            <Route path="management/event" element={<Event />} />
            <Route path="management/window" element={<Window />} />

            <Route path="reservation" element={<Reservation />} />
            <Route path="event" element={<Events />} />
            <Route path="settings" element={<Settings />} />
          </Routes>
        </MLayout>
      </MobileView>

      {/*Pouze na počítačích*/}
      <BrowserView>
        <Layout>
          <Routes>
            <Route path="dashboard" element={<Dashboard />} />

            <Route path="management" element={<Management />} />
            <Route path="management/form" element={<Form />} />
            <Route path="management/calendar" element={<Calendar />} />
            <Route path="management/event" element={<Event />} />
            <Route path="management/window" element={<Window />} />
            <Route path="reservation" element={<Reservation />} />
            
            <Route path="action" element={<Events />} />
            <Route path="settings" element={<Settings />} />
            <Route path="profile/edit" element={<Profile />} />
            <Route path="*" element={<>Not found</>} />
          </Routes>
        </Layout>
      </BrowserView>
    </React.Fragment>
  );
}
