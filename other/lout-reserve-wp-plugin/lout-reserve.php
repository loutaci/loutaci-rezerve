<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://loutaci.cz/
 * @since             1.0.0
 * @package           Lout_Reserve
 *
 * @wordpress-plugin
 * Plugin Name:       Loutaci Reserve
 * Plugin URI:        https://rezervace.loutaci.cz/
 * Description:       Rezervační formuláře snadno a rychle
 * Version:           1.0.0
 * Author:            Louťáci
 * Author URI:        https://loutaci.cz/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       https://rezervace.loutaci.cz/
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('LOUT_RESERVE_VERSION', '1.0.0');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-lout-reserve-activator.php
 */
function activate_lout_reserve()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-lout-reserve-activator.php';
	Lout_Reserve_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-lout-reserve-deactivator.php
 */
function deactivate_lout_reserve()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-lout-reserve-deactivator.php';
	Lout_Reserve_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_lout_reserve');
register_deactivation_hook(__FILE__, 'deactivate_lout_reserve');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-lout-reserve.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_lout_reserve()
{

	$plugin = new Lout_Reserve();
	$plugin->run();
}
run_lout_reserve();


function load_lout_reserve_page()
{
	require_once plugin_dir_path(__FILE__) . 'admin/class-lout-reserve-admin.php';
	$plugin_admin = new Lout_Reserve_Admin(plugin_basename(__FILE__), '1.0.0');
	$plugin_admin->enqueue_styles();
	$plugin_admin->enqueue_scripts();
	$plugin_admin->template();
}

function wpdocs_register_my_custom_menu_page()
{
	add_menu_page(
		"Lout Reserve",
		"Lout Reserve",
		"manage_options",
		"lout-reserve/admin/partials/lout-reserve-admin-display.php",
		'load_lout_reserve_page',
		'dashicons-admin-multisite',
		100
	);
}
add_action('admin_menu', 'wpdocs_register_my_custom_menu_page');


$GLOBALS['is_lout_reserve_shortcode_used'] = false;

function lout_reserve_shortcode($atts = [], $content = null)
{
	$out = "";
	if (!$GLOBALS['is_lout_reserve_shortcode_used']) {
		$out .= '<script defer="defer" src="https://test-vitek.loutaci.cz/static/js/bundle.js"></script>';
		$GLOBALS['is_lout_reserve_shortcode_used'] = true;
	}

	$out .= "<p>" . '<div style="width:' . ($atts['width'] ? $atts['width'] . "" : "100%") .  '; height:' . ($atts['height'] ? $atts['height'] . "" : "300px") . ';"><loutacky-formular windowid="' . $atts['id'] . '"/></div>' . "</p>";
	return $out;
}

add_shortcode('lout-reserve', 'lout_reserve_shortcode');
