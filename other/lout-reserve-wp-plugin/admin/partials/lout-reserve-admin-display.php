<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://lukaskovar.com/
 * @since      1.0.0
 *
 * @package    Lout_Reserve
 * @subpackage Lout_Reserve/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<h1>Lout Reserve</h1>
<iframe src="https://testing.loutaci.cz" style="height:calc(80vh); width:98%;display:block" frameborder="0"></iframe>