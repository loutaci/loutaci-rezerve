<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://lukaskovar.com/
 * @since      1.0.0
 *
 * @package    Lout_Reserve
 * @subpackage Lout_Reserve/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Lout_Reserve
 * @subpackage Lout_Reserve/includes
 * @author     Lukáš Kovář <lkov@post.cz>
 */
class Lout_Reserve_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'lout-reserve',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
