<?php

/**
 * Fired during plugin activation
 *
 * @link       https://lukaskovar.com/
 * @since      1.0.0
 *
 * @package    Lout_Reserve
 * @subpackage Lout_Reserve/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Lout_Reserve
 * @subpackage Lout_Reserve/includes
 * @author     Lukáš Kovář <lkov@post.cz>
 */
class Lout_Reserve_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
