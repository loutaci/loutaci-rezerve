<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://lukaskovar.com/
 * @since      1.0.0
 *
 * @package    Lout_Reserve
 * @subpackage Lout_Reserve/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Lout_Reserve
 * @subpackage Lout_Reserve/includes
 * @author     Lukáš Kovář <lkov@post.cz>
 */
class Lout_Reserve_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
