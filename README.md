# Loutaci-Rezerve

- with team DingDingDong

# Instalace/Nasazení

záloha databáze je v db.sql

- spouštění

```sh
docker-compose up # pro spusteni frontendu
```

- backend je třeba nasadit na hosting podporující _PHP_
- databáze musí být mysql
- konfigurace udaju v databazi je v `backend/.env`


# lokální spuštění aplikace s https (npm start v bash konzoli)
WDS_SOCKET_PORT=9000  npm start
local-ssl-proxy --source 9000 --target 3000
