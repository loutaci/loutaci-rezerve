import { Server } from "socket.io";
import { io } from "./server";

export interface lockedDayInterface {
  windowId:number,
  calendar:{
    socketId: string,
    select:selectInterface
  }[]
}

interface selectInterface {
  startDate: Date | null;
  endDate: Date | null;
}

export let lockedDay:lockedDayInterface[] = [];

// Vytvoření polžek dat místnosti a kalendáře
export function createLockedData(windowId:number,data:selectInterface,socketId:string){
  // Hledání existence místnosti (windowId)
  const findIndex = findWindowId(windowId);
  if(findIndex !== -1){
    lockedDay[findIndex].calendar.push({
      socketId:socketId,
      select:data
    });
    console.log("Vytvoření položky kalendáře", windowId, socketId);
    // Odeslání zprávy do místnosti
    io.to(windowId.toString()).emit("lockedDay",lockedDay[findIndex].calendar);
  }else{
    lockedDay.push({
      windowId:windowId,
      calendar:[{
        socketId:socketId,
        select:data
      }]
    });
    console.log("Vytvoření celé místnosti", windowId);
    // Odeslání zprávy do místnosti
    io.to(windowId.toString()).emit("lockedDay",lockedDay[lockedDay.length-1].calendar);
  }
}

// Smazání položek dat místnosti a kalendáře
export function deleteLockedData(windowId:number,socketId:string){
  // Hledání existence místnosti (windowId)
  const findIndex = findWindowId(windowId);
  if(findIndex !== -1){
    // Když je pole rovno jedné smažou se data celé místnost inak je obsah pole
    if(lockedDay[findIndex].calendar.length === 1){
      lockedDay.splice(findIndex,1);
      console.log("Smazání celé místnosti",windowId);
    }else{
      // Hledání konkrétního kalendáře (sockedId)
      for(let e=1; lockedDay[findIndex].calendar.length > e; e++){
        if(lockedDay[findIndex].calendar[e].socketId === socketId){
          lockedDay[findIndex].calendar.splice(e,1);
          console.log("Smazání položky kalendáře",windowId,socketId);
        }
      }
      // Odeslání zprávy do místnosti
      io.to(windowId.toString()).emit("lockedDay",lockedDay[findIndex].calendar);
    }
  }
}

// Přepis polžky kalendáře
export function rewriteLockedData(windowId:number,data:selectInterface,socketId:string){
  // Hledání indexu místnosti (windowId)
  const findIndex = findWindowId(windowId);
  if(findIndex !== -1){
    // Hledání konkrétního kalendáře (sockedId)
    for(let e=1; lockedDay[findIndex].calendar.length > e; e++){
      if(lockedDay[findIndex].calendar[e].socketId === socketId){
        lockedDay[findIndex].calendar[e].select = data;
        console.log("Přepis položky kalendáře s id: "+windowId,data);
      }
    }
    // Odeslání zprávy do místnosti
    io.to(windowId.toString()).emit("lockedDay",lockedDay[findIndex].calendar);
  }
}

// Hledání položky dat odpovídající windowId a soketu
export function findLockedData(windowId:number,socketId:string){
  // Hledání místnosti (windowID)
  for(let i=1; lockedDay.length > i; i++){
    if(lockedDay[i].windowId === windowId){
      // Hledání konkrétního kalendáře (sockedId)
      for(let e=1; lockedDay[i].calendar.length > e; e++){
        if(lockedDay[i].calendar[e].socketId === socketId){
          return true
        }
      }
    }
  }
  return false;
}

// Hledání indexu místnosti
export function findWindowId(windowId:number){
  // Hledání místnosti (windowID)
  for(let i=1; lockedDay.length > i; i++){
    if(lockedDay[i].windowId === windowId){
      return i
    }
  }
  return -1;
}