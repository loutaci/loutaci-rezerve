import { createServer } from "http";
import { Server, Socket } from "socket.io";

import {
  createLockedData,
  deleteLockedData,
  rewriteLockedData,
  findLockedData,
  lockedDay,
  findWindowId
} from "./LockedData";

const config = {
  socket:{
    port: 3001,
    cors:[
      "https://loutaci.cz"
    ]
  }
}

const httpServer = createServer();
export const io = new Server(httpServer, {
  cors:{
    origin: config.socket.cors
  }
});

io.on('connection', async (socket: Socket) => {
  //const windowId = await fetchWindowId(socket);
  const windowId:number = Number(socket.handshake.query.windowId || 0);
  console.log("Připojeno: "+socket.id+", WindowID: "+windowId);

  // Přidání do místnosti
  socket.join(windowId.toString());

  // Hledání existence dat místnosti
  const index = findWindowId(windowId);
  if(index !== -1){
    // Odeslání zamčených položek
    socket.emit("lockedDay",lockedDay[index].calendar);
  }

  // Zásip zamčených položek
  socket.on('lockedData', (data) => {
    if(findLockedData(windowId,socket.id)){
      rewriteLockedData(windowId,data,socket.id);
    }else{
      createLockedData(windowId,data,socket.id);
    }
  });

  socket.on('disconnecting', () => {
    const rooms = Object.keys(socket.rooms);
    // the rooms array contains at least the socket ID
  });

  socket.on('disconnect', () => {
    console.log("Odpojeno: "+socket.id+", WindowID: "+windowId);
    if(findLockedData(windowId,socket.id)){
      deleteLockedData(windowId,socket.id);
    }
  });
});

httpServer.listen(config.socket.port);